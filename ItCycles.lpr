{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
program ItCycles;

{< Starter for integration tests. }

{$mode objfpc}{$H+}

uses
  Interfaces, Forms, GuiTestRunner, it_SinglePositions, unitdomainxchg,
  UnitAstron, it_allcelpointpos, UnitTestTools, it_pairedpositions;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TGuiTestRunner, TestRunner);
  Application.Run;
end.

