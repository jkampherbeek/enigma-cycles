{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
program UtCycles;

{< Starter for unit tests. }

{$mode objfpc}{$H+}

uses
  Interfaces, Forms, GuiTestRunner, test_astron, test_conversions,
  test_domainxchg, test_init, test_process, test_uiperiods, test_uitexts,
  test_validation, test_reqresp, test_translation, swissdelphi, UnitUiUtils;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TGuiTestRunner, TestRunner);
  Application.Run;
end.

