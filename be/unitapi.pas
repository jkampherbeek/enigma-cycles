{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitAPI;

{< Unit for API's. This should be the only entrance to units in the backend that do process information.}
{$mode objfpc}{$H+}

interface

uses Classes, SysUtils, unitdomainxchg, UnitLog, UnitProcess, UnitReqResp;

type

  { API for all supported series (both single celestial points and pairs of celestial points). }

  { TSeriesAPI }

  TSeriesAPI = class
  strict private
    Series: TSeries;
    Logger: TLogger;
    function CreateCycleDefinition(Request: TSeriesRequest): TCycleDefinition;
  public
    constructor Create;
    destructor Destroy; override;
    function GetSeries(Request: TSeriesRequest): TSeriesResponse;
  end;


implementation


uses
  UnitAstron, UnitConversions;

{ TSeriesAPI }

constructor TSeriesAPI.Create;
begin
  Logger := TLogger.Create;
end;

destructor TSeriesAPI.Destroy;
begin
  inherited Destroy;
end;

function TSeriesAPI.GetSeries(Request: TSeriesRequest): TSeriesResponse;
var
  Ephemeris: TEphemeris;
  JulianDayConversion: TJulianDayConversion;
  FloatingToDecimalDegreeConversion: TFloatingToDecimalDegreeConversion;
  CycleDefinition: TCycleDefinition;
  Response: TSeriesResponse;
begin
  CycleDefinition := CreateCycleDefinition(Request);
  Ephemeris := TEphemeris.Create;
  JulianDayConversion := TJulianDayConversion.Create(Ephemeris.SeFrontend);
  FloatingToDecimalDegreeConversion := TFloatingToDecimalDegreeConversion.Create;
  Series := TSeries.Create(Ephemeris, JulianDayConversion, FloatingToDecimalDegreeConversion);
  Logger.Log('UnitAPI: Sending request');
  if (CycleDefinition.CycleType.Identification = position) then
    Response := Series.HandleCycle(CycleDefinition, (Request as TSeriesSingleRequest).CelPoints)
  else
    Response := Series.HandleCycle(CycleDefinition, (Request as TSeriesPairedRequest).CelPointPairs);
  Logger.log('UnitAPI: Received response');
  Result := Response;
end;


function TSeriesAPI.CreateCycleDefinition(Request: TSeriesRequest): TCycleDefinition;
var
  CycleDefinition: TCycleDefinition;
begin
  CycleDefinition.JdStart := Request.Period.StartDate.JulianDay;
  CycleDefinition.JdEnd := Request.Period.EndDate.JulianDay;
  CycleDefinition.Calendar := Request.Period.StartDate.Calendar;
  CycleDefinition.Interval := Request.Interval;
  CycleDefinition.CoordinateType := Request.Coordinate;
  CycleDefinition.Ayanamsha := Request.Ayanamsha;
  CycleDefinition.CycleType := Request.CycleType;
  CycleDefinition.ObserverPos := Request.ObserverPos;
  Result := CycleDefinition;
end;

end.
