{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitConversions;

{< Unit for conversions and formatting of results.}
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UnitAstron, unitdomainxchg;

type

  { Converts a Double into a string representing a decimal degree }
  TFloatingToDecimalDegreeConversion = class
  public
    {Converts using ddd.fffff, 3 positions for degrees and 5 for the fraction}
    function ConvertDoubleToFormattedText(FormatValue: double): string;
    {Converts using supplied format}
    function ConvertDoubleToFormattedText(FormatValue: double; Format: string): string;
  end;

  { Converts a JUlian Day Numer to a formatted text.}

  { TJulianDayConversion }

  TJulianDayConversion = class
  strict private
    FSeFrontend: TSeFrontend;
    FloatingConversion: TFloatingToDecimalDegreeConversion;
    FmtMonthDay: string;
    FmtYear: string;
  public
    constructor Create(SeFrontend: TSeFrontend);
    destructor Destroy; override;
    { Converts a Julian Day number into a string with a formatted date, using yyyy/mm/dd }
    function ConvertJdToDateText(JdNr: double; Calendar: integer): string;
    { Converts a Julian Day number into a string with a formatted date and time, using yyyy/mm/dd hh.hh}
    function ConvertJdToDateTimeText(JdNr: double; Calendar: integer): string;
    function ConvertJdToSimpleDateTime(JdNr: double; Calendar: integer): TSimpleDateTime;
  end;




implementation

{ TJulianDayConversion ----------------------------------------------------------------------------------------------- }

constructor TJulianDayConversion.Create(SeFrontend: TSeFrontend);
begin
  FSeFrontend := SeFrontend;
  FmtMonthDay := '%2.2d';
  FmtYear := '%4.4d';
  FloatingConversion := TFloatingToDecimalDegreeConversion.Create;
end;

destructor TJulianDayConversion.Destroy;
begin
  FreeAndNil(FloatingConversion);
  inherited Destroy;
end;

function TJulianDayConversion.ConvertJdToDateText(JdNr: double; Calendar: integer): string;
var
  DateTime: TSimpleDateTime;
  DateText: string;
begin
  DateTime := FSeFrontend.SeRevJul(JdNr, Calendar);
  DateText := format(fmtYear, [DateTime.Year]) + '/' + format(fmtMonthDay, [DateTime.Month]) +
    '/' + format(fmtMonthDay, [DateTime.Day]);
  Result := DateText;
end;

function TJulianDayConversion.ConvertJdToDateTimeText(JdNr: double; Calendar: integer): string;
var
  DateTime: TSimpleDateTime;
  UtText, DateText: string;
begin
  DateTime := FSeFrontend.SeRevJul(JdNr, Calendar);
  UtText := FloatingConversion.ConvertDoubleToFormattedText(DateTime.UT + 0.0001, '00.00');
  DateText := format(fmtYear, [DateTime.Year]) + '/' + format(fmtMonthDay, [DateTime.Month]) +
    '/' + format(fmtMonthDay, [DateTime.Day]) + ' ' + UtText;
  Result := DateText;
end;

function TJulianDayConversion.ConvertJdToSimpleDateTime(JdNr: double; Calendar: integer): TSimpleDateTime;
begin
  Result := FSeFrontend.SeRevJul(JdNr, Calendar);
end;

{ TFloatingToDecimalDegreeConversion --------------------------------------------------------------------------------- }

function TFloatingToDecimalDegreeConversion.ConvertDoubleToFormattedText(FormatValue: double): string;
var
  FormatString: string;
begin
  FormatString := '000.00000';
  Result := FormatFloat(FormatString, FormatValue);
end;

function TFloatingToDecimalDegreeConversion.ConvertDoubleToFormattedText(FormatValue: double; Format: string): string;
begin
  Result := FormatFloat(Format, FormatValue);
end;

end.


