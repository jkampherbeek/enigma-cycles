{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitdomainxchg;

{< Domain items that can be used by the frontend and the backend.}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UnitLog;

type
  TDoubleArray = array of double;
  TIntegerArray = array of integer;
  TStringArray = array of string;

  TEnumCycleType = (position, angle);
  TEnumObserverPos = (geocentric, heliocentric);
  TEnumCoordinate = (longitude, latitude, rightasc, decl, radv);
  TEnumAyanamsha = (none, fagan, lahiri, raman, krishnamurti, huber, brand);
  TEnumCelpoint = (sun, moon, mercury, venus, mars, jupiter, saturn, uranus, neptune, pluto,
    meannode, oscunode, earth, chiron, pholus, ceres, pallas, juno, vesta, nessus, huya, makemake,
    haumea, eris, ixion, orcus, quaoar, sedna, varuna, meanapog, oscuapog);

  TValidatedDate = record
    IsValid: boolean;
    JulianDay: double;
    Year, Month, Day, Calendar: integer;
  end;

  TValidatedJulianDay = record
    IsValid: boolean;
    JulianDay: double;
  end;

  TCelPointSpec = record
    SeId, Interval, MaxDayParts: integer;
    Identification: TEnumCelpoint;
    Name: string;
    FirstJd, LastJD: double;
    GeoCentric, HelioCentric, Distance, Selected: boolean;
  end;

  TCelPointPairedSpec = record
    FirstCP: TCelPointSpec;
    SecondCP: TCelPointSpec;
  end;

  TCelPointSpecArray = array [0..29] of TCelPointSpec;

  TCelPointPairedSpecArray = array of TCelPointPairedSpec;

  TPeriod = record
    StartDate: TValidatedDate;
    EndDate: TValidatedDate;
  end;

  TAyanamshaSpec = record
    SeId: integer;
    Identification: TEnumAyanamsha;
    Name: string;
  end;

  TAyanamshaSpecArray = array of TAyanamshaSpec;

  TCoordinateSpec = record
    Identification: TEnumCoordinate;
    Name: string
  end;

  TCoordinateSpecArray = array of TCoordinateSpec;

  TObserverPosSpec = record
    Identification: TEnumObserverPos;
    Name: string;
  end;

  TObserverPosSpecArray = array of TObserverPosSpec;

  TCycleTypeSpec = record
    Identification: TEnumCycleType;
    Name: string;
  end;

  TCycleTypeSpecArray = array of TCycleTypeSpec;

  TCycleDefinition = record
    JdStart, JdEnd: double;
    Calendar, Interval: integer;
    CoordinateType: TCoordinateSpec;
    Ayanamsha: TAyanamshaSpec;
    CycleType: TCycleTypeSpec;
    ObserverPos: TObserverPosSpec;
  end;

  TTimedPosition = class
  strict private
    FJdUt: double;
    FPosition: double;
  public
    constructor Create(PJdUt: double; PPosition: double);
    property JdUt: double read FJdUt;
    property Position: double read FPosition;
  end;

  TFullPosForCoordinate = record
    MainPos: double;
    DeviationPos: double;
    DistancePos: double;
    MainSpeed: double;
    DeviationSpeed: double;
    DistanceSpeed: double;
  end;

  TSimpleDateTime = record
    Year: integer;
    Month: integer;
    Day: integer;
    UT: double;
  end;

  { Converts between numeric id and text for enums for CycleType }
  TCycleTypeEnumConverter = class
  public
    function IdFromString(IdText: string): TEnumCycleType;
    function StringFromId(Id: TEnumCycleType): string;
  end;

  { Converts between numeric id and text for enums for Coordinates }
  TCoordinateEnumConverter = class
  public
    function IdFromString(IdText: string): TEnumCoordinate;
    function StringFromId(Id: TEnumCoordinate): string;
  end;

  { Converts between numeric id and text for enums for Observer positions }
  TObserverPosEnumConverter = class
  public
    function IdFromString(IdText: string): TEnumObserverPos;
    function StringFromId(Id: TEnumObserverPos): string;
  end;

  { Converts between numeric id and text for enums for Ayanamshas }
  TAyanamshaEnumConverter = class
    private
    Logger: TLogger;
  public
    constructor Create;
    function IdFromString(IdText: string): TEnumAyanamsha;
    function StringFromId(Id: TEnumAyanamsha): string;
  end;

  { Converts between numeric id and text for enums for Celestial Points }
  TCelPointEnumConverter = class
  private
    Logger: TLogger;
  public
    constructor Create;
    function IdFromString(IdText: string): TEnumCelpoint;
    function StringFromId(Id: TEnumCelpoint): string;
    function CpSpecFromId(Id: TEnumCelpoint): TCelPointSpec;
  end;


implementation

uses
  typInfo, UnitInit;

{ TCelPointEnumConverter }

constructor TCelPointEnumConverter.Create;
begin
  Logger := TLogger.Create;
end;

function TCelPointEnumConverter.IdFromString(IdText: string): TEnumCelpoint;
var
  CelPoint: TEnumCelpoint;
begin
  case IdText of
    'sun': CelPoint := sun;
    'moon': CelPoint := moon;
    'mercury': CelPoint := mercury;
    'venus': CelPoint := venus;
    'mars': CelPoint := mars;
    'jupiter': CelPoint := jupiter;
    'saturn': CelPoint := saturn;
    'uranus': CelPoint := uranus;
    'neptune': CelPoint := neptune;
    'pluto': CelPoint := pluto;
    'meannode': CelPoint := meannode;
    'oscunode': CelPoint := oscunode;
    'earth': CelPoint := earth;
    'chiron': CelPoint := chiron;
    'pholus': CelPoint := pholus;
    'ceres': CelPoint := ceres;
    'pallas': CelPoint := pallas;
    'juno': CelPoint := juno;
    'vesta': CelPoint := vesta;
    'nessus': CelPoint := nessus;
    'huya': CelPoint := huya;
    'makemake': CelPoint := makemake;
    'haumea': CelPoint := haumea;
    'eris': CelPoint := eris;
    'ixion': CelPoint := ixion;
    'orcus': CelPoint := orcus;
    'quaoar': CelPoint := quaoar;
    'sedna': CelPoint := sedna;
    'varuna': CelPoint := varuna;
    'meanapog': CelPoint := meanapog;
    'oscuapog': CelPoint := oscuapog
    else begin
      Logger.log('ERROR:UnitDomainXchg - Unknown value for IdText in CelPointEnumConverter: ' + IdText);
      raise(EArgumentException.Create('Unknown value for IdText in CelPointEnumConverter: ' + IdText));
    end;
  end;
  Result := CelPoint;
end;

function TCelPointEnumConverter.StringFromId(Id: TEnumCelpoint): string;
var
  CelPointTxt: string;
begin
  WriteStr(CelPointTxt, Id);
  Result := CelPointTxt;
end;

function TCelPointEnumConverter.CpSpecFromId(Id: TEnumCelpoint): TCelPointSpec;
var
  i: integer;
  CpSpec: TCelPointSpec;
  LookupValues: TLookupValues;
begin
  LookupValues := TLookupValues.Create;
  for i := 0 to length(LookupValues.AllCelPoints) - 1 do if (LookupValues.AllCelPoints[i].Identification = Id) then
      CpSpec := LookupValues.AllCelPoints[i];
  Result := CpSpec;
end;



{ TAyanamshaEnumConverter }

constructor TAyanamshaEnumConverter.Create;
begin
  Logger:= TLogger.Create;
end;

function TAyanamshaEnumConverter.IdFromString(IdText: string): TEnumAyanamsha;
var
  Ayanamsha: TEnumAyanamsha;
begin
  case IdText of
    'none': Ayanamsha := none;
    'fagan': Ayanamsha := fagan;
    'lahiri': Ayanamsha := lahiri;
    'raman': Ayanamsha := raman;
    'krishnamurti': Ayanamsha := krishnamurti;
    'huber': Ayanamsha := huber;
    'brand': Ayanamsha := brand
    else begin
      Logger.log('ERROR:UnitDomainXchg - Unknown value for IdText in AyanamshaEnumConverter: ' + IdText);
      raise(EArgumentException.Create('Unknown value for IdText in AyanamshaCelPointEnumConverter: ' + IdText));
  end;
      end;
  Result := Ayanamsha;
end;

function TAyanamshaEnumConverter.StringFromId(Id: TEnumAyanamsha): string;
var
  AyanamshaTxt: string;
begin
  WriteStr(AyanamshaTxt, Id);
  Result := AyanamshaTxt;
end;

{ TObserverPosEnumConverter }

function TObserverPosEnumConverter.IdFromString(IdText: string): TEnumObserverPos;
var
  ObserverPos: TEnumObserverPos;
begin
  case IdText of
    'geocentric': ObserverPos := geocentric;
    'heliocentric': ObserverPos := heliocentric;
  end;
  Result := ObserverPos;
end;

function TObserverPosEnumConverter.StringFromId(Id: TEnumObserverPos): string;
var
  ObserverPosTxt: string;
begin
  WriteStr(ObserverPosTxt, Id);
  Result := ObserverPosTxt;
end;

{ TCoordinateEnumConverter }

function TCoordinateEnumConverter.IdFromString(IdText: string): TEnumCoordinate;
var
  Coordinate: TEnumCoordinate;
begin
  case IdText of
    'longitude': Coordinate := longitude;
    'latitude': Coordinate := latitude;
    'rightasc': Coordinate := rightasc;
    'decl': Coordinate := decl;
    'radv': Coordinate := radv;
  end;
  Result := Coordinate;
end;

function TCoordinateEnumConverter.StringFromId(Id: TEnumCoordinate): string;
var
  CoordinateTxt: string;
begin
  WriteStr(CoordinateTxt, Id);
  Result := CoordinateTxt;
end;

{ TCycleTypeEnumConverter }

function TCycleTypeEnumConverter.IdFromString(IdText: string): TEnumCycleType;
var
  CycleType: TEnumCycleType;
begin
  case IdText of
    'position': CycleType := position;
    'angle': CycleType := angle;
  end;
  Result := CycleType;
end;

function TCycleTypeEnumConverter.StringFromId(Id: TEnumCycleType): string;
var
  CycleTypeTxt: string;
begin
  WriteStr(CycleTypeTxt, Id);
  Result := CycleTypeTxt;
end;

{ TTimedPosition ----------------------------------------------------------------------------------------------------- }

constructor TTimedPosition.Create(PJdUt: double; PPosition: double);
begin
  FJdUt := PJdUt;
  FPosition := PPosition;
end;

end.
