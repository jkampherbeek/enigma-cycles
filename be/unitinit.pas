{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitInit;

{< Unit for initialisation of the application.}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, unitdomainxchg, unittranslation;

type

  { Lookup Values to be used in lists etc. Handles definition and population of these values. }
  TLookupValues = class
  strict private
    FAllAyanamshas: TAyanamshaSpecArray;
    FAllCoordinates: TCoordinateSpecArray;
    FAllCycleTypes: TCycleTypeSpecArray;
    FAllCelpoints: TCelPointSpecArray;
    FAllObserverPos: TObserverPosSpecArray;
    Rosetta: TRosetta;
    function ConstructAyanamshaSpec(SeId: integer; id: TEnumAyanamsha; KeyForName: string): TAyanamshaSpec;
    function ConstructCoordinateSpec(Identification: TEnumCoordinate; KeyForName: string): TCoordinateSpec;
    function ConstructCycleTypeSpec(Identification: TEnumCycleType; KeyForName: string): TCycleTypeSpec;
    function ConstructObserverPosSpec(Identification: TEnumObserverPos; KeyForName: string): TObserverPosSpec;
    function ConstructCelpointSpec(SeId, Interval, MaxDayParts: integer; Identification: TEnumCelpoint;
      KeyForName: string; FirstJd, lastJd: double; Geocentric, HelioCentric, Distance: boolean): TCelPointSpec;
  public
    constructor Create;
    procedure DefineValues;
    procedure DefineAllAyanamshas;
    procedure DefineAllCoordinates;
    procedure DefineAllCycleTypes;
    procedure DefineAllCelpoints;
    procedure DefineAllObserverPos;
    property AllAyanamshas: TAyanamshaSpecArray read FAllAyanamshas;
    property AllCoordinates: TCoordinateSpecArray read FAllCoordinates;
    property AllCycleTypes: TCycleTypeSpecArray read FAllCycleTypes;
    property AllCelPoints: TCelPointSpecArray read FAllCelpoints;
    property AllObserverPos: TObserverPosSpecArray read FAllObserverPos;

  end;




implementation

const
  SECTION = 'lookup';

{ TLookupValues ------------------------------------------------------------------------------------------------------- }

constructor TLookupValues.Create;
begin
  Rosetta := TRosetta.Create;
  DefineValues;
end;

procedure TLookupValues.DefineValues;
begin
  DefineAllAyanamshas;
  DefineAllCoordinates;
  DefineAllCycleTypes;
  DefineAllCelPoints;
  DefineAllObserverPos;
end;

procedure TLookupValues.DefineAllAyanamshas;
var
  nrOfAyanamshas: integer = 7;
begin
  SetLength(FAllAyanamshas, nrOfAyanamshas);
  FAllAyanamshas[0] := ConstructAyanamshaSpec(-1, none, 'ayanamshanone');
  FAllAyanamshas[1] := ConstructAyanamshaSpec(0, fagan, 'ayanamshafagan');
  FAllAyanamshas[2] := ConstructAyanamshaSpec(1, lahiri, 'ayanamshalahiri');
  FAllAyanamshas[3] := ConstructAyanamshaSpec(2, raman, 'ayanamsharaman');
  FAllAyanamshas[4] := ConstructAyanamshaSpec(3, krishnamurti, 'ayanamshakrishnamurti');
  FAllAyanamshas[5] := ConstructAyanamshaSpec(4, huber, 'ayanamshahuber');
  FAllAyanamshas[6] := ConstructAyanamshaSpec(5, brand, 'ayanamshabrand');
end;

function TLookupValues.ConstructAyanamshaSpec(SeId: integer; id: TEnumAyanamsha; KeyForName: string): TAyanamshaSpec;
var
  AyanamshaSpec: TAyanamshaSpec;
begin
  AyanamshaSpec.SeId := SeId;
  AyanamshaSpec.Name := Rosetta.GetText(SECTION, KeyForName);
  AyanamshaSpec.Identification := id;
  Result := AyanamshaSpec;
end;

procedure TLookupValues.DefineAllCoordinates;
var
  NrOfCoordinates: integer = 5;
begin
  SetLength(FAllCoordinates, NrOfCoordinates);
  FAllCoordinates[0] := ConstructCoordinateSpec(longitude, 'coordecllon');
  FAllCoordinates[1] := ConstructCoordinateSpec(latitude, 'coordecllat');
  FAllCoordinates[2] := ConstructCoordinateSpec(rightasc, 'coordra');
  FAllCoordinates[3] := ConstructCoordinateSpec(decl, 'coorddecl');
  FAllCoordinates[4] := ConstructCoordinateSpec(radv, 'coorddistance');
end;

function TLookupValues.ConstructCoordinateSpec(Identification: TEnumCoordinate; KeyForName: string): TCoordinateSpec;
var
  CoordinateSpec: TCoordinateSpec;
begin
  CoordinateSpec.Identification := Identification;
  CoordinateSpec.Name := Rosetta.GetText(SECTION, KeyForName);
  Result := CoordinateSpec;
end;

procedure TLookupValues.DefineAllCycleTypes;
var
  NrOfCycleTypes: integer = 2;
begin
  SetLength(FAllCycleTypes, NrOfCycleTypes);
  FAllCycleTypes[0] := ConstructCycleTypeSpec(position, 'cycletypesimplepos');
  FAllCycleTypes[1] := ConstructCycleTypeSpec(angle, 'cycletypeangle2');
end;

function TLookupValues.ConstructCycleTypeSpec(Identification: TEnumCycleType; KeyForName: string): TCycleTypeSpec;
var
  CycleTypeSpec: TCycleTypeSpec;
begin
  CycleTypeSpec.Identification := Identification;
  CycleTypeSpec.Name := Rosetta.GetText(SECTION, KeyForName);
  Result := CycleTypeSpec;
end;


procedure TLookupValues.DefineAllObserverPos;
var
  NrOfObserverPos: integer = 2;
begin
  SetLength(FAllObserverPos, NrOfObserverPos);
  FAllObserverPos[0] := ConstructObserverPosSpec(geocentric, 'obsposgeocentric');
  FAllObserverPos[1] := ConstructObserverPosSpec(heliocentric, 'obsposheliocentric');
end;

function TLookupValues.ConstructObserverPosSpec(Identification: TEnumObserverPos;
  KeyForName: string): TObserverPosSpec;
var
  ObserverPosSpec: TObserverPosSpec;
begin
  ObserverPosSpec.Identification := Identification;
  ObserverPosSpec.Name := Rosetta.GetText(SECTION, KeyForName);
  Result := ObserverPosSpec;
end;

procedure TLookupValues.DefineAllCelpoints;
begin
  FAllCelpoints[0] := ConstructCelpointSpec(0, 10, 200000, sun, 'celpointsun', -3026613.5,
    5227458.5, True, False, True);
  FAllCelpoints[1] := ConstructCelpointSpec(1, 1, 20000, moon, 'celpointmoon', -3026613.5,
    5227458.5, True, False, True);
  FAllCelpoints[2] := ConstructCelpointSpec(2, 10, 200000, mercury, 'celpointmercury', -3026613.5,
    5227458.5, True, True, True);
  FAllCelpoints[3] := ConstructCelpointSpec(3, 10, 200000, venus, 'celpointvenus', -3026613.5,
    5227458.5, True, True, True);
  FAllCelpoints[4] := ConstructCelpointSpec(4, 10, 200000, mars, 'celpointmars', -3026613.5,
    5227458.5, True, True, True);
  FAllCelpoints[5] := ConstructCelpointSpec(5, 50, 2000000, jupiter, 'celpointjupiter',
    -3026613.5, 5227458.5, True, True, True);
  FAllCelpoints[6] := ConstructCelpointSpec(6, 50, 2000000, saturn, 'celpointsaturn', -3026613.5,
    5227458.5, True, True, True);
  FAllCelpoints[7] := ConstructCelpointSpec(7, 100, 7500000, uranus, 'celpointuranus', -3026613.5,
    5227458.5, True, True, True);
  FAllCelpoints[8] := ConstructCelpointSpec(8, 100, 7500000, neptune, 'celpointneptune',
    -3026613.5, 5227458.5, True, True, True);
  FAllCelpoints[9] := ConstructCelpointSpec(9, 100, 7500000, pluto, 'celpointpluto', -3026613.5,
    5227458.5, True, True, True);
  FAllCelpoints[10] := ConstructCelpointSpec(10, 100, 2000000, meannode, 'celpointmeannode',
    -3026613.5, 5227458.5, True, False, False);
  FAllCelpoints[11] := ConstructCelpointSpec(11, 100, 2000000, oscunode, 'celpointoscnode',
    -3026613.5, 5227458.5, True, False, False);
  FAllCelpoints[12] := ConstructCelpointSpec(14, 10, 200000, earth, 'celpointearth', -3026613.5,
    5227458.5, False, True, True);
  FAllCelpoints[13] := ConstructCelpointSpec(15, 100, 2000000, chiron, 'celpointchiron',
    1967601.5, 3419437.5, True, True, True);
  FAllCelpoints[14] := ConstructCelpointSpec(16, 100, 2000000, pholus, 'celpointpholus',
    640648.5, 4390615.5, True, True, True);
  FAllCelpoints[15] := ConstructCelpointSpec(17, 50, 2000000, ceres, 'celpointceres', -3026613.5,
    5224242.5, True, True, True);
  FAllCelpoints[16] := ConstructCelpointSpec(18, 10, 200000, pallas, 'celpointpallas', -3026613.5,
    5227458.5, True, True, True);
  FAllCelpoints[17] := ConstructCelpointSpec(19, 10, 200000, juno, 'celpointjuno', -3026613.5,
    5227458.5, True, True, True);
  FAllCelpoints[18] := ConstructCelpointSpec(20, 10, 200000, vesta, 'celpointvesta', -3026613.5,
    5221544.5, True, True, True);
  FAllCelpoints[19] := ConstructCelpointSpec(17066, 100, 2000000, nessus, 'celpointnessus',
    625372.5, 2816371.5, True, True, True);
  FAllCelpoints[20] := ConstructCelpointSpec(48628, 100, 2000000, huya, 'celpointhuya',
    625296.5, 2816295.5, True, True, True);
  FAllCelpoints[21] := ConstructCelpointSpec(146472, 200, 20000000, makemake, 'celpointmakemake',
    625292.5, 2816291.5, True, True, True);
  FAllCelpoints[22] := ConstructCelpointSpec(146108, 200, 20000000, haumea, 'celpointhaumea',
    625292.5, 2816291.5, True, True, True);
  FAllCelpoints[23] := ConstructCelpointSpec(146199, 200, 20000000, eris, 'celpointeris',
    625384.5, 2816383.5, True, True, True);
  FAllCelpoints[24] := ConstructCelpointSpec(38978, 200, 20000000, ixion, 'celpointixion',
    625296.5, 2816295.5, True, True, True);
  FAllCelpoints[25] := ConstructCelpointSpec(100482, 200, 20000000, orcus, 'celpointorcus',
    625296.5, 2816295.5, True, True, True);
  FAllCelpoints[26] := ConstructCelpointSpec(60000, 200, 20000000, quaoar, 'celpointquaoar',
    625292.5, 2816291.5, True, True, True);
  FAllCelpoints[27] := ConstructCelpointSpec(100377, 200, 20000000, sedna, 'celpointsedna',
    624947.5, 2816295.5, True, True, True);
  FAllCelpoints[28] := ConstructCelpointSpec(30000, 200, 20000000, varuna, 'celpointvaruna',
    625296.5, 2816295.5, True, True, True);
  FAllCelpoints[29] := ConstructCelpointSpec(12, 10, 200000, meanapog, 'celpointmeanapogee',
    -3026613.5, 5227458.5, True, False, False);

end;

function TLookupValues.ConstructCelpointSpec(SeId, Interval, MaxDayParts: integer;
  Identification: TEnumCelpoint; KeyForName: string; FirstJd, lastJd: double;
  Geocentric, HelioCentric, Distance: boolean): TCelPointSpec;
var
  CelPointSpec: TCelPointSpec;
begin
  CelPointSpec.SeId := SeId;
  CelPointSpec.Interval := Interval;
  CelPointSpec.MaxDayParts := MaxDayParts;
  CelPointSpec.Identification := Identification;
  CelPointSpec.Name := Rosetta.getText(SECTION, KeyForName);
  CelPointSpec.FirstJd := FirstJd;
  CelPointSpec.LastJD := LastJd;
  CelPointSpec.GeoCentric := Geocentric;
  CelPointSpec.HelioCentric := HelioCentric;
  CelPointSpec.Distance := Distance;
  CelPointSpec.Selected := False;
  Result := CelPointSpec;
end;



end.
