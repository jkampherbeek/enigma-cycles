{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitProcess;

{< Classes for processing calculation results. }
{$mode objfpc}{$H+}
{$interfaces corba}

interface

uses
  Classes, SysUtils, Types, unitastron, UnitConst, UnitConversions, unitdomainxchg, UnitLog,
  UnitReqResp, UnitTranslation;

const
  SEMI_COLON = ';';


type

  { Handles processing the cycles for all types of series. }
  TSeries = class
  strict private
    Logger: TLogger;
    Rosetta: TRosetta;
    FFloatingToDecimalDegreeConversion: TFloatingToDecimalDegreeConversion;
    FJulianDayConversion: TJulianDayConversion;
    FEphemeris: TEphemeris;
    FCycleTypeEnumConverter: TCycleTypeEnumConverter;
    FCoordinateEnumConverter: TCoordinateEnumConverter;
    FObserverPosEnumConverter: TObserverPosEnumConverter;
    FAyanamshaEnumConverter: TAyanamshaEnumConverter;
    Frequencies: TIntegerDynArray;
    CelPoints: TCelPointSpecArray;
    PairedCelPoints: TCelPointPairedSpecArray;
    FilenameData, FilenameFreq, ErrorText: string;
    Errors: boolean;
    function FindOffsetsRelevantFrequencies(GroupSize: integer): TIntegerArray;
    function DefineCelpointsText(PCelPoints: TCelPointSpecArray): string;
    function DefinePairedCelpointsText(Pairs: TCelPointPairedSpecArray): string;
    procedure InitFrequency(PCycleDefinition: TCycleDefinition);
    procedure HandleFrequency(Position: double; Coordinate: TCoordinateSpec);
    procedure WriteCp2CsvFile(CycleDefinition: TCycleDefinition);
    procedure WritePair2CsvFile(CycleDefinition: TCycleDefinition);
    procedure WriteFrequenciesFile(CycleDefinition: TCycleDefinition);
  public
    constructor Create(PEphemeris: TEphemeris; PJulianDayConversion: TJulianDayConversion;
      PFloatingToDecimaldegreeConversion: TFloatingToDecimalDegreeConversion);
    destructor Destroy; override;
    function HandleCycle(PCycleDefinition: TCycleDefinition; PCelPoints: TCelPointSpecArray): TSeriesResponse;
    function HandleCycle(PCycleDefinition: TCycleDefinition;
      PCelPointPairs: TCelPointPairedSpecArray): TSeriesResponse;
  end;


  { Flags as used by then Swiss Ephemeris. Assumes that the SE-files are always used and that speed has always to be calculated.}
  TSeFlags = class
  strict private
    FFlagsValue: longint;
    FCoordinateType: TCoordinateSpec;
    FAyanamsha: TAyanamshaSpec;
    FObserverPos: TObserverPosSpec;
    function DefineFlags: longint;
  public
    constructor Create(PCoordinateSpec: TCoordinateSpec; PAyanamsha: TAyanamshaSpec; PObserverPos: TObserverPosSpec);
    property FlagsValue: longint read FFlagsValue;
  end;



  { Converter for date and time. Returns Julian day for UT for date in format yyyy/mm/dd.
    Calendar: 1 = Gregorian, 0 = Julian. }
  TDateTimeConversion = class
  strict private
    FEphemeris: TEphemeris;
  public
    constructor Create(PEphemeris: TEphemeris);
    function DateTextToJulianDay(PDateText: string; PCalendar: integer): double;
  end;


implementation

uses
  Math, StrUtils, swissdelphi;

{ TSeries }

constructor TSeries.Create(PEphemeris: TEphemeris; PJulianDayConversion: TJulianDayConversion;
  PFloatingToDecimaldegreeConversion: TFloatingToDecimalDegreeConversion);
begin
  Logger := TLogger.Create;
  Rosetta := TRosetta.Create;
  FEphemeris := PEphemeris;
  FJulianDayConversion := PJulianDayConversion;
  FFloatingToDecimalDegreeConversion := PFloatingToDecimaldegreeConversion;
  FCycleTypeEnumConverter := TCycleTypeEnumConverter.Create;
  FCoordinateEnumConverter := TCoordinateEnumConverter.Create;
  FObserverPosEnumConverter := TObserverPosEnumConverter.Create;
  FayanamshaEnumConverter := TAyanamshaEnumConverter.Create;
end;

destructor TSeries.Destroy;
begin
  FreeAndNil(FJulianDayConversion);
  FreeAndNil(FCycleTypeEnumConverter);
  FreeAndNil(FCoordinateEnumConverter);
  FreeAndNil(FObserverPosEnumConverter);
  FreeAndNil(FayanamshaEnumConverter);
  inherited Destroy;
end;

function TSeries.HandleCycle(PCycleDefinition: TCycleDefinition; PCelPoints: TCelPointSpecArray): TSeriesResponse;
begin
  Errors := False;
  ErrorText := '';
  CelPoints := PCelPoints;
  if (PCycleDefinition.CoordinateType.Identification <> radv) then InitFrequency(PCycleDefinition);
  WriteCp2CsvFile(PCycleDefinition);
  if (PCycleDefinition.CoordinateType.Identification <> radv) then WriteFrequenciesFile(PCycleDefinition);
  if Errors then Logger.log('ERROR: UnitProcess|In HandleCycle for single celpoints: ' + ErrorText);
  Result := TSeriesResponse.Create(ErrorText, Errors);
end;

function TSeries.HandleCycle(PCycleDefinition: TCycleDefinition;
  PCelPointPairs: TCelPointPairedSpecArray): TSeriesResponse;
begin
  Errors := False;
  ErrorText := '';
  PairedCelPoints := PCelPointPairs;
  if (PCycleDefinition.CoordinateType.Identification <> radv) then InitFrequency(PCycleDefinition);
  WritePair2CsvFile(PCycleDefinition);
  if (PCycleDefinition.CoordinateType.Identification <> radv) then WriteFrequenciesFile(PCycleDefinition);
  if Errors then Logger.log('ERROR: UnitProcess|In HandleCycle for celpoint pairs: ' + ErrorText);
  Result := TSeriesResponse.Create(ErrorText, Errors);
end;

procedure TSeries.InitFrequency(PCycleDefinition: TCycleDefinition);
var
  i: integer;
begin
  case PCycleDefinition.CoordinateType.Identification of
    longitude: SetLength(Frequencies, MAX_INDEX_LON - MIN_INDEX_LON + 1);
    latitude: SetLength(Frequencies, MAX_INDEX_LAT - MIN_INDEX_LAT + 1);
    rightasc: SetLength(Frequencies, MAX_INDEX_RA - MIN_INDEX_RA + 1);
    decl: SetLength(Frequencies, MAX_INDEX_DECL - MIN_INDEX_DECL + 1);
    else begin
      SetLength(Frequencies, 0);
      Errors := True;
      ErrorText := ErrorText + 'Wrong coordinate while initializing frequency: ' +
        PCycleDefinition.CoordinateType.Name;
    end;
  end;
  for i := 0 to Length(Frequencies) - 1 do Frequencies[i] := 0;
end;

procedure TSeries.HandleFrequency(Position: double; Coordinate: TCoordinateSpec);
var
  IntegerPos, Index: integer;
begin
  IntegerPos := Ceil(Position);
  case Coordinate.Identification of
    longitude: Index := IntegerPos - MIN_INDEX_LON;
    latitude: Index := IntegerPos - MIN_INDEX_LAT;
    rightasc: Index := IntegerPos - MIN_INDEX_RA;
    decl: Index := IntegerPos - MIN_INDEX_DECL;
    else begin
      Index := -1;
      Errors := True;
      ErrorText := ErrorText + 'Wrong coordinate while calculating frequency: ' + Coordinate.Name;
    end;
  end;
  if (Index >= 0) then Inc(Frequencies[Index]);
end;


procedure TSeries.WriteCp2CsvFile(CycleDefinition: TCycleDefinition);
var
  CsvHeader, CsvLine, DateTimeText, PositionText: string;
  CsvFile: TextFile;
  Position, CurrentJd: double;
  SeFlags: TSeFlags;
  Flags: longint;
  i, Interval, Count, TempJd: integer;
  FullPosForCoordinate: TFullPosForCoordinate;
begin
  SeFlags := TSeFlags.Create(CycleDefinition.CoordinateType, CycleDefinition.Ayanamsha, CycleDefinition.ObserverPos);
  Flags := SeFlags.FlagsValue;
  if (CycleDefinition.Ayanamsha.SeId >= 0) then FEphemeris.SetSidMode(CycleDefinition.Ayanamsha.SeId);
  CurrentJd := CycleDefinition.JdStart;
  try
    FilenameData := DATA_PATH + POSFILENAME;
    AssignFile(CsvFile, FilenameData);
    CsvHeader := Rosetta.getText(SHARED_SECTION, 'datetime') + SEMI_COLON +
      Rosetta.getText(SHARED_SECTION, 'jdnr') + SEMI_COLON;
    for i := 0 to Length(CelPoints) - 1 do if (CelPoints[i].Selected) then
        CsvHeader := CsvHeader + CelPoints[i].Name + SEMI_COLON;
    rewrite(CsvFile);
    writeLn(CsvFile, CsvHeader);
    Interval := CycleDefinition.Interval;
    Count := 0;
    repeat
      DateTimeText := FJulianDayConversion.ConvertJdToDateTimeText(CurrentJd, CycleDefinition.Calendar);
      PositionText := '';
      for i := 0 to Length(CelPoints) - 1 do if (CelPoints[i].Selected) then begin
          FullPosForCoordinate := FEphemeris.CalcCelPoint(CurrentJD, CelPoints[i].SeId, flags);
          case CycleDefinition.CoordinateType.Identification of
            longitude, rightasc: Position := FullPosForCoordinate.MainPos;
            latitude, decl: Position := FullPosForCoordinate.DeviationPos;
            radv: Position := FullPosForCoordinate.DistancePos;
          end;
          if (CycleDefinition.CoordinateType.Identification <> radv) then
            HandleFrequency(Position, CycleDefinition.CoordinateType);
          PositionText := PositionText + FFloatingToDecimalDegreeConversion.ConvertDoubleToFormattedText(
            Position) + SEMI_COLON;
        end;
      CsvLine := DateTimeText + SEMI_COLON + FloatToStr(CurrentJd) + SEMI_COLON + PositionText;
      writeln(CsvFile, CsvLine);
      Inc(Count);
      TempJd := Round((CurrentJd + Interval / 10 + 0.001) * 100);
      CurrentJd := TempJd / 100.0;
    until CurrentJd > CycleDefinition.JdEnd;
  finally
    CloseFile(CsvFile);
  end;
end;

procedure TSeries.WritePair2CsvFile(CycleDefinition: TCycleDefinition);
var
  CsvHeader, CsvLine, DateTimeText, PairText: string;
  CsvFile: TextFile;
  Position1, Position2, PosMax, PosMin, Angle, CurrentJd: double;
  SeFlags: TSeFlags;
  Flags: longint;
  i, Interval, Count, TempJd: integer;
  FullPosForCoordinate1, FullPosForCoordinate2: TFullPosForCoordinate;
begin
  SeFlags := TSeFlags.Create(CycleDefinition.CoordinateType, CycleDefinition.Ayanamsha, CycleDefinition.ObserverPos);
  Flags := SeFlags.FlagsValue;
  if (CycleDefinition.Ayanamsha.SeId >= 0) then FEphemeris.SetSidMode(CycleDefinition.Ayanamsha.SeId);
  CurrentJd := CycleDefinition.JdStart;
  try
    FilenameData := DATA_PATH + POSFILENAME;
    AssignFile(CsvFile, FilenameData);
    CsvHeader := Rosetta.getText(SHARED_SECTION, 'datetime') + SEMI_COLON +
      Rosetta.getText(SHARED_SECTION, 'jdnr') + SEMI_COLON;
    for i := 0 to Length(PairedCelPoints) - 1 do begin
      PairText := PairedCelPoints[i].FirstCP.Name + SEMI_COLON + PairedCelPoints[i].SecondCP.Name +
        SEMI_COLON + Rosetta.getText(SHARED_SECTION, 'value') + SEMI_COLON;
      CsvHeader := CsvHeader + PairText;
    end;
    rewrite(CsvFile);
    writeLn(CsvFile, CsvHeader);
    Interval := CycleDefinition.Interval;
    Count := 0;
    repeat
      DateTimeText := FJulianDayConversion.ConvertJdToDateTimeText(CurrentJd, CycleDefinition.Calendar);
      PairText := '';
      for i := 0 to Length(PairedCelPoints) - 1 do begin
        FullPosForCoordinate1 := FEphemeris.CalcCelPoint(CurrentJD, PairedCelPoints[i].FirstCP.SeId, flags);
        FullPosForCoordinate2 := FEphemeris.CalcCelPoint(CurrentJD, PairedCelPoints[i].SecondCP.SeId, flags);
        case CycleDefinition.CoordinateType.Identification of
          longitude, rightasc: begin
            Position1 := FullPosForCoordinate1.MainPos;
            Position2 := FullPosForCoordinate2.MainPos;
          end;
          latitude, decl: begin
            Position1 := FullPosForCoordinate1.DeviationPos;
            Position2 := FullPosForCoordinate2.DeviationPos;
          end;
        end;
        PosMax := Max(Position1, Position2);
        PosMin := Min(Position1, Position2);
        Angle := PosMax - PosMin;
        if (Angle > 180.0) then Angle := 360.0 - Angle;
        if (CycleDefinition.CoordinateType.Identification <> radv) then     // should not occur: freq not used for radv
          HandleFrequency(Angle, CycleDefinition.CoordinateType);
        PairText := PairText + FFloatingToDecimalDegreeConversion.ConvertDoubleToFormattedText(Position1) +
          SEMI_COLON + FFloatingToDecimalDegreeConversion.ConvertDoubleToFormattedText(Position2) +
          SEMI_COLON + FFloatingToDecimalDegreeConversion.ConvertDoubleToFormattedText(Angle) + SEMI_COLON;
      end;
      CsvLine := DateTimeText + SEMI_COLON + FloatToStr(CurrentJD) + SEMI_COLON + PairText;
      writeln(CsvFile, CsvLine);
      Inc(Count);
      TempJd := Round((CurrentJd + Interval / 10 + 0.001) * 100);
      CurrentJd := TempJd / 100.0;
    until CurrentJd > CycleDefinition.JdEnd;
  finally
    CloseFile(CsvFile);
  end;
end;



procedure TSeries.WriteFrequenciesFile(CycleDefinition: TCycleDefinition);
var
  i, InitialOffset, BeginOffset, EndOffset: integer;
  AdditionalOffsets: TIntegerArray;
  FreqText: string;
  FreqFile: TextFile;
begin
  case CycleDefinition.CoordinateType.Identification of
    longitude: begin
      InitialOffset := MIN_INDEX_LON;
      BeginOffset := InitialOffset;
      EndOffset := MAX_INDEX_LON;
    end;
    latitude: begin
      InitialOffset := MIN_INDEX_LAT;
      AdditionalOffsets := FindOffsetsRelevantFrequencies(30);
      BeginOffset := AdditionalOffsets[0];
      EndOffset := AdditionalOffsets[1];
    end;
    rightasc: begin
      InitialOffset := MIN_INDEX_RA;
      BeginOffset := InitialOffset;
      EndOffset := MAX_INDEX_RA;
    end;
    decl: begin
      InitialOffset := MIN_INDEX_DECL;
      AdditionalOffsets := FindOffsetsRelevantFrequencies(30);
      BeginOffset := AdditionalOffsets[0];
      EndOffset := AdditionalOffsets[1];
    end
  end;
  try
    FilenameFreq := DATA_PATH + FREQFILENAME;
    AssignFile(FreqFile, FileNameFreq);
    rewrite(FreqFile);
    writeln(FreqFile, 'Bucket; Value');
    for i := BeginOffset - 1 to EndOffset + 30 - 1 do if (i < Length(Frequencies)) then begin
        FreqText := IntToStr(i + InitialOffset) + SEMI_COLON + IntToStr(Frequencies[i]);
        writeln(FreqFile, FreqText);
      end;
  finally
    CloseFile(FreqFile);
  end;
end;

function TSeries.FindOffsetsRelevantFrequencies(GroupSize: integer): TIntegerArray;
var
  i, j, BlockCount, FirstOffset, LastOffset: integer;
  FirstOffsetFound, LastOffsetFound: boolean;
  Offsets: TIntegerArray;
begin
  BlockCount := Length(Frequencies) div 30;
  FirstOffset := 0;
  FirstOffsetFound := False;
  i := 0;
  while ((i < BlockCount) and not (FirstOffsetFound)) do begin
    j := 0;
    while ((j < 30) and not (FirstOffsetFound)) do begin
      if (Frequencies[(i * 30) + j] > 0) then begin
        FirstOffset := i * GroupSize;
        FirstOffsetFound := True;
      end;
      Inc(j);
    end;
    Inc(i);
  end;
  LastOffset := 0;
  LastOffsetFound := False;
  i := BlockCount - 1;
  while ((i >= 0) and not (LastOffsetFound)) do begin
    j := 0;
    while ((j < 30) and not (LastOffsetFound)) do begin
      if (Frequencies[(i * 30) + j] > 0) then begin
        LastOffset := i * GroupSize;
        LastOffsetFound := True;
      end;
      Inc(j);
    end;
    Dec(i);
  end;
  SetLength(Offsets{%H-}, 2);
  Offsets[0] := FirstOffset;
  Offsets[1] := LastOffset;
  Result := Offsets;
end;

function TSeries.DefineCelpointsText(PCelPoints: TCelPointSpecArray): string;
var
  i: integer;
  TempTxt: string;
begin
  TempTxt := '';
  for i := 0 to length(PCelPoints) - 1 do TempTxt := TempTxt + PCelPoints[i].Name + ' ';
  Result := TempTxt;
end;

function TSeries.DefinePairedCelpointsText(Pairs: TCelPointPairedSpecArray): string;
var
  i: integer;
  TempTxt: string;
begin
  TempTxt := '';
  for i := 0 to length(Pairs) - 1 do TempTxt := TempTxt + Pairs[i].FirstCP.Name + '-' + Pairs[i].SecondCP.Name + ' ';
  Result := Temptxt;
end;


{ TSeFlags ----------------------------------------------------------------------------------------------------------- }

constructor TSeFlags.Create(PCoordinateSpec: TCoordinateSpec; PAyanamsha: TAyanamshaSpec;
  PObserverPos: TObserverPosSpec);
begin
  FCoordinateType := PCoordinateSpec;
  FAyanamsha := PAyanamsha;
  FObserverPos := PObserverPos;
  FFlagsValue := DefineFlags;
end;

function TSeFlags.DefineFlags: longint;
var
  Flags: longint;
begin
  Flags := 2 or 256;                                   // Swiss Ephemeris and speed
  if (FObserverPos.Identification = heliocentric) then Flags := Flags or 8;
  if ((FCoordinateType.Identification = rightasc) or (FCoordinateType.Identification = decl)) then
    Flags := Flags or 2048
  else            // geocentric equatorial
  if FAyanamsha.Identification <> None then Flags := Flags or (64 * 1024);
  // sidereal, not for equatorial coordinates
  Result := Flags;
end;



{ TDateTimeConversion ------------------------------------------------------------------------------------------------ }

constructor TDateTimeConversion.Create(PEphemeris: TEphemeris);
begin
  FEphemeris := PEphemeris;
end;


function TDateTimeConversion.DateTextToJulianDay(PDateText: string; PCalendar: integer): double;
var
  TextElements: TStringDynArray;
  Day, Month, Year, Calendar: integer;
  UT: double;
begin

  Calendar := PCalendar;
  UT := 0.0;
  TextElements := SplitString(PDateText, '/');
  Year := StrToInt(TextElements[0]);
  Month := StrToInt(TextElements[1]);
  Day := StrToInt(TextElements[2]);
  Result := FEphemeris.CalcJdUt(Year, Month, Day, UT, Calendar);
end;


end.
