{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
program cycles;

{< Application starter. }

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, Forms, tachartlazaruspkg, unitmain, swissdelphi, unitastron, UnitProcess,
  UnitAPI, UnitConversions, UnitReqResp, UnitInit, unitcentralcontroller,
  UnitValidation, UnitDlgCycleType, UnitDlgCoordinate, UnitDlgPeriod,
  unitdlgsinglecp, unitdlgpairedcp, unitdlgconfirm, UnitConst, unitlinechart,
  unitfreqchart, unitstartscreen, UnitUiUtils, UnitTranslation, UnitLog,
  UnitAbout;

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TFormStartScreen, FormStartScreen);
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormLineChart, FormLineChart);
  Application.CreateForm(TFormFreqChart, FormFreqChart);
  Application.CreateForm(TFormDlgCycleType, FormDlgCycleType);
  Application.CreateForm(TFormDlgConfirm, FormDlgConfirm);
  Application.CreateForm(TFormDlgCoordinate, FormDlgCoordinate);
  Application.CreateForm(TFormDlgPairedCP, FormDlgPairedCP);
  Application.CreateForm(TFormDlgPeriod, FormDlgPeriod);
  Application.CreateForm(TFormDlgSingleCP, FormDlgSingleCP);
  Application.CreateForm(TFormAbout, FormAbout);
  Application.Run;
end.

