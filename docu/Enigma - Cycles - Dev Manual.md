# Enigma - Cycles

<img src="D:\dev\projFP\cycles\docu\img\cycle-img-small.jpg" style="zoom:50%;" />

## Programmers manual version 0.1 Beta

Information for programmers about the program *Enigma Cycles*.



[TOC]



### Introduction

Enigma Cycles (from now on *Cycles* for short) supports the calculation and presentation of several cycles. For a description of the functionality, please check the User Manual.

This document is for developers who want to use the code, create an own version or are just curious.

Cycles is free software and open source. You are allowed to use it provided your own software is also free and open source. The same goes for the use of the Swiss Ephemeris, a library for astronomical calculations that is used by Cycles. Check the file copyright.txt in the root of the source for a formal description about restrictions and possibilities regarding copyright.

Please note that my experience as a programmer is mainly with Java. Cycles is written in *Free Pascal*. I have used several Pascal versions before but that was mostly a long time ago. So I am actually learning on the job. This will not result in the best code possible but I will try to improve it consistently.

### Setting up your development environment

The source of Cycles is written in Free Pascal, an open source implementation of Object Pascal, and to a high degree compatible with Delphi. However, I do use some constructions that are not supported by Delphi. If you want to use Delphi, you will need to make some changes in the code. 

If you want to check the code of Cycles and compile the program yourself, you should use Free Pascal and the standard IDE called *Lazarus*.

#### Free Pascal and Lazarus

You can install Lazarus which includes a current version of FreePascal. Download Lazarus from https://www.lazarus-ide.org/  and navigate to *Downloads*. Make sure you select *Windows (32 and 64 Bits)*. 

Lazarus also works on Linux but I did not test that.

The installation itself is straightforward. One point to keep in mind: do not install in the standard *Program files* folder. This will cause troubles if you want to update and updates happen frequently.  The standard folder *c:\lazarus* is the best way to go.

#### Additional packages

It is easy to install additional packages in Lazarus. This will cause the environment to be rebuild, but that is an automatic process.

Cycles uses the following additional packages:

- BGRABitmap (graphics, supports anti-aliasing)
- BGRAControls (required for BGRABitmap)
- LazRGBGraphics
- VirtualTreeView V5

#### External software and data

Enigma uses software by the Swiss Ephemeris (SE) to perform most astronomical calculations. You need to install the following:

- The file swedll32.dll
- The datafiles from the SE.

You can use the dll and datafiles that are part of the installation package of Enigma-Cycles. The dll should be in the root of your Lazarus project. The datafiles should be in a folder *se* that is one level up, so at the same level as the folder for the project itself.

#### Source and files

The source is available at https://gitlab.com/jkampherbeek/enigma-cycles

The application uses three projects that are combined within a project-group:

- PgCycles.lpg: projectgroup
  - Cycles: project for application.
  - UtCycles: project with unit tests.
  - ItCycles: project with integration tests.

The code is in the folders:

- cycles: general code, projects, projectgroup.
  - be: code for the backend.
  - fe: code for the frontend.
  - test: unit-tests and integration-tests.

The project contains the following additional files and folders:

- data: results from the calculations, exported graphics.
- docu: user manuals (English and Dutch) and developers manual (only English).
- help: html-files for help, including folder css with stylesheets.
- language: ini-files with translated texts for i18N.
- log: logfiles.
- copyright-files: copyright.txt and gpl-3.0.txt
- readme.md
- swedll32.dll: dll for the Swiss Ephemeris.
- cycles.ico: icon
- Cycle-img-small.jpg: image file.
- lang.sel.properties: properties-file for the selected language.

The folders *data* and *log* are not part of the distribution: you need to create them yourself.

The file swedll32.dll must be downloaded from: 

https://www.astro.com/ftp/swisseph/ 

It is in the zip file sweph.zip.

### Architecture

#### Separation of concerns

There is a separation between backend and frontend (user interface). The backend is accessible via an API. This is not enforced but should be done by convention. As this is a small application more effort in this respect is required when the application grows.

#### Naming and documentation

An attempt is made to use meaningful names for variables, methods and classes. Documentation in the source will be minimal.

#### i18N (Internationalization)

Free Pascal has a standard solution for i18N. There is however a drawback: texts from forms are automatically added to the language files, but not in all cases. And the language-files are verbose. No problem for a small application as Cycles but it will become a problem if the application grows significantly. Therefore I developed a  simple alternative for i18N. The logic is in the class *Rosetta* in the unit *UnitTranslation*.  The translated texts are saved in ini files in the folder *language*. Only English and Dutch are supported.

#### Singletons

Several classes are singletons: classes for logging, translation and the central controller.

At https://wiki.freepascal.org/Singleton_Pattern you will find an excellent description of various implementations of this design pattern. I used the last example, Singleton3, as an approach for all Singletons in Cycles.

In this approach, the Constructor is private, which leads to a warning *Constructor should be public*. To make it unnecessary to change the project options to disable this message, I added the following directive to the sources that contain singletons. 

`{$WARN 3018 off : Constructor should be public}` 

#### Logging

Existing log-systems for Free Pascal are mostly aimed at debugging. Most importantly: they do not support rotating logfiles. To solve this I wrote a simple logging system that keeps only the logfiles for the last three sessions. In case of an error, these files can come in handy. The logging is implemented in the class *TLogger* in the unit *UnitLog*.

#### Testing

Unit-tests are implemented and also integration tests. Unfortunately, Free Pascal/Lazarus offers no possibility to calculate the coverage. 

The focus of the unit tests is mainly on parts of the backend. The integration tests mainly test the correctness of the calculations.

I do not intend to add tests for the UI. These types of tests are difficult to maintain and add little value, especially as most of the logic is within the backend. The UI is focussed only on presentation. 



### Astronomical

#### Period of validity of celestial points

The Swiss Ephemeris supports calculations from 13201 BCE up to 17191 CE.  For several non planetary bodies a shorter period is available, depending on the current knowledge about these bodies.

The following table shows the Celestial Points that are available and the period that they are supported, defined in Julian Day Numbers. Check the user manual for a specification in days, months and years.

| Celestial Point(s)                                           | Start        | END         |   Start JD |    End JD |
| :----------------------------------------------------------- | ------------ | ----------- | ---------: | --------: |
| Sun, Moon, Mercury, Venus, Mars, Jupiter, Saturn, Uranus, Neptune, Pluto. Mean Node, Osc. Node, Mean apogee, Pallas, Juno | -12999/08/02 | 16799/12/30 | -3026613.5 | 7857131.5 |
| Chiron                                                       | 0675/01/01   | 4650/01/01  |  1967601.5 | 3419437.5 |
| Pholus                                                       | -2958/01/01  | 7308/12/30  |   640648.5 | 4390615.5 |
| Nessus, Huya, Ixion, Orcus, Varuna, MakeMake, Haumea, Quaoar, Eris, Sedna | -3000/03/18  | 2998/08/23  |   625384.5 | 2816291.5 |
| Ceres, Vesta                                                 | -12999/08/02 | 9591/05/23  | -3026613.5 | 5224242.5 |
|                                                              |              |             |            |           |





