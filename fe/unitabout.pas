{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitAbout;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls, UnitTranslation;

type

  { Form that shows about screen }

  TFormAbout = class(TForm)
    BtnClose: TButton;
    Image: TImage;
    LblDescription: TLabel;
    LblFree: TLabel;
    LblINfo: TLabel;
    LblTechnical: TLabel;
    LblTitle: TLabel;
    procedure BtnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    Rosetta: TRosetta;
    procedure Populate;
  public

  end;

var
  FormAbout: TFormAbout;

implementation

{$R *.lfm}

uses
  UnitConst;

const
  SECTION = 'formabout';

{ TFormAbout }

procedure TFormAbout.FormCreate(Sender: TObject);
begin
  Rosetta:= TRosetta.Create;
end;

procedure TFormAbout.FormShow(Sender: TObject);
begin
  Populate;
end;

procedure TFormAbout.Populate;
begin
  with Rosetta do begin
    FormAbout.Caption:= GetText(SECTION, 'formtitle');
    LblTitle.Caption:= GetText(SECTION, 'title');
    LblDescription.Caption:= GetText(SECTION, 'description');
    LblFree.Caption:= GetText(SECTION, 'free');
    LblInfo.Caption:= GetText(SECTION, 'info');
    LblTechnical.Caption:= GetText(SECTION, 'technical');
    BtnClose.Caption:= GetText(SHARED_SECTION, 'close');
  end;
end;

procedure TFormAbout.BtnCloseClick(Sender: TObject);
begin
  Close;
end;



end.

