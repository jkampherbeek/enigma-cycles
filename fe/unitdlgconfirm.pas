{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitdlgconfirm;

{< Dialog to confirm selections.}

{$mode objfpc}{$H+}

interface

uses
  Classes, ComCtrls, Controls, Dialogs, HelpIntfs, Forms, Graphics, StdCtrls, SysUtils,
  unitcentralcontroller, unitdomainxchg, UnitLog, UnitTranslation, UnitUiUtils;

type

  { Form that shows configrmation dialogue }
  TFormDlgConfirm = class(TForm)
    BtnCancel: TButton;
    BtnConfirm: TButton;
    BtnHelp: TButton;
    LblAyanamsha: TLabel;
    LblAyanamshaValue: TLabel;
    LblCalculating: TLabel;
    LblCelPoints: TLabel;
    LblCelPointsValue: TLabel;
    LblCoordinate: TLabel;
    LblCoordinateValue: TLabel;
    LblCycleType: TLabel;
    LblCycleTypeValue: TLabel;
    LblEndDate: TLabel;
    LblEndDateChanged: TLabel;
    LblEndDateValue: TLabel;
    LblInterval: TLabel;
    LblIntervalValue: TLabel;
    LblObserverPos: TLabel;
    LblObserverPosValue: TLabel;
    LblStartDate: TLabel;
    LblStartDateValue: TLabel;
    LblTitle: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnConfirmClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
  private
    CenCon: TCenCon;
    StateMachine: TFinStateMachine;
    Rosetta: TRosetta;
    Logger: TLogger;
    UiTexts: TUiTexts;
    UiPeriods: TUiPeriods;
    WarningEndDate: string;
    procedure ShowCalcStatus;
    procedure Populate;
    procedure CheckCorrectPeriodLength;
    procedure CheckCorrectInterval;

  public

  end;

var
  FormDlgConfirm: TFormDlgConfirm;

implementation

{$R *.lfm}

uses
  Math, UnitConst;

const
  SECTION = 'formdlgconfirm';

{ TFormDlgConfirm }

procedure TFormDlgConfirm.FormCreate(Sender: TObject);
begin
  CenCon := TCenCon.Create;
  StateMachine := TFinStateMachine.Create;
  Rosetta := TRosetta.Create;
  Logger := TLogger.Create;
  UiTexts := TUiTexts.Create;
  UiPeriods := TUiPeriods.Create;
end;


procedure TFormDlgConfirm.FormShow(Sender: TObject);
begin
  LblCalculating.Visible := False;
  BtnConfirm.Visible := True;
  BtnHelp.Visible := True;
  BtnCancel.Visible := True;
  Populate;
end;

procedure TFormDlgConfirm.BtnConfirmClick(Sender: TObject);
var
  sep: string;
begin
  ShowCalcStatus;
  sep := '|';
  Logger.log('UnitDlgConfirm: Confirmed data.' + LblCycleTypeValue.Caption + sep +
    LblAyanamshaValue.Caption + sep + LblObserverPosValue.Caption + sep + LblCoordinateValue.Caption +
    sep + LblStartDateValue.Caption + LblIntervalValue.Caption + LblCelPointsValue.Caption);
  StateMachine.ChangeState(Confirmed);
end;

procedure TFormDlgConfirm.BtnCancelClick(Sender: TObject);
begin
  Close;
  StateMachine.ChangeState(Cancel);
end;

procedure TFormDlgConfirm.BtnHelpClick(Sender: TObject);
var
  HelpText: string;
begin
  if Rosetta.CurrentLanguage = 'en' then HelpText:= 'html/en_dlgconfirm.html' else HelpText:= 'html/nl_dlgconfirm.html';
  ShowHelpOrErrorForKeyword(EmptyStr, HelpText);
end;

procedure TFormDlgConfirm.ShowCalcStatus;
begin
  LblCalculating.Visible := True;
  BtnConfirm.Visible := False;
  BtnCancel.Visible := False;
  BtnHelp.Visible := False;
  LblCalculating.Visible := True;
  FormDlgConfirm.Refresh;
end;

procedure TFormDlgConfirm.Populate;
begin
  with Rosetta do begin
    FormDlgConfirm.Caption := GetText(SECTION, 'formtitle');
    LblTitle.Caption := GetText(SECTION, 'title');
    LblCycleType.Caption := GetText(SECTION, 'cycletype');
    LblAyanamsha.Caption := GetText(SECTION, 'ayanamsha');
    LblObserverPos.Caption := GetText(SECTION, 'observerpos');
    LblCoordinate.Caption := GetText(SECTION, 'coordinate');
    LblStartDate.Caption := GetTExt(SECTION, 'startdate');
    LblEndDate.Caption := GetText(SECTION, 'enddate');
    LblInterval.Caption := GetText(SECTION, 'interval');
    LblCelPoints.Caption := GetText(SECTION, 'celpoints');
    LblCalculating.Caption := GetText(SECTION, 'msgCalculating');
    BtnConfirm.Caption := GetText(SECTION, 'confirm');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'help');
    BtnCancel.Caption := GetText(SHARED_SECTION, 'cancel');
    WarningEndDate := GetText(SECTION, 'warningenddate');
  end;
  CheckCorrectPeriodLength;
  LblCycleTypeValue.Caption := StateMachine.CycleType.Name;
  LblAyanamshaValue.Caption := StateMachine.Ayanamsha.Name;
  LblObserverPosValue.Caption := StateMachine.ObserverPos.Name;
  LblCoordinateValue.Caption := StateMachine.Coordinate.Name;
  LblStartDateValue.Caption := UiTexts.TextForDate(StateMachine.Period.StartDate);
  LblEndDateValue.Caption := UiTexts.TextForDate(StateMachine.Period.EndDate);
  LblIntervalValue.Caption := IntToStr(StateMachine.Interval);
  CheckCorrectInterval;
end;

procedure TFormDlgConfirm.CheckCorrectPeriodLength;
var
  SupportedPeriodInDays, RequestedPeriodInDays: integer;
  CorrectedPeriod: TPeriod;
begin
  if (StateMachine.CycleType.Identification = angle) then begin
    LblCelPointsValue.Caption := UiTexts.TextForCelPointPairs(StateMachine.PairedCPs);
    SupportedPeriodInDays := UiPeriods.defineShortestPeriodLength(StateMachine.PairedCPs) div 10;
  end else begin
    LblCelPointsValue.Caption := UiTexts.TextForCelPoints(StateMachine.SingleCPs);
    SupportedPeriodInDays := UiPeriods.defineShortestPeriodLength(StateMachine.SingleCPs) div 10;
  end;
  RequestedPeriodInDays := Floor(StateMachine.Period.EndDate.JulianDay -
    StateMachine.Period.StartDate.JulianDay + 0.0001);
  if (RequestedPeriodInDays > SupportedPeriodInDays) then begin
    CorrectedPeriod := UiPeriods.DefineMaxFittingPeriod(StateMachine.Period, SupportedPeriodInDays);
    StateMachine.Period := CorrectedPeriod;
    LblEndDateChanged.Caption := format(WarningEndDate, [SupportedPeriodInDays]);
    Logger.Log('UnitDlgConfirm: adapted period to the max of ' + SupportedPeriodInDays.ToString + ' days');
  end else
  LblEndDateChanged.Caption := '';
end;

procedure TFormDlgConfirm.CheckCorrectInterval;
var
  ShortestInterval: integer;
begin
  if (StateMachine.CycleType.Identification = angle) then ShortestInterval :=
      UiPeriods.DefineShortestInterval(StateMachine.PairedCPs)
  else
    ShortestInterval := UiPeriods.DefineShortestInterval(StateMachine.SingleCPs);
  StateMachine.Interval := ShortestInterval;
  LblIntervalValue.Caption := (ShortestInterval / 10).toString;
end;


end.
