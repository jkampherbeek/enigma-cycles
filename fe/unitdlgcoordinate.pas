{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitDlgCoordinate;

{< Dialog for geographic coordinates.}

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, Forms, Graphics, StdCtrls, SysUtils, HelpIntfs, UnitCentralController,
  UnitDomainXchg, UnitTranslation;

type

  { Form for dialogue that asks for type of coordinate }
  TFormDlgCoordinate = class(TForm)
    BtnCancel: TButton;
    BtnContinue: TButton;
    BtnHelp: TButton;
    CbCoordinate: TComboBox;
    LblTitle: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnContinueClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
    procedure CbCoordinateEditingDone(Sender: TObject);
  private
    CenCon: TCenCon;
    StateMachine: TFinStateMachine;
    Rosetta: TRosetta;
    function ShowIt(ObserverPos: TObserverPosSpec; CoordinateId: TEnumCoordinate;
      CycleType: TCycleTypeSpec): boolean;
    procedure SaveState;
    procedure DefineCoordinateItems;
    procedure Populate;

  public

  end;

var
  FormDlgCoordinate: TFormDlgCoordinate;


implementation

{$R *.lfm}

uses
  UnitConst;

const
  SECTION = 'formdlgcoordinate';

var
  SelectedCoordinate: TCoordinateSpec;
  AllCoordinates: TCoordinateSpecArray;

procedure TFormDlgCoordinate.FormCreate(Sender: TObject);
begin
  CenCon := TCenCon.Create;
  StateMachine := TFinStateMachine.Create;
  Rosetta := TRosetta.Create;
end;

procedure TFormDlgCoordinate.FormShow(Sender: TObject);
begin
  Populate;
  DefineCoordinateItems;
end;

function TFormDlgCoordinate.ShowIt(ObserverPos: TObserverPosSpec; CoordinateId: TEnumCoordinate;
  CycleType: TCycleTypeSpec): boolean;
var
  IsHelio, IsEquat, IsRadv, IsPaired: boolean;
begin
  IsHelio := Observerpos.Identification = heliocentric;
  IsEquat := ((CoordinateId = rightasc) or (CoordinateId = decl));
  IsRadv := CoordinateId = radv;
  IsPaired := CycleType.Identification = angle;
  Result := ((not (IsHelio and IsEquat)) and (not (IsPaired and IsRadv)));
end;

procedure TFormDlgCoordinate.SaveState;
begin
  StateMachine.Coordinate := SelectedCoordinate;
end;

procedure TFormDlgCoordinate.BtnCancelClick(Sender: TObject);
begin
  Close;
  StateMachine.ChangeState(Cancel);
end;

procedure TFormDlgCoordinate.BtnContinueClick(Sender: TObject);
begin
  Close;
  SaveState;
  StateMachine.ChangeState(CoordinateDefined);
end;

procedure TFormDlgCoordinate.BtnHelpClick(Sender: TObject);
var
  HelpText: string;
begin
  if Rosetta.CurrentLanguage = 'en' then HelpText := 'html/en_dlgcoordinate.html'
  else
    HelpText := 'html/nl_dlgcoordinate.html';
  ShowHelpOrErrorForKeyword(EmptyStr, HelpText);
end;

procedure TFormDlgCoordinate.CbCoordinateEditingDone(Sender: TObject);
var
  Index: integer;
begin
  Index := CbCoordinate.ItemIndex;
  SelectedCoordinate := AllCoordinates[Index];
end;


procedure TFormDlgCoordinate.DefineCoordinateItems;
var
  i, NrOfCoordinates: integer;
  CoordinateId: TEnumCoordinate;
begin
  AllCoordinates := CenCon.LookupValues.AllCoordinates;
  CbCoordinate.Items.Clear;
  NrOfCoordinates := Length(AllCoordinates);
  for i := 0 to NrOfCoordinates - 1 do begin
    CoordinateId := AllCoordinates[i].Identification;
    if (ShowIt(StateMachine.ObserverPos, CoordinateId, StateMachine.CycleType)) then
      CbCoordinate.Items.add(AllCoordinates[i].Name);
  end;
  CbCoordinate.ItemIndex := 0;
  SelectedCoordinate := AllCoordinates[0];
end;

procedure TFormDlgCoordinate.Populate;
begin
  with Rosetta do begin
    FormDlgCoordinate.Caption := GetText(SECTION, 'formtitle');
    LblTitle.Caption := GetText(SECTION, 'title');
    BtnContinue.Caption := GetText(SHARED_SECTION, 'continue');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'help');
    BtnCancel.Caption := GetTExt(SHARED_SECTION, 'cancel');
  end;
end;




end.
