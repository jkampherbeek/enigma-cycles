{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitdlgpairedcp;

{< Dialog for pairs of celestial points.}

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, Forms, Graphics, HelpIntfs, StdCtrls, SysUtils, unitcentralcontroller,
  unitdomainxchg, UnitTranslation;

type

  { Form for selection of pairs of celestial points. }
  TFormDlgPairedCP = class(TForm)
    BtnAdd: TButton;
    BtnCancel: TButton;
    BtnClear: TButton;
    BtnContinue: TButton;
    BtnHelp: TButton;
    CBoxCPLeft: TComboBox;
    CboxCPRight: TComboBox;
    LblAnd: TLabel;
    LblSet1: TLabel;
    LblSet1Value: TLabel;
    LblSet2: TLabel;
    LblSet2Value: TLabel;
    LblSet3: TLabel;
    LblSet3Value: TLabel;
    LblSet4: TLabel;
    LblSet4Value: TLabel;
    LblSet5: TLabel;
    LblSet5Value: TLabel;
    LblTitle: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnClearClick(Sender: TObject);
    procedure BtnContinueClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
    procedure CBoxEdited(Sender: TObject);

  private
    CenCon: TCenCon;
    StateMachine: TFinStateMachine;
    Rosetta: TRosetta;
    procedure AddPair;
    procedure CheckSelections;
    procedure ClearSelections;
    procedure DefineCPs;
    procedure PopulateCBoxes;
    procedure SaveState;
    procedure Populate;
  public

  end;

var
  FormDlgPairedCP: TFormDlgPairedCP;

implementation

{$R *.lfm}

uses
  UnitConst;

const
  SECTION = 'formdlgpairedcp';

var
  AllCPs, AvailableCPs: TCelPointSpecArray;
  SelectedPairs: TCelPointPairedSpecArray;
  IndexForSelectedPairs: integer;

{ TFormDlgPairedCP }
procedure TFormDlgPairedCP.FormCreate(Sender: TObject);
begin
  CenCon := TCenCon.Create;
  StateMachine := TFinStateMachine.Create;
  Rosetta := TRosetta.Create;
end;

procedure TFormDlgPairedCP.FormShow(Sender: TObject);
begin
  SetLength(SelectedPairs, 0);
  Populate;
  DefineCPs;
  PopulateCBoxes;
  IndexForSelectedPairs := 0;
  CheckSelections;
end;

procedure TFormDlgPairedCP.BtnAddClick(Sender: TObject);
begin
  AddPair;
end;

procedure TFormDlgPairedCP.BtnCancelClick(Sender: TObject);
begin
  Close;
  StateMachine.ChangeState(Cancel);
end;

procedure TFormDlgPairedCP.BtnClearClick(Sender: TObject);
begin
  ClearSelections;
end;

procedure TFormDlgPairedCP.BtnContinueClick(Sender: TObject);
begin
  Close;
  SaveState;
  StateMachine.ChangeState(PairedCPDefined);
end;

procedure TFormDlgPairedCP.BtnHelpClick(Sender: TObject);
var
  HelpText: string;
begin
  if Rosetta.CurrentLanguage = 'en' then HelpText := 'html/en_dlgpairedcp.html'
  else
    HelpText := 'html/nl_dlgpairedcp.html';
  ShowHelpOrErrorForKeyword(EmptyStr, HelpText);
end;


procedure TFormDlgPairedCP.CBoxEdited(Sender: TObject);
begin
  CheckSelections;
end;

procedure TFormDlgPairedCP.CheckSelections;
var
  LeftSelectIdent, RightSelectIdent,                       // newly selected CPs
  SelectedFirst, SelectedLast: TEnumCelpoint;              // previously selected CPs
  IsNewPair: boolean;
  i, NrOfSelectedPairs: integer;
begin
  IsNewPair := True;
  LeftSelectIdent := AvailableCPs[CBoxCPLeft.ItemIndex].Identification;
  RightSelectIdent := AvailableCPs[CboxCPRight.ItemIndex].Identification;
  if (LeftSelectIdent = RightSelectIdent) or (IndexForSelectedPairs > 4) then IsNewPair := False
  else begin
    NrOfSelectedPairs := Length(SelectedPairs);
    for i := 0 to NrOfSelectedPairs - 1 do begin
      SelectedFirst := SelectedPairs[i].FirstCP.Identification;
      SelectedLast := SelectedPairs[i].SecondCP.Identification;
      if (((LeftSelectIdent = SelectedFirst) and (RightSelectIdent = SelectedLast)) or
        ((LeftSelectIdent = SelectedLast) and (RightSelectIdent = SelectedFirst))) then IsNewPair := False;
    end;
  end;
  BtnAdd.Enabled := IsNewPair;
end;

procedure TFormDlgPairedCP.AddPair;
var
  LeftSelect, RightSelect: TCelPointSpec;
  PairToAdd: TCelPointPairedSpec;
  DescrText: string;
begin
  LeftSelect := AvailableCPs[CBoxCPLeft.ItemIndex];
  RightSelect := AvailableCPs[CboxCPRight.ItemIndex];
  PairToAdd.FirstCP := LeftSelect;
  PairToAdd.SecondCP := RightSelect;
  SetLength(SelectedPairs, IndexForSelectedPairs + 1);
  SelectedPairs[IndexForSelectedPairs] := PairtoAdd;
  DescrText := LeftSelect.Name + ' - ' + RightSelect.Name;
  case IndexForSelectedPairs of
    0: LblSet1Value.Caption := DescrText;
    1: LblSet2Value.Caption := DescrText;
    2: LblSet3Value.Caption := DescrText;
    3: LblSet4Value.Caption := DescrText;
    4: LblSet5Value.Caption := DescrText;
  end;
  Inc(IndexForSelectedPairs);
  BtnContinue.Enabled := True;
  BtnAdd.Enabled := False;
end;

procedure TFormDlgPairedCP.ClearSelections;
begin
  SelectedPairs := Default(TCelPointPairedSpecArray);
  IndexForSelectedPairs := 0;
  LblSet1Value.Caption := EmptyStr;
  LblSet2Value.Caption := EmptyStr;
  LblSet3Value.Caption := EmptyStr;
  LblSet4Value.Caption := EmptyStr;
  LblSet5Value.Caption := EmptyStr;
  BtnContinue.Enabled := False;
  BtnAdd.Enabled := True;
end;

procedure TFormDlgPairedCP.DefineCPs;
var
  i, j, NrOfCPs: integer;
  StartJd, EndJd: double;
  PeriodSupported, ObserverPosSupported: boolean;
  ObserverPos: TEnumObserverPos;
  CurrentCP: TCelPointSpec;
begin
  j := 0;
  AllCPs := CenCon.LookupValues.AllCelPoints;
  NrOfCPs := Length(AllCPs);
  StartJd := StateMachine.Period.StartDate.JulianDay;
  EndJd := StateMachine.Period.EndDate.JulianDay;
  ObserverPos := StateMachine.ObserverPos.Identification;
  for i := 0 to NrOfCPs - 1 do begin
    CurrentCP := AllCPs[i];
    PeriodSupported := ((CurrentCP.FirstJd <= StartJd) and (CurrentCP.LastJD >= EndJd));
    ObserverPosSupported := (((CurrentCP.GeoCentric) and (ObserverPos = geocentric)) or
      ((CurrentCP.HelioCentric) and (ObserverPos = heliocentric)));
    if (PeriodSupported and ObserverPosSupported) then begin
      AvailableCPs[j] := CurrentCP;
      Inc(j);
    end;
  end;
end;

procedure TFormDlgPairedCP.PopulateCBoxes;
var
  i, NrOfCPs: integer;
begin
  CBoxCPLeft.Items.Clear;
  CboxCPRight.Items.Clear;
  NrOfCPs := Length(AvailableCPs);
  for i := 0 to NrOfCPs - 1 do begin
    CBoxCPLeft.Items.add(AvailableCPs[i].Name);
    CBoxCPRight.Items.add(AvailableCPs[i].Name);
  end;
  CBoxCPLeft.ItemIndex := 0;
  CboxCPRight.ItemIndex := 1;
end;

procedure TFormDlgPairedCP.SaveState;
begin
  StateMachine.PairedCPs := SelectedPairs;
end;

procedure TFormDlgPairedCP.Populate;
begin
  with Rosetta do begin
    FormDlgPairedCP.Caption := GetText(SECTION, 'formtitle');
    LblTitle.Caption := GetText(SECTION, 'title');
    LblAnd.Caption := GetText(SECTION, 'and');
    LblSet1.Caption := GetText(SECTION, 'pair1');
    LblSet2.Caption := GetText(SECTION, 'pair2');
    LblSet3.Caption := GetText(SECTION, 'pair3');
    LblSet4.Caption := GetText(SECTION, 'pair4');
    LblSet5.Caption := GetText(SECTION, 'pair5');
    LblSet1Value.Caption := GetText(SECTION, 'empty');
    LblSet2Value.Caption := GetText(SECTION, 'empty');
    LblSet3Value.Caption := GetText(SECTION, 'empty');
    LblSet4Value.Caption := GetText(SECTION, 'empty');
    LblSet5Value.Caption := GetText(SECTION, 'empty');
    BtnAdd.Caption := GetText(SECTION, 'addpair');
    BtnClear.Caption := GetText(SHARED_SECTION, 'clear');
    BtnContinue.Caption := GetText(SHARED_SECTION, 'continue');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'help');
    BtnCancel.Caption := GetText(SHARED_SECTION, 'cancel');

  end;
end;

end.


