{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitDlgPeriod;

{< Dialog for a period.}

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, ExtCtrls, Forms, Graphics, HelpIntfs, StdCtrls, SysUtils, unitcentralcontroller,
  unitdomainxchg, UnitTranslation, UnitValidation;

type

  { Form with dialogue to enter a period }
  TFormDlgPeriod = class(TForm)
    BtnCancel: TButton;
    BtnContinue: TButton;
    BtnHelp: TButton;
    CbCalendar: TComboBox;
    LblCalendar: TLabel;
    LblEditEndDate: TLabeledEdit;
    LblEditStartDate: TLabeledEdit;
    LblTitle: TLabel;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnContinueClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LblEditEndDateEditingDone(Sender: TObject);
    procedure LblEditStartDateEditingDone(Sender: TObject);
  private
    DateTimeValidation: TDateTimeValidation;
    CenCon: TCenCon;
    StateMachine: TFinStateMachine;
    Rosetta: TRosetta;
    procedure CheckDateSequence;
    procedure CheckStatus;
    function CreatePeriod: TPeriod;
    procedure ProcessEndDate;
    procedure ProcessStartDate;
    procedure SaveState;
    procedure Populate;

  public

  end;

var
  FormDlgPeriod: TFormDlgPeriod;

implementation

{$R *.lfm}

uses
  UnitConst;

const
  SECTION = 'formdlgperiod';

var
  ValidatedStartDate, ValidatedEndDate: TValidatedDate;
  StartJD, EndJD: double;
  StartDateOk, EndDateOk, DateSequenceOk: boolean;

procedure TFormDlgPeriod.FormCreate(Sender: TObject);
begin
  CenCon := TCenCon.Create;
  StateMachine := TFinStateMachine.Create;
  Rosetta := TRosetta.Create;
  DateTimeValidation := TDateTimeValidation.Create;
end;

procedure TFormDlgPeriod.FormShow(Sender: TObject);
begin
  Populate;
end;

procedure TFormDlgPeriod.BtnContinueClick(Sender: TObject);
begin
  Close;
  SaveState;
  StateMachine.ChangeState(PeriodDefined);
end;

procedure TFormDlgPeriod.BtnCancelClick(Sender: TObject);
begin
  Close;
  StateMachine.ChangeState(Cancel);
end;

procedure TFormDlgPeriod.BtnHelpClick(Sender: TObject);
var
  HelpText: string;
begin
  if Rosetta.CurrentLanguage = 'en' then HelpText := 'html/en_dlgperiod.html'
  else
    HelpText := 'html/nl_dlgperiod.html';
  ShowHelpOrErrorForKeyword(EmptyStr, HelpText);
end;

procedure TFormDlgPeriod.SaveState;
begin
  StateMachine.Period := CreatePeriod;
end;

procedure TFormDlgPeriod.Populate;
begin
  with Rosetta do begin
    FormDlgPeriod.Caption := GetText(SECTION, 'title');
    LblTitle.Caption := GetText(SECTION, 'formtitle');
    LblCalendar.Caption := GetText(SECTION, 'calendar');
    LblEditStartDate.EditLabel.Caption := GetText(SECTION, 'startdate');
    LblEditEndDate.EditLabel.Caption := GetText(SECTION, 'enddate');
    BtnContinue.Caption := GetText(SHARED_SECTION, 'continue');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'help');
    BtnCancel.Caption := GetText(SHARED_SECTION, 'cancel');
    CbCalendar.Items.Clear;
    CbCalendar.Items.Add(GetText(SECTION, 'calgreg'));
    CbCalendar.Items.Add(GetText(SECTION, 'caljul'));
    CbCalendar.ItemIndex := 0;
  end;
  LblEditStartDate.Text := '';
  LblEditEndDate.Text := '';
end;

procedure TFormDlgPeriod.LblEditEndDateEditingDone(Sender: TObject);
begin
  ProcessEndDate;
  CheckDateSequence;
  CheckStatus;
end;


procedure TFormDlgPeriod.LblEditStartDateEditingDone(Sender: TObject);
begin
  ProcessStartDate;
  CheckDateSequence;
  CheckStatus;
end;


procedure TFormDlgPeriod.ProcessStartDate;
var
  Calendar: integer;
begin
  LblEditStartDate.Color := clDefault;
  if CbCalendar.ItemIndex = 1 then Calendar := 0
  else
    Calendar := 1;
  ValidatedStartDate := DateTimeValidation.CheckDate(LblEditStartDate.Text, Calendar);
  if not ValidatedStartDate.IsValid then begin
    LblEditStartDate.Color := clYellow;
    StartDateOk := False;
  end else begin
    StartJD := ValidatedStartDate.JulianDay;
    LblEditStartDate.Color := clDefault;
    StartDateOk := True;
  end;
end;

procedure TFormDlgPeriod.ProcessEndDate;
var
  Calendar: integer;
begin
  LblEditEndDate.Color := clDefault;
  if CbCalendar.ItemIndex = 1 then Calendar := 0
  else
    Calendar := 1;
  ValidatedEndDate := DateTimeValidation.CheckDate(LblEditEndDate.Text, Calendar);
  if not ValidatedEndDate.IsValid then begin
    LblEditEndDate.Color := clYellow;
    EndDateOk := False;
  end else begin
    EndJD := ValidatedEndDate.JulianDay;
    LblEditEndDate.Color := clDefault;
    EndDateOk := True;
  end;
end;



procedure TFormDlgPeriod.CheckDateSequence;
begin
  if ValidatedStartDate.IsValid and ValidatedEndDate.IsValid then begin
    DateSequenceOk := True;
    LblEditStartDate.Color := clDefault;
    LblEditEndDate.Color := clDefault;
    if StartJD > EndJD then begin
      DateSequenceOk := False;
      LblEditStartDate.Color := clYellow;
      LblEditEndDate.Color := clYellow;
    end;
  end;
end;


function TFormDlgPeriod.CreatePeriod: TPeriod;
var
  Period: Tperiod;
begin
  Period.StartDate := ValidatedStartDate;
  Period.EndDate := ValidatedEndDate;
  Result := Period;
end;

procedure TFormDlgPeriod.CheckStatus;
begin
  BtnContinue.Enabled := (StartDateOk and EndDateOk and DateSequenceOk);
end;



end.
