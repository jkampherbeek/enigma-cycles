{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitdlgsinglecp;

{< Dialog for celestial points.}

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, Forms, Graphics, HelpIntfs, StdCtrls, SysUtils,
  unitcentralcontroller, unitdomainxchg, unittranslation;

type

  { Form for the selection for max 5 single celestial points }
  TFormDlgSingleCP = class(TForm)
    BtnCancel: TButton;
    BtnContinue: TButton;
    BtnHelp: TButton;
    CbCeres: TCheckBox;
    CbChiron: TCheckBox;
    CbEarth: TCheckBox;
    CbEris: TCheckBox;
    CbHaumea: TCheckBox;
    CbHuya: TCheckBox;
    CbIxion: TCheckBox;
    CbJuno: TCheckBox;
    CbJupiter: TCheckBox;
    CbMakeMake: TCheckBox;
    CbMars: TCheckBox;
    CbMeanApogee: TCheckBox;
    CbMeanNode: TCheckBox;
    CbMercury: TCheckBox;
    CbMoon: TCheckBox;
    CbNeptune: TCheckBox;
    CbNessus: TCheckBox;
    CbOrcus: TCheckBox;
    CbOscNode: TCheckBox;
    CbPallas: TCheckBox;
    CbPholus: TCheckBox;
    CbPluto: TCheckBox;
    CbQuaoar: TCheckBox;
    CbSaturn: TCheckBox;
    CbSedna: TCheckBox;
    CbSun: TCheckBox;
    CbUranus: TCheckBox;
    CbVaruna: TCheckBox;
    CbVenus: TCheckBox;
    CbVesta: TCheckBox;
    LblTitle: TLabel;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnContinueClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    CenCon: TCenCon;
    StateMachine: TFinStateMachine;
    Rosetta: TRosetta;
    procedure CheckObserverPos;
    procedure CheckPeriod;
    procedure DefineCPs;
    function PeriodSupported(CpIndex: integer; StartJd, EndJd: double): boolean;
    procedure SaveState;
    procedure Populate;
  public

  end;

var
  FormDlgSingleCP: TFormDlgSingleCP;

implementation

{$R *.lfm}

uses
  UnitConst;

const
  SECTION = 'formdlgsinglecp';

var
  AllCPs, SelectedCPs: TCelPointSpecArray;
  NoErrors: boolean;
  ErrorNrOfCPs: string;

procedure TFormDlgSingleCP.FormCreate(Sender: TObject);
begin
  CenCon := TCenCon.Create;
  StateMachine := TFinStateMachine.Create;
  Rosetta := TRosetta.Create;
end;

procedure TFormDlgSingleCP.FormShow(Sender: TObject);
begin
  Populate;
  DefineCPs;
end;

procedure TFormDlgSingleCP.BtnContinueClick(Sender: TObject);
begin
  SaveState;
  if NoErrors then begin
    Close;
    StateMachine.ChangeState(SingleCPDefined);
  end;
end;

procedure TFormDlgSingleCP.BtnCancelClick(Sender: TObject);
begin
  Close;
  StateMachine.ChangeState(Cancel);
end;

procedure TFormDlgSingleCP.BtnHelpClick(Sender: TObject);
var
  HelpText: string;
begin
  if Rosetta.CurrentLanguage = 'en' then HelpText := 'html/en_dlgsinglecp.html'
  else
    HelpText := 'html/nl_dlgsinglecp.html';
  ShowHelpOrErrorForKeyword(EmptyStr, HelpText);
end;

procedure TFormDlgSingleCP.DefineCPs;
begin
  AllCPs := CenCon.LookupValues.AllCelPoints;
  CheckObserverPos;
  CheckPeriod;
end;

procedure TFormDlgSingleCP.CheckObserverPos;
begin
  if StateMachine.ObserverPos.Identification = geocentric then CbEarth.Enabled := False;
  if StateMachine.ObserverPos.Identification = heliocentric then begin
    CbEarth.Enabled := True;
    CbSun.Enabled := False;
    CbMoon.Enabled := False;
    CbMeanNode.Enabled := False;
    CbOscNode.Enabled := False;
    CbMeanApogee.Enabled := False;
  end;
end;

function TFormDlgSingleCP.PeriodSupported(CpIndex: integer; StartJd, EndJd: double): boolean;
begin
  Result := ((AllCPs[CpIndex].FirstJd <= StartJd) and (AllCPs[CpIndex].LastJD >= EndJd));
end;

procedure TFormDlgSingleCP.CheckPeriod;
var
  StartJd, EndJD: double;
begin
  StartJd := StateMachine.Period.StartDate.JulianDay;
  EndJd := StateMachine.Period.EndDate.JulianDay;
  CbChiron.Enabled := PeriodSupported(13, StartJd, EndJd);
  CbPholus.Enabled := PeriodSupported(14, StartJd, EndJd);
  CbCeres.Enabled := PeriodSupported(15, StartJd, EndJd);
  CbVesta.Enabled := PeriodSupported(18, StartJd, EndJd);
  CbNessus.Enabled := PeriodSupported(19, StartJd, EndJd);
  CbHuya.Enabled := PeriodSupported(20, StartJd, EndJd);
  CbMakeMake.Enabled := PeriodSupported(21, StartJd, EndJd);
  CbHaumea.Enabled := PeriodSupported(22, StartJd, EndJd);
  CbEris.Enabled := PeriodSupported(23, StartJd, EndJd);
  CbIxion.Enabled := PeriodSupported(24, StartJd, EndJd);
  CbOrcus.Enabled := PeriodSupported(25, StartJd, EndJd);
  CbQuaoar.Enabled := PeriodSupported(26, StartJd, EndJd);
  CbSedna.Enabled := PeriodSupported(27, StartJd, EndJd);
  CbVaruna.Enabled := PeriodSupported(28, StartJd, EndJd);
end;

procedure TFormDlgSingleCP.SaveState;
var
  i, Count: integer;
begin
  SelectedCPs := AllCPs;
  SelectedCPs[0].Selected := CbSun.Checked;
  SelectedCPs[1].Selected := CbMoon.Checked;
  SelectedCPs[2].Selected := CbMercury.Checked;
  SelectedCPs[3].Selected := CbVenus.Checked;
  SelectedCPs[4].Selected := CbMars.Checked;
  SelectedCPs[5].Selected := CbJupiter.Checked;
  SelectedCPs[6].Selected := CbSaturn.Checked;
  SelectedCPs[7].Selected := CbUranus.Checked;
  SelectedCPs[8].Selected := CbNeptune.Checked;
  SelectedCPs[9].Selected := CbPluto.Checked;
  SelectedCPs[10].Selected := CbMeanNode.Checked;
  SelectedCPs[11].Selected := CbOscNode.Checked;
  SelectedCPs[12].Selected := CbEarth.Checked;
  SelectedCPs[13].Selected := CbChiron.Checked;
  SelectedCPs[14].Selected := CbPholus.Checked;
  SelectedCPs[15].Selected := CbCeres.Checked;
  SelectedCPs[16].Selected := CbPallas.Checked;
  SelectedCPs[17].Selected := CbJuno.Checked;
  SelectedCPs[18].Selected := CbVesta.Checked;
  SelectedCPs[19].Selected := CbNessus.Checked;
  SelectedCPs[20].Selected := CbHuya.Checked;
  SelectedCPs[21].Selected := CbMakeMake.Checked;
  SelectedCPs[22].Selected := CbHaumea.Checked;
  SelectedCPs[23].Selected := CbEris.Checked;
  SelectedCPs[24].Selected := CbIxion.Checked;
  SelectedCPs[25].Selected := CbOrcus.Checked;
  SelectedCPs[26].Selected := CbQuaoar.Checked;
  SelectedCPs[27].Selected := CbSedna.Checked;
  SelectedCPs[28].Selected := CbVaruna.Checked;
  SelectedCPs[29].Selected := CbMeanApogee.Checked;
  Count := 0;
  for i := 0 to 30 do if SelectedCPs[i].Selected then Inc(Count);
  if (Count > 5) or (Count < 1) then begin
    ShowMessage(ErrorNrOfCPs);
    NoErrors := False;
  end else begin
    StateMachine.SingleCPs := SelectedCPs;
    NoErrors := True;
  end;
end;

procedure TFormDlgSingleCP.Populate;
begin
  with Rosetta do begin
    FormDlgSingleCP.Caption := GetText(SECTION, 'formtitle');
    LblTitle.Caption := GetText(SECTION, 'title');
    BtnContinue.Caption := GetText(SHARED_SECTION, 'continue');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'help');
    BtnCancel.Caption := GetText(SHARED_SECTION, 'cancel');
    ErrorNrOfCPs := GetText(SECTION, 'errornrofcps');
    CbCeres.Caption := GetText(LOOKUP_SECTION, 'celpointceres');
    CbChiron.Caption := GetText(LOOKUP_SECTION, 'celpointchiron');
    CbEarth.Caption := GetText(LOOKUP_SECTION, 'celpointearth');
    CbEris.Caption := GetText(LOOKUP_SECTION, 'celpointeris');
    CbHaumea.Caption := GetText(LOOKUP_SECTION, 'celpointhaumea');
    CbHuya.Caption := GetText(LOOKUP_SECTION, 'celpointhuya');
    CbIxion.Caption := GetText(LOOKUP_SECTION, 'celpointixion');
    CbJuno.Caption := GetText(LOOKUP_SECTION, 'celpointjuno');
    CbJupiter.Caption := GetText(LOOKUP_SECTION, 'celpointjupiter');
    CbMakeMake.Caption := GetText(LOOKUP_SECTION, 'celpointmakemake');
    CbMars.Caption := GetText(LOOKUP_SECTION, 'celpointmars');
    CbMeanApogee.Caption := GetText(LOOKUP_SECTION, 'celpointmeanapogee');
    CbMeanNode.Caption := GetText(LOOKUP_SECTION, 'celpointmeannode');
    CbMercury.Caption := GetText(LOOKUP_SECTION, 'celpointmercury');
    CbMoon.Caption := GetText(LOOKUP_SECTION, 'celpointmoon');
    CbNeptune.Caption := GetText(LOOKUP_SECTION, 'celpointneptune');
    CbNessus.Caption := GetText(LOOKUP_SECTION, 'celpointnessus');
    CbOrcus.Caption := GetText(LOOKUP_SECTION, 'celpointorcus');
    CbOscNode.Caption := GetText(LOOKUP_SECTION, 'celpointoscnode');
    CbPallas.Caption := GetText(LOOKUP_SECTION, 'celpointpallas');
    CbPholus.Caption := GetText(LOOKUP_SECTION, 'celpointpholus');
    CbPluto.Caption := GetText(LOOKUP_SECTION, 'celpointpluto');
    CbQuaoar.Caption := GetText(LOOKUP_SECTION, 'celpointquaoar');
    CbSaturn.Caption := GetText(LOOKUP_SECTION, 'celpointsaturn');
    CbSedna.Caption := GetText(LOOKUP_SECTION, 'celpointsedna');
    CbSun.Caption := GetText(LOOKUP_SECTION, 'celpointsun');
    CbUranus.Caption := GetText(LOOKUP_SECTION, 'celpointuranus');
    CbVaruna.Caption := GetText(LOOKUP_SECTION, 'celpointvaruna');
    CbVenus.Caption := GetText(LOOKUP_SECTION, 'celpointvenus');
    CbVesta.Caption := GetText(LOOKUP_SECTION, 'celpointvesta');
  end;
  CbCeres.Checked := False;
  CbChiron.Checked := False;
  CbEarth.Checked := False;
  CbEris.Checked := False;
  CbHaumea.Checked := False;
  CbHuya.Checked := False;
  CbIxion.Checked := False;
  CbJuno.Checked := False;
  CbJupiter.Checked := False;
  CbMakeMake.Checked := False;
  CbMars.Checked := False;
  CbMeanApogee.Checked := False;
  CbMeanNode.Checked := False;
  CbMercury.Checked := False;
  CbMoon.Checked := False;
  CbNeptune.Checked := False;
  CbNessus.Checked := False;
  CbOrcus.Checked := False;
  CbOscNode.Checked := False;
  CbPallas.Checked := False;
  CbPholus.Checked := False;
  CbPluto.Checked := False;
  CbQuaoar.Checked := False;
  CbSaturn.Checked := False;
  CbSedna.Checked := False;
  CbSun.Checked := False;
  CbUranus.Checked := False;
  CbVaruna.Checked := False;
  CbVenus.Checked := False;
  CbVesta.Checked := False;
end;

end.
