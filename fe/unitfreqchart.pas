{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitfreqchart;

{< Shows chart with frequencies.}

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, ExtCtrls, Forms, Graphics, HelpIntfs,
  StdCtrls, SysUtils, TAGraph, TAGUIConnectorBGRA, TASeries, unitcentralcontroller, unitdomainxchg,
  UnitTranslation, UnitUiUtils;

type

  { Form to show chart with frequencies }
  TFormFreqChart = class(TForm)
    BtnClose: TButton;
    BtnHelp: TButton;
    BtnExport: TButton;
    ChartFreq: TChart;
    ChartFreqBarSeries: TBarSeries;
    ChartGUIConnectorBGRA: TChartGUIConnectorBGRA;
    LblAyanamsha: TLabel;
    LblCelPoints: TLabel;
    LblCoordinate: TLabel;
    LblInterval: TLabel;
    LblObserverPos: TLabel;
    LblPeriod: TLabel;
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnExportClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    AllXValues, AllYValues: array of integer;
    StateMachine: TFinStateMachine;
    Rosetta: TRosetta;
    UiTexts: TUiTexts;
    BucketSize: integer;
    procedure Populate;
    procedure DefineLabelsForInfo;
    procedure DefineLabelForCelPoints;
    procedure ReadValues;
    procedure PopulateFreqChart;
  public

  end;

var
  FormFreqChart: TFormFreqChart;

implementation

{$R *.lfm}

uses
  UnitConst;

const
  SECTION = 'formfreqchart';

{ TFormFreqChart }

procedure TFormFreqChart.FormCreate(Sender: TObject);
begin
  StateMachine := TFinStateMachine.Create;
  Rosetta := TRosetta.Create;
  UiTexts := TUiTexts.Create;
end;

procedure TFormFreqChart.FormShow(Sender: TObject);
begin
  Populate;
  ChartFreq.Title.Text[0] := StateMachine.CycleType.Name;
  DefineLabelsForInfo;
  DefineLabelForCelPoints;
  BucketSize := 1;
  ReadValues;
  PopulateFreqChart;
end;

procedure TFormFreqChart.Populate;
begin
  with Rosetta do begin
    FormFreqChart.Caption := GetText(SECTION, 'formtitle');
    ChartFreq.LeftAxis.Title.Caption := GetText(SECTION, 'values');
    ChartFreq.BottomAxis.Title.Caption := GetText(SECTION, 'xtitle');
    BtnExport.Caption := GetText(SECTION, 'export');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'help');
    BtnClose.Caption := GetText(SHARED_SECTION, 'close');
  end;
end;

procedure TFormFreqChart.BtnHelpClick(Sender: TObject);
var
  HelpText: string;
begin
  if Rosetta.CurrentLanguage = 'en' then HelpText := 'html/en_freqchart.html'
  else
    HelpText := 'html/nl_freqchart.html';
  ShowHelpOrErrorForKeyword(EmptyStr, HelpText);
end;

procedure TFormFreqChart.BtnCloseClick(Sender: TObject);
begin
  Close;
  StateMachine.ChangeState(CloseFreqChart);
end;

procedure TFormFreqChart.BtnExportClick(Sender: TObject);
begin
  ChartFreq.SaveToFile(TPortableNetworkGraphic, './data/FrequencyChart.png');
  ShowMessage(Rosetta.GetText(SECTION, 'pngsaved'));
end;

procedure TFormFreqChart.PopulateFreqChart;
var
  i: integer;
begin
  ChartFreqBarSeries.Clear;
  for i := 0 to Length(AllXValues) - 1 do ChartFreqBarSeries.AddXY(AllXValues[i], AllYValues[i]);
end;



procedure TFormFreqChart.ReadValues;
var
  DataList, SplittedLine: TStringList;
  i, n, xValue, yValue: integer;
begin
  DataList := TStringList.Create;
  SplittedLine := TStringList.Create;
  try
    DataList.LoadFromFile(DATA_PATH + FREQFILENAME);
    n := DataList.Count;
    SetLength(AllXValues, n - 1);  // 1 less as header line will be skipped
    SetLength(AllYValues, n - 1);
    SplittedLine.Delimiter := ';';
    SplittedLine.StrictDelimiter := True;
    for i := 1 to n - 1 do begin    // skip first line
      SplittedLine.DelimitedText := DataList[i];
      xValue := SplittedLine[0].toInteger;
      yValue := SplittedLine[1].toInteger;
      AllXValues[i - 1] := xValue;
      AllYValues[i - 1] := yValue;
    end;
  finally
  end;
end;

procedure TFormFreqChart.DefineLabelsForInfo;
begin
  with Rosetta do begin
    LblCoordinate.Caption := StateMachine.Coordinate.Name;
    LblAyanamsha.Caption := GetText(SECTION, 'ayanamsha') + ': ' + StateMachine.Ayanamsha.Name;
    LblObserverPos.Caption := StateMachine.ObserverPos.Name;
    LblPeriod.Caption := UiTexts.TextForPeriod(StateMachine.StartDate, StateMachine.EndDate);
    LblInterval.Caption := GetText(SECTION, 'interval') + ': ' + (StateMachine.Interval / 10).toString() +
      GetText(SECTION, 'intervalparts');
  end;
end;

procedure TFormFreqChart.DefineLabelForCelPoints;
begin
  if (StateMachine.CycleType.Identification = TEnumCycleType.position) then LblCelPoints.Caption :=
      UiTexts.TextForCelPoints(StateMachine.SingleCPs)
  else
    LblCelPoints.Caption := UiTexts.TextForCelPointPairs(StateMachine.PairedCPs);
end;


end.


