{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitlinechart;

{< Shows linechart of cycle(s).}

{$mode objfpc}{$H+}
{$WARN 6058 off : Call to subroutine "$1" marked as inline is not inlined}
interface

uses
  Classes, Controls, Dialogs, Forms, Graphics, StdCtrls, SysUtils, HelpIntfs,
  TACustomSeries, TAGraph, TALegend, TASeries, TATools, TAGUIConnectorBGRA,
  unitcentralcontroller, unitdomainxchg, UnitTranslation, UnitUiUtils;

type

  { Form that shows linechart }
  TFormLineChart = class(TForm)
    BtnClose: TButton;
    BtnExport: TButton;
    BtnHelp: TButton;
    Chart1: TChart;
    ChartGUIConnectorBGRA: TChartGUIConnectorBGRA;
    LblAyanamsha: TLabel;
    LblCoordinate: TLabel;
    LblInterval: TLabel;
    LblObserverPos: TLabel;
    LblPeriod: TLabel;
    Series1: TLineSeries;
    Series2: TLineSeries;
    Series3: TLineSeries;
    Series4: TLineSeries;
    Series5: TLineSeries;
    procedure BtnExportClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
  private
    StateMachine: TFinStateMachine;
    Rosetta: TRosetta;
    UiTexts: TUiTexts;
    PrevValues: TDoubleArray;
    procedure AddDataAndCheckOverflow(Series: TLineSeries; DateText: string; Value, PrevValue: double);
    procedure Populate;
    procedure PopulateSeries;
    procedure DefineLabelsForInfo;
    procedure ConstructSeriesTitles(SplittedLine: TStringList; Cardinality: integer; Paired: boolean);
    procedure ConstructSeriesData(SplittedLine: TStringList; Cardinality: integer; Paired: boolean);
    function ConstructDate(DateText: string): TDateTime;
  public

  end;

var
  FormLineChart: TFormLineChart;

implementation

{$R *.lfm}

uses
  DateUtils, Math, UnitConst;

const
  SECTION = 'formlinechart';

{ TFormLineChart }

procedure TFormLineChart.FormCreate(Sender: TObject);
begin
  StateMachine := TFinStateMachine.Create;
  Rosetta := TRosetta.Create;
  PrevValues := TDoubleArray.Create;
end;

procedure TFormLineChart.FormShow(Sender: TObject);
begin
  Populate;
  Series1.Clear;
  Series2.Clear;
  Series3.Clear;
  Series4.Clear;
  Series5.Clear;
  Chart1.BottomAxis.Marks.Source := Series1.ListSource;
  PopulateSeries;
end;

procedure TFormLineChart.Populate;
begin
  with Rosetta do begin
    FormLineChart.Caption := GetText(SECTION, 'formtitle');
    Chart1.LeftAxis.Title.Caption := GetText(SECTION, 'degreeschart');
    Chart1.BottomAxis.Title.Caption := GetText(SECTION, 'periodchart');
    BtnExport.Caption:= GetText(SECTION, 'export');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'help');
    BtnClose.Caption := GetText(SHARED_SECTION, 'close');
  end;
end;

procedure TFormLineChart.BtnHelpClick(Sender: TObject);
var
  HelpText: string;
begin
  if Rosetta.CurrentLanguage = 'en' then HelpText := 'html/en_linechart.html'
  else
    HelpText := 'html/nl_linechart.html';
  ShowHelpOrErrorForKeyword(EmptyStr, HelpText);
end;

procedure TFormLineChart.BtnCloseClick(Sender: TObject);
begin
  Close;
  StateMachine.ChangeState(CloseLineChart);
end;

procedure TFormLineChart.BtnExportClick(Sender: TObject);
begin
  Chart1.SaveToFile(TPortableNetworkGraphic, './data/LineChart.png');
  ShowMessage(Rosetta.GetText(SECTION, 'pngsaved'));
end;

procedure TFormLineChart.PopulateSeries;
var
  DataList, SplittedLine: TStringList;
  i, n, Cardinality: integer;
  Paired: boolean;
begin
  Paired := StateMachine.CycleType.Identification <> TEnumCycleType.position;
  DataList := TStringList.Create;
  SplittedLine := TStringList.Create;
  try
    DataList.LoadFromFile(DATA_PATH + POSFILENAME);
    DefineLabelsForInfo;
    n := DataList.Count;
    SplittedLine.Delimiter := ';';
    SplittedLine.StrictDelimiter := True;
    SplittedLine.DelimitedText := DataList[0];
    Cardinality := SplittedLine.Count - 3;
    if (Paired) then Cardinality := Cardinality div 3;
    for i := 0 to n - 1 do begin
      SplittedLine.DelimitedText := DataList[i];
      if (i = 0) then ConstructSeriesTitles(SplittedLine, Cardinality, Paired)
      else
        ConstructSeriesData(SplittedLine, Cardinality, Paired);
    end;
  finally
  end;
end;

procedure TFormLineChart.DefineLabelsForInfo;
begin
  LblCoordinate.Caption := StateMachine.Coordinate.Name;
  LblAyanamsha.Caption := 'Ayanamsha: ' + StateMachine.Ayanamsha.Name;
  LblObserverPos.Caption := StateMachine.ObserverPos.Name;
  LblPeriod.Caption := UiTexts.TextForPeriod(StateMachine.StartDate, StateMachine.EndDate);
  LblInterval.Caption := 'Interval: ' + (StateMachine.Interval / 10).toString();
end;

procedure TFormLineChart.ConstructSeriesTitles(SplittedLine: TStringList; Cardinality: integer; Paired: boolean);
var
  i: integer;
  Series: TLineSeries;
begin
  for i := 0 to (Cardinality - 1) do begin
    case i of
      0: Series := Series1;
      1: Series := Series2;
      2: Series := Series3;
      3: Series := Series4;
      4: Series := Series5;
    end;
    if (Paired) then Series.Title := SplittedLine[2 + i * 3] + '-' + SplittedLine[3 + i * 3]
    else
      Series.Title := SplittedLine[i + 2];
  end;
  for i := Cardinality to 5 do case i of
      1: Series2.ShowInLegend := False;
      2: Series3.ShowInLegend := False;
      3: Series4.ShowInLegend := False;
      4: Series5.ShowInLegend := False;
    end;
end;

procedure TFormLineChart.ConstructSeriesData(SplittedLine: TStringList; Cardinality: integer; Paired: boolean);
var
  ValueText, DateText: string;
  i: integer;
  Value: double;
  Series: TLineSeries;
begin
  SetLength(PrevValues, 5);
  DateText := trim(SplittedLine[0]);
  for i := 0 to (Cardinality - 1) do begin
    case i of
      0: Series := Series1;
      1: Series := Series2;
      2: Series := Series3;
      3: Series := Series4;
      4: Series := Series5;
    end;
    if (Paired) then begin
      ValueText := trim(SplittedLine[4 + i * 3]);
      Value := ValueText.ToDouble;
      Series.Add(Value, DateText);
    end else begin
      ValueText := trim(SplittedLine[2 + i]);
      Value := ValueText.ToDouble;
      AddDataAndCheckOverflow(Series, DateText, Value, PrevValues[i]);
      PrevValues[i] := Value;
    end;
  end;
end;

procedure TFormLineChart.AddDataAndCheckOverflow(Series: TLineSeries; DateText: string; Value, PrevValue: double);
begin
  if ((abs(Value - PrevValue)) < 300.0) then Series.Add(Value, DateText)
  else
    Series.Add(NaN, DateText);
end;

function TFormLineChart.ConstructDate(DateText: string): TDateTime;
var
  Year, Month, Day: word;
begin
  Year := StrToInt(Copy(DateText, 1, 4));
  Month := StrToInt(Copy(DateText, 6, 2));
  Day := StrToInt(Copy(DateText, 9, 2));
  Result := EncodeDate(Year, Month, Day);
end;

end.
