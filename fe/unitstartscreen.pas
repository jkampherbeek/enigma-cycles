{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitstartscreen;

{< Startup form.}

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, ExtCtrls, Forms, Graphics, HelpIntfs, LazHelpHTML, Menus, StdCtrls,
  SysUtils, UnitAbout, UnitCentralController, UnitMain, UnitTranslation, UnitUiUtils;

type

  { Form with startup screen }
  TFormStartScreen = class(TForm)
    BtnExist: TButton;
    BtnExit: TButton;
    BtnHelp: TButton;
    BtnNew: TButton;
    CycleImage: TImage;
    HTMLBrowserHelpViewer: THTMLBrowserHelpViewer;
    HTMLHelpDatabase: THTMLHelpDatabase;
    LblCycles: TLabel;
    LblEnigma: TLabel;
    LblVersion: TLabel;
    StartMainMenu: TMainMenu;
    MiCycle: TMenuItem;
    MiAbout: TMenuItem;
    MiNew: TMenuItem;
    MiExist: TMenuItem;
    MiExit: TMenuItem;
    MiLang: TMenuItem;
    MiLangEn: TMenuItem;
    MiLangNl: TMenuItem;
    MiHelp: TMenuItem;
    MiScreenHelp: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ExistClick(Sender: TObject);
    procedure ExitClick(Sender: TObject);
    procedure HelpClick(Sender: TObject);
    procedure MiAboutClick(Sender: TObject);
    procedure MiLangEnClick(Sender: TObject);
    procedure MiLangNlClick(Sender: TObject);
    procedure NewClick(Sender: TObject);
  private
    UiFiles: TUiFiles;
    Rosetta: TRosetta;
    Cencon: TCencon;
    procedure Populate;
    procedure SwitchLanguage(lang: string);
  public

  end;

var
  FormStartScreen: TFormStartScreen;

implementation

{$R *.lfm}

uses
  UnitConst, UnitLog;

const
  SECTION = 'formstartscreen';

var
  Logger: TLogger;

{ TFormStartScreen }

procedure TFormStartScreen.FormCreate(Sender: TObject);
begin
  Rosetta := TRosetta.Create;
  Cencon := TCencon.Create;
  Logger := TLogger.Create;
  Logger.Log('New session started');
  UiFiles := TUiFiles.Create;
end;

procedure TFormStartScreen.FormShow(Sender: TObject);
begin
  Populate;
  BtnExist.Enabled := UiFiles.DataForCyclesAvailable;
  MiExist.Enabled := UiFiles.DataForCyclesAvailable;
end;

procedure TFormStartScreen.Populate;
begin
  with Rosetta do begin
    FormStartScreen.Caption := GetText(SECTION, 'formtitle');
    LblEnigma.Caption := GetText(SECTION, 'title');
    LblCycles.Caption := GetText(SECTION, 'apptitle');
    LblVersion.Caption := GetText(SECTION, 'version');
    BtnExist.Caption := GetText(SECTION, 'existing');
    BtnNew.Caption := GetText(SECTION, 'new');
    BtnExit.Caption := GetText(SHARED_SECTION, 'exit');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'help');
    MiCycle.Caption := GetText(SECTION, 'micycles');
    MiNew.Caption := GetText(SECTION, 'new');
    MiExist.Caption := GetText(SECTION, 'existing');
    MiExit.Caption := GetText(SHARED_SECTION, 'exit');
    MiLang.Caption := GetText(SECTION, 'milang');
    MiLangEn.Caption := GetText(SECTION, 'milangen');
    MiLangNl.Caption := GetText(SECTION, 'milangnl');
    MiHelp.Caption := GetText(SHARED_SECTION, 'help');
    MiScreenHelp.Caption := GetText(SECTION, 'miscreenhelp');
    MiAbout.Caption := GetText(SECTION, 'miabout');
  end;
end;

procedure TFormStartScreen.SwitchLanguage(lang: string);
begin
  Rosetta.ChangeLanguage(lang);
  CenCon.LookupValues.DefineValues;
  Logger.log('Switched language to: ' + lang);
  Populate;
end;

procedure TFormStartScreen.NewClick(Sender: TObject);
begin
  FormMain.FinStateMachine.StatusInit := True;
  FormMain.FinStateMachine.ChangeState(NewCycle);
end;

procedure TFormStartScreen.ExistClick(Sender: TObject);
begin
  FormMain.FinStateMachine.ChangeState(ExistingCycle);
end;

procedure TFormStartScreen.ExitClick(Sender: TObject);
begin
  Close;
end;

procedure TFormStartScreen.HelpClick(Sender: TObject);
var
  HelpText: string;
begin
  if Rosetta.CurrentLanguage = 'en' then HelpText := 'html/en_start.html'
  else
    HelpText := 'html/nl_start.html';
  ShowHelpOrErrorForKeyword(EmptyStr, HelpText);
end;

procedure TFormStartScreen.MiAboutClick(Sender: TObject);
begin
  FormAbout.ShowModal;
end;

procedure TFormStartScreen.MiLangEnClick(Sender: TObject);
begin
  SwitchLanguage('en');
end;

procedure TFormStartScreen.MiLangNlClick(Sender: TObject);
begin
  SwitchLanguage('nl');
end;

end.
