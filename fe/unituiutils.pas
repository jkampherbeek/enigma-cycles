{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitUiUtils;

{< Utilities to support Forms.}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, unitdomainxchg, UnitConversions, UnitTranslation;

type

  { Construct texts for Forms. }
  TUiTexts = class
  private
    Rosetta: TRosetta;
  public
    constructor Create;
    { Text that contains a date. }
    function TextForDate(Date: TValidatedDate): string;
    { Text that describes a period.}
    function TextForPeriod(StartDate, EndDate: TValidatedDate): string;
    { Text that describes a period.}
    function TextForPeriod(StartDate, EndDate: string): string;
    { Text with formatted list of celestial points.}
    function TextForCelPoints(CelPoints: TCelPointSpecArray): string;
    { Text with formatted list of pairs of celestial points.}
    function TextForCelPointPairs(CelPointPairs: TCelPointPairedSpecArray): string;
  end;

  { Logic related to periods. }
  TUiPeriods = class
  private
    JdConversion: TJulianDayConversion;
  public
    constructor Create;
    destructor Destroy; override;
    function DefineShortestInterval(CelObjects: TCelPointSpecArray): integer;
    function DefineShortestInterval(CelObjectPairs: TCelPointPairedSpecArray): integer;
    function DefineShortestPeriodLength(CelObjects: TCelPointSpecArray): integer;
    function DefineShortestPeriodLength(CelObjectPairs: TCelPointPairedSpecArray): integer;
    function DefineMaxFittingPeriod(InitialPeriod: TPeriod; MaxPeriodLength: integer): TPeriod;
  end;

  { File utilities for forms}
  TUiFiles = class
  public
    function DataForCyclesAvailable: boolean;
  end;

implementation

uses
  Math, UnitConst, UnitAstron;

const
  SECTION = 'uiutils';

{ TUiFiles }

function TUiFiles.DataForCyclesAvailable: boolean;
var
  DataExists, MetaExists: boolean;
begin
  DataExists:= FileExists(DATA_PATH + POSFILENAME);
  MetaExists:= FileExists(DATA_PATH + METAFILENAME);
  Result:= DataExists and MetaExists;
end;

{ TUiPeriods }

constructor TUiPeriods.Create;
begin
  JdConversion:= TJulianDayConversion.Create(TSeFrontend.Create);
end;

destructor TUiPeriods.Destroy;
begin
  FreeAndNil(JdConversion);
  inherited Destroy;
end;

function TUiPeriods.DefineShortestInterval(CelObjects: TCelPointSpecArray): integer;
var
  i, MinInterval: integer;
begin
  MinInterval := MaxInt;
  for i := 0 to Length(CelObjects) - 1 do if (CelObjects[i].Selected) then
      MinInterval := Min(CelObjects[i].Interval, MinInterval);
  Result := MinInterval;
end;

function TUiPeriods.DefineShortestInterval(CelObjectPairs: TCelPointPairedSpecArray): integer;
var
  i, MinInterval: integer;
begin
  MinInterval := MaxInt;
  for i := 0 to Length(CelObjectPairs) - 1 do begin
    MinInterval := Min(CelObjectPairs[i].FirstCP.Interval, MinInterval);
    MinInterval := Min(CelObjectPairs[i].SecondCP.Interval, MinInterval);
  end;
  Result := MinInterval;
end;

function TUiPeriods.DefineShortestPeriodLength(CelObjects: TCelPointSpecArray): integer;
var
  i, MinPeriod: integer;
begin
  MinPeriod := MaxInt;
  for i := 0 to Length(CelObjects) - 1 do begin
    if (CelOBjects[i].Selected) then MinPeriod := Min(CelObjects[i].MaxDayParts, MinPeriod);
  end;
  Result:= MinPeriod;
end;

function TUiPeriods.DefineShortestPeriodLength(CelObjectPairs: TCelPointPairedSpecArray): integer;
var
  i, MinPeriod: integer;
begin
  MinPeriod := MaxInt;
  for i := 0 to Length(CelObjectPairs) - 1 do begin
    MinPeriod := Min(CelObjectPairs[i].FirstCP.MaxDayParts, MinPeriod);
    MinPeriod := Min(CelObjectPairs[i].SecondCP.MaxDayParts, MinPeriod);
  end;
  Result:= MinPeriod;
end;

function TUiPeriods.DefineMaxFittingPeriod(InitialPeriod: TPeriod; MaxPeriodLength: integer): TPeriod;
var
  StartDate, NewEndDate: TValidatedDate;
  CorrectedDate: TSimpleDateTime;
  NewJd: double;
  Calendar: integer;
  NewPeriod: TPeriod;
begin
  StartDate:= InitialPeriod.StartDate;
  Calendar := StartDate.Calendar;
  NewJd:= StartDate.JulianDay + MaxPeriodLength;
  CorrectedDate:= JdConversion.ConvertJdToSimpleDateTime(NewJd, Calendar);
  NewEndDate.Calendar:= Calendar;
  NewEndDate.Day:= CorrectedDate.Day;
  NewEndDate.Month:= CorrectedDate.Month;
  NewEndDate.Year:= CorrectedDate.Year;
  NewEndDate.JulianDay:= NewJd;
  NewEndDate.IsValid:= true;
  NewPeriod.StartDate:= StartDate;
  NewPeriod.EndDate:= NewEndDate;
  Result:= NewPeriod;
end;

{ TUiTexts }

constructor TUiTexts.Create;
begin
  Rosetta:= TRosetta.Create;
end;

function TUiTexts.TextForDate(Date: TValidatedDate): string;
var
  y, m, d, cal: string;
begin
  y := IntToStr(Date.Year);
  m := IntToStr(Date.Month);
  d := IntToStr(Date.Day);
  if (Date.Calendar = 1) then cal := 'Greg.'
  else
    cal := 'Jul.';
  Result := y + SEP_DATE + m + SEP_DATE + d + SPACE + cal;
end;

function TUiTexts.TextForPeriod(StartDate, EndDate: TValidatedDate): string;
begin
  Result := TextforDate(StartDate) + SPACE + SEP_DIVISION + SPACE + TextForDate(EndDate);
end;

function TUiTexts.TextForPeriod(StartDate, EndDate: string): string;
begin
  Result:= StartDate + SPACE + SEP_DIVISION + SPACE + EndDate;
end;

function TUiTexts.TextForCelPoints(CelPoints: TCelPointSpecArray): string;
var
  Count, i: integer;
  sep, CpText, CpName: string;
  FirstToShow: boolean;
begin
  sep := ' - ';
  CpText := '';
  Count := 0;
  FirstToShow := True;
  for i := 0 to Length(CelPoints) - 1 do begin
    CpName := CelPoints[i].Name;
    if ((CelPoints[i].Selected) and (Length(CelPoints[i].Name) > 0)) then begin
      if (Count > 1) then begin
        CpText := CpText + LineEnding;
        Count := 1;
      end else
        Inc(Count);
      if ((i > 0) and (Count > 1) and not (FirstToShow)) then CpText := CpText + sep;
      FirstToShow := False;
      CpText := CpText + CpName;
    end;
  end;
  Result := CpText;
end;

function TUiTexts.TextForCelPointPairs(CelPointPairs: TCelPointPairedSpecArray): string;
var
  Count, i: integer;
  sep, CpText, AndTxt: string;
begin
  AndTxt:= Rosetta.GetText(SECTION, 'and');
  sep := ' - ';
  Count := 0;
  CpText := '';
  for i := 0 to Length(CelPointPairs) - 1 do if (Length(CelPointPairs[i].FirstCP.Name) > 1) then begin
      if (Count > 1) then begin
        CpText := CpText + LineEnding;
        Count := 0;
      end;
      if ((i > 0) and (Count > 0)) then CpText := CpText + sep;
      CpText := CpText + CelPointPairs[i].FirstCP.Name + ' ' + AndTxt + ' ' + CelPointPairs[i].SecondCP.Name;
      Inc(Count);
    end;
  Result := CpText;
end;


end.







