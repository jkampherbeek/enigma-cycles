{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit it_allcelpointpos;

{< Integration tests for the calculation of celestial points.}

{$mode objfpc}{$H+}

interface

uses
  Classes, fpcunit, SysUtils, testregistry, UnitApi, unitdomainxchg, UnitInit,
  UnitReqResp, UnitTestTools, UnitTranslation;

type

  TIntegerArray = array of integer;

  { Parent for testclasses for positions of celestial points. }
  ItCelPointPosParent = class(TTestCase)
  protected
    Request: TSeriesSingleRequest;
    Response: TSeriesResponse;
    Api: TSeriesAPI;
    Conv4Test: TConversions4Test;
    SelectedCelPoints: TIntegerArray;
    ResultValues: TDoubleArray;
    function CreateDate(PJd: double; PYear, PMonth, PDay, PCalendar: integer): TValidatedDate;
    function CreateRequest(PAyanamsha, PObserverPos, PCoordinate: integer;
      PSelectedCelPoints: TIntegerArray): TSeriesSingleRequest;
    function RetrieveResults: TDoubleArray;
    procedure ProcessIt; virtual; abstract;
    procedure SetUp; override;
    procedure TearDown; override;
  end;


  { Tests for geographic longitude }
  ItGeoLong = class(ItCelPointPosParent)
  protected
    procedure ProcessIt; override;
  published
    procedure TestSunToMars;
    procedure TestJupiterToPluto;
    procedure TestMeanNodeToCeres;
    procedure TestPallasToHuya;
    procedure TestMakeMakeToOrcus;
    procedure QuaOarToMeanApogee;
  end;

  { Tests for geographic latitude }
  ItGeoLat = class(ItCelPointPosParent)
  protected
    procedure ProcessIt; override;
  published
    procedure TestMoonMarsPlutoMakeMakeEris;
  end;

  { Test for right ascension }
  ItGeoRA = class(ItCelPointPosParent)
  protected
    procedure ProcessIt; override;
  published
    procedure TestSunSaturnOscnodeHaumeaMeanapogee;
  end;

  { Test for declination }
  ItGeoDecl = class(ItCelPointPosParent)
  protected
    procedure ProcessIt; override;
  published
    procedure TestMercuryUranusChironNessusIxion;
  end;

  { Test for distnace (RADV) }
  ItGeoDistance = class(ItCelPointPosParent)
  protected
    procedure ProcessIt; override;
  published
    procedure TestVenusNeptunePholusJunoEris;
  end;

  { Test for heliocentric longitude }
  ItHelioLong = class(ItCelPointPosParent)
  protected
    procedure ProcessIt; override;
  published
    procedure TestEarthUranusCeresJunoVaruna;
  end;

  { Test for heliocentric latitude }
  ItHelioLat = class(ItCelPointPosParent)
  protected
    procedure ProcessIt; override;
  published
    procedure TestMercJupPluHaumeaSedna;
  end;

  { Test for heliocentric distance (RADV)}
  ItHelioDistance = class(ItCelPointPosParent)
  protected
    procedure ProcessIt; override;
  published
    procedure TestVenusSatChiVestaQuaoar;
  end;

  { Test for sidereal positions. }
  ItSidereal = class(ItCelPointPosParent)
  protected
    procedure ProcessIt; override;
  published
    procedure TestFaganSunEris;
    procedure TestLahiriMercuryUranus;
  end;


implementation

var
  Delta: double;


{ ItCelPointPosParent }

function ItCelPointPosParent.CreateDate(PJd: double; PYear, PMonth, PDay, PCalendar: integer): TValidatedDate;
var
  ValDate: TValidatedDate;
begin
  ValDate.IsValid := True;
  ValDate.JulianDay := PJd;
  ValDate.Calendar := PCalendar;
  ValDate.Year := PYear;
  ValDate.Month := PMonth;
  ValDate.Day := PDay;
  Result := ValDate;
end;

function ItCelPointPosParent.CreateRequest(PAyanamsha, PObserverPos, PCoordinate: integer;
  PSelectedCelPoints: TIntegerArray): TSeriesSingleRequest;
var
  LookupValues: TLookupValues;
  ObserverPos: TObserverPosSpec;
  Coordinate: TCoordinateSpec;
  Ayanamsha: TAyanamshaSpec;
  CycleType: TCycleTypeSpec;
  Period: TPeriod;
  i, Interval: integer;
  CelPoints: TCelPointSpecArray;
begin
  LookupValues := TLookupValues.Create;
  Period.StartDate := CreateDate(2458849.5, 2020, 1, 1, 1);
  Period.EndDate := CreateDate(2458850.5, 2020, 1, 2, 1);
  Interval := 1;
  CycleType := LookupValues.AllCycleTypes[0];     // position
  Ayanamsha := LookupValues.AllAyanamshas[PAyanamsha];
  Coordinate := LookupValues.AllCoordinates[PCoordinate];
  ObserverPos := LookupValues.AllObserverPos[PObserverPos];
  CelPoints := LookupValues.AllCelPoints;
  for i := 0 to Length(PSelectedCelPoints) - 1 do CelPoints[PSelectedCelPoints[i]].Selected := True;
  Result := TSeriesSingleRequest.Create(Period, Interval, CycleType, Ayanamsha, Coordinate, ObserverPos, CelPoints);
end;

function ItCelPointPosParent.RetrieveResults: TDoubleArray;
var
  PosFile: TextFile;
  PosFileName, Line1, Line2: string;
  LineItems: TStringArray;
  i: integer;
  Values: array [0..4] of double;
begin
  PosFileName := './data/positions.csv';
  AssignFile(PosFile, PosFileName);
  try
    Reset(PosFile);
    ReadLn(PosFile, Line1);
    ReadLn(PosFile, Line2);
  finally
    CloseFile(PosFile);
  end;
  LineItems := Line2.Split(';');
  for i := 0 to 4 do if LineItems[i + 2] <> '' then Values[i] := StrToFloat(LineItems[i + 2])
    else
      Values[i] := 0.0;
  Result := values;
end;

procedure ItCelPointPosParent.SetUp;
var
  Rosetta: TRosetta;
begin
  Delta := 0.0001;
  Conv4Test := TConversions4Test.Create;
  Rosetta := TRosetta.Create;
  Rosetta.ChangeLanguage('en');
  Api := TSeriesAPI.Create;
  inherited SetUp;
end;

procedure ItCelPointPosParent.TearDown;
begin
  FreeAndNil(Conv4Test);
  FreeAndNil(Request);
  FreeAndNil(Api);
  inherited TearDown;
end;


{ ItGeoLong }


procedure ItGeoLong.ProcessIt;
begin
  Request := CreateRequest(0, 0, 0, SelectedCelPoints);   // ayanamsha: none, observerpos: geo, coordinate: geolong
  Response := Api.GetSeries(Request);
  ResultValues := RetrieveResults;
end;


procedure ItGeoLong.TestSunToMars;
begin
  SelectedCelPoints := [0, 1, 2, 3, 4];                    // Sun, Moon, Mercury, Venus, Mars
  ProcessIt;
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(10, 10, 0, 34.2263), ResultValues[0], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(12, 16, 8, 18.2918), ResultValues[1], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(10, 4, 22, 59.9911), ResultValues[2], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(11, 14, 24, 34.2299), ResultValues[3], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(8, 28, 23, 04.6412), ResultValues[4], Delta);
end;

procedure ItGeoLong.TestJupiterToPluto;
begin
  SelectedCelPoints := [5, 6, 7, 8, 9];                    // Jupiter, Saturn, Uranus, Neptune, Pluto
  ProcessIt;
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(10, 6, 40, 13.2704), ResultValues[0], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(10, 21, 23, 41.8949), ResultValues[1], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(2, 2, 41, 38.5796), ResultValues[2], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(12, 16, 15, 52.3261), ResultValues[3], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(10, 22, 23, 8.1905), ResultValues[4], Delta);
end;

procedure ItGeoLong.TestMeanNodeToCeres;
begin
  SelectedCelPoints := [10, 11, 13, 14, 15];                // Mean node, Osc. node. Chiron, Pholus, Ceres
  ProcessIt;
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(4, 8, 14, 21.2192), ResultValues[0], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(4, 8, 23, 15.8534), ResultValues[1], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(1, 1, 35, 46.9720), ResultValues[2], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(10, 2, 21, 44.9319), ResultValues[3], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(10, 17, 54, 22.0399), ResultValues[4], Delta);
end;

procedure ItGeoLong.TestPallasToHuya;
begin
  SelectedCelPoints := [16, 17, 18, 19, 20];                // Pallas, Juno, Vesta, Nessus, Huya
  ProcessIt;
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(9, 22, 51, 36.4290), ResultValues[0], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(7, 17, 21, 46.3713), ResultValues[1], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(2, 12, 6, 30.5751), ResultValues[2], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(12, 8, 44, 43.2506), ResultValues[3], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(9, 11, 7, 3.1495), ResultValues[4], Delta);
end;

procedure ItGeoLong.TestMakeMakeToOrcus;
begin
  SelectedCelPoints := [21, 22, 23, 24, 25];               // MakeMake, Haumea, Eris, Ixion, Orcus
  ProcessIt;
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(7, 6, 50, 56.0217), ResultValues[0], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(7, 27, 43, 53.8427), ResultValues[1], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(1, 23, 13, 49.9377), ResultValues[2], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(9, 28, 43, 56.1552), ResultValues[3], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(6, 12, 7, 49.9514), ResultValues[4], Delta);
end;

procedure ItGeoLong.QuaOarToMeanApogee;
begin
  SelectedCelPoints := [26, 27, 28, 29];                   // Quaoar, Sedna, Varuna, Mean Apogee
  ProcessIt;
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(10, 3, 12, 27.5013), ResultValues[0], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(2, 27, 4, 29.5917), ResultValues[1], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(5, 3, 24, 14.3191), ResultValues[2], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(12, 27, 3, 5.0383), ResultValues[3], Delta);
end;

{ ItGeoLat }

procedure ItGeoLat.ProcessIt;
begin
  Request := CreateRequest(0, 0, 1, SelectedCelPoints);   // ayanamsha: none, observerpos: geo, coordinate: geolat
  Response := Api.GetSeries(Request);
  ResultValues := RetrieveResults;
end;


procedure ItGeoLat.TestMoonMarsPlutoMakeMakeEris;
begin
  SelectedCelPoints := [1, 4, 9, 21, 23];                   // Moon, Mars, Pluto, MakeMake, Eris
  ProcessIt;
  AssertEquals(Conv4Test.SePositionDMS2Decimal(-1, 4, 53, 38.6909), ResultValues[0], Delta);
  AssertEquals(Conv4Test.SePositionDMS2Decimal(1, 0, 21, 41.6955), ResultValues[1], Delta);
  AssertEquals(Conv4Test.SePositionDMS2Decimal(-1, 0, 39, 10.2162), ResultValues[2], Delta);
  AssertEquals(Conv4Test.SePositionDMS2Decimal(1, 28, 5, 41.3846), ResultValues[3], Delta);
  AssertEquals(Conv4Test.SePositionDMS2Decimal(-1, 11, 46, 15.1506), ResultValues[4], Delta);
end;

{ ItGeoRA }

procedure ItGeoRA.ProcessIt;
begin
  Request := CreateRequest(0, 0, 2, SelectedCelPoints);   // ayanamsha: none, observerpos: geo, coordinate: RA
  Response := Api.GetSeries(Request);
  ResultValues := RetrieveResults;
end;


procedure ItGeoRA.TestSunSaturnOscnodeHaumeaMeanapogee;
begin
  SelectedCelPoints := [0, 6, 11, 22, 29];                   // Sun, Saturn, Osc node, Haumea, Mean apogee
  ProcessIt;
  AssertEquals(280.88886, ResultValues[0], Delta);
  AssertEquals(293.11486, ResultValues[1], Delta);
  AssertEquals(99.12972, ResultValues[2], Delta);
  AssertEquals(215.77441, ResultValues[3], Delta);
  AssertEquals(359.30795, ResultValues[4], Delta);
end;


{ ItGeoDecl }

procedure ItGeoDecl.ProcessIt;
begin
  Request := CreateRequest(0, 0, 3, SelectedCelPoints);   // ayanamsha: none, observerpos: geo, coordinate:declination
  Response := Api.GetSeries(Request);
  ResultValues := RetrieveResults;
end;


procedure ItGeoDecl.TestMercuryUranusChironNessusIxion;
begin
  SelectedCelPoints := [2, 7, 13, 19, 24];                   // Mercury, Uranus, Chiron, Nessus, Ixion
  ProcessIt;
  AssertEquals(-24.63572, ResultValues[0], Delta);
  AssertEquals(11.94191, ResultValues[1], Delta);
  AssertEquals(3.31562, ResultValues[2], Delta);
  AssertEquals(-19.46076, ResultValues[3], Delta);
  AssertEquals(-29.28892, ResultValues[4], Delta);
end;

{ ItGeoDistance }

procedure ItGeoDistance.ProcessIt;
begin
  Request := CreateRequest(0, 0, 4, SelectedCelPoints);   // ayan.: none, obspos: geo, coordinate: RADV (distance)
  Response := Api.GetSeries(Request);
  ResultValues := RetrieveResults;
end;


procedure ItGeoDistance.TestVenusNeptunePholusJunoEris;
begin
  SelectedCelPoints := [3, 8, 14, 17, 23];                   // Venus, Neptune, Pholus, Juno, Eris
  ProcessIt;
  AssertEquals(1.278028131, ResultValues[0], Delta);
  AssertEquals(30.315286060, ResultValues[1], Delta);
  AssertEquals(29.444879745, ResultValues[2], Delta);
  AssertEquals(2.804882095, ResultValues[3], Delta);
  AssertEquals(95.788397175, ResultValues[4], Delta);
end;

{ ItHelioLong }

procedure ItHelioLong.ProcessIt;
begin
  Request := CreateRequest(0, 1, 0, SelectedCelPoints);      // no ayan, helio, longitude
  Response := Api.GetSeries(Request);
  ResultValues := RetrieveResults;
end;

procedure ItHelioLong.TestEarthUranusCeresJunoVaruna;
begin
  SelectedCelPoints := [12, 7, 15, 17, 28];                   // Uranus, Earth, Ceres, Juno, Varuna
  ProcessIt;
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(2, 5, 18, 56.7445), ResultValues[0], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(4, 10, 0, 34.2263), ResultValues[1], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(10, 20, 34, 47.9197), ResultValues[2], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(6, 27, 21, 11.9558), ResultValues[3], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(5, 2, 53, 8.1005), ResultValues[4], Delta);
end;


{ ItHelioLat }

procedure ItHelioLat.ProcessIt;
begin
  Request := CreateRequest(0, 1, 1, SelectedCelPoints);      // no ayan, helio, latitude
  Response := Api.GetSeries(Request);
  ResultValues := RetrieveResults;
end;

procedure ItHelioLat.TestMercJupPluHaumeaSedna;
begin
  SelectedCelPoints := [2, 5, 9, 22, 27];                       //Mercurius, Jupiter, Pluto, Haumea, Sedna
  ProcessIt;
  AssertEquals(Conv4Test.SePositionDMS2Decimal(-1, 3, 55, 7.1488), ResultValues[0], Delta);
  AssertEquals(Conv4Test.SePositionDMS2Decimal(1, 0, 6, 18.8232), ResultValues[1], Delta);
  AssertEquals(Conv4Test.SePositionDMS2Decimal(-1, 0, 40, 16.6201), ResultValues[2], Delta);
  AssertEquals(Conv4Test.SePositionDMS2Decimal(+1, 28, 5, 6.3255), ResultValues[3], Delta);
  AssertEquals(Conv4Test.SePositionDMS2Decimal(-1, 11, 54, 44.6876), ResultValues[4], Delta);
end;

{ ItHelioDistance }

procedure ItHelioDistance.ProcessIt;
begin
  Request := CreateRequest(0, 1, 4, SelectedCelPoints);      // no ayan, helio, latitude
  Response := Api.GetSeries(Request);
  ResultValues := RetrieveResults;
end;

procedure ItHelioDistance.TestVenusSatChiVestaQuaoar;
begin
  SelectedCelPoints := [3, 6, 13, 18, 26];                   // Venus, Saturn, Chiron, Vesta, Quaoar
  ProcessIt;
  AssertEquals(0.726266231, ResultValues[0], Delta);
  AssertEquals(10.034341445, ResultValues[1], Delta);
  AssertEquals(18.827196219, ResultValues[2], Delta);
  AssertEquals(2.563703975, ResultValues[3], Delta);
  AssertEquals(42.837848670, ResultValues[4], Delta);
end;


{ ItSidereal }

procedure ItSidereal.ProcessIt;
begin
  Response := Api.GetSeries(Request);
  ResultValues := RetrieveResults;
end;

procedure ItSidereal.TestFaganSunEris;
begin
  SelectedCelPoints:= [0, 23];
  Request := CreateRequest(1, 0, 0, SelectedCelPoints);      // Fagan ayanamsha, geo, longitude
  ProcessIt;
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(9, 14, 59, 39.9053), ResultValues[0], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(12, 28, 12, 55.6166), ResultValues[1], Delta);
end;

procedure ItSidereal.TestLahiriMercuryUranus;
begin
  SelectedCelPoints:= [2, 7];
  Request := CreateRequest(2, 0, 0, SelectedCelPoints);      // Lahiri ayanamsha, geo, longitude
  ProcessIt;
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(9, 10, 15, 5.2177), ResultValues[0], Delta);
  AssertEquals(Conv4Test.SePositionSignDMS2Decimal(1, 8, 33, 43.8061), ResultValues[1], Delta);
end;


initialization
  RegisterTest(ItGeoLong);
  RegisterTest(ItGeoLat);
  RegisterTest(ItGeoRA);
  RegisterTest(ItGeoDecl);
  RegisterTest(ItGeoDistance);
  RegisterTest(ItHelioLong);
  RegisterTest(ItHelioLat);
  RegisterTest(ItHelioDistance);
  RegisterTest(ItSidereal);
end.
