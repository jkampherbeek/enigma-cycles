{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit it_pairedpositions;

{< Integration tests for paired positions.}


{$mode objfpc}{$H+}
{$WARN 5091 off : Local variable "$1" of a managed type does not seem to be initialized}
interface

uses
  Classes, SysUtils, fpcunit, UnitApi, testregistry, UnitInit, UnitReqResp, UnitTranslation, UniTdomainXchg;

type

  { Parent for testclasses for paired positions }
  ItPairedPosParent = class(TTestCase)
  protected
    Request: TSeriesPairedRequest;
    Response: TSeriesResponse;
    Api: TSeriesAPI;
    function CreateDate(PJd: double; PYear, PMonth, PDay, PCalendar: integer): TValidatedDate;
    function CreateRequest: TSeriesPairedRequest; virtual; abstract;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestResponse;
  end;

  { Test for paired positions using longitude }
  ItPairedPositionsLongSunMoon = class(ItPairedPosParent)
  protected
    function CreateRequest: TSeriesPairedRequest; override;
  published
    procedure TestLongSunMoon;
  end;

  { Test for paired positions using latitude }
  ItPairedPositionsLatVenusMars = class(ItPairedPosParent)
  protected
    function CreateRequest: TSeriesPairedRequest; override;
  published
    procedure TestLatVenusMars;
  end;

implementation


{ ItPairedPosParent }

procedure ItPairedPosParent.SetUp;
var
  Rosetta: TRosetta;
begin
  Rosetta := TRosetta.Create;
  Rosetta.ChangeLanguage('en');
  Request := CreateRequest;
  Api := TSeriesAPI.Create;
  Response := Api.GetSeries(Request);
  inherited SetUp;
end;

procedure ItPairedPosParent.TearDown;
begin
  FreeAndNil(Request);
  FreeAndNil(Api);
  inherited TearDown;
end;


function ItPairedPosParent.CreateDate(PJd: double; PYear, PMonth, PDay, PCalendar: integer): TValidatedDate;
var
  ValDate: TValidatedDate;
begin
  ValDate.IsValid := True;
  ValDate.JulianDay := PJd;
  ValDate.Calendar := PCalendar;
  ValDate.Year := PYear;
  ValDate.Month := PMonth;
  ValDate.Day := PDay;
  Result := ValDate;
end;

procedure ItPairedPosParent.TestResponse;
begin
  AssertFalse(Response.Errors);
  AssertEquals('', Response.ErrorText);
end;

{ ItPairedPositionsLongSunMoon }

function ItPairedPositionsLongSunMoon.CreateRequest: TSeriesPairedRequest;
var
  LookupValues: TLookupValues;
  ObserverPos: TObserverPosSpec;
  Coordinate: TCoordinateSpec;
  Ayanamsha: TAyanamshaSpec;
  CycleType: TCycleTypeSpec;
  Period: TPeriod;
  Interval: integer;
  CelPointPair: TCelPointPairedSpec;
  CelPointPairs : TCelPointPairedSpecArray;
begin
  LookupValues := TLookupValues.Create;
  Period.StartDate := CreateDate(2458849.5, 2020, 1, 1, 1);
  Period.EndDate := CreateDate(2459214.5, 2021, 1, 1, 1);
  Interval := 1;
  CycleType := LookupValues.AllCycleTypes[1];                // paired position
  Ayanamsha := LookupValues.AllAyanamshas[0];                // tropical (no Ayanamsha)
  Coordinate := LookupValues.AllCoordinates[0];              // longuitude
  ObserverPos := LookupValues.AllObserverPos[0];             // geocentric
  CelPointPair.FirstCP:= LookUpValues.AllCelPoints[0];       // Sun
  CelPointPair.SecondCP:= LookupValues.AllCelPoints[1];      // Moon
  SetLength(CelPointPairs, 1);
  CelPointPairs[0]:= CelPointPair;
  Result := TSeriesPairedRequest.Create(Period, Interval, CycleType, Ayanamsha, Coordinate, ObserverPos, CelPointPairs);
end;

procedure ItPairedPositionsLongSunMoon.TestLongSunMoon;
{  position Sun: 10 cp  0'34.2263  = 280.009507306
   position Moon: 16 pi  8'18.2918 = 346.138414389
   difference                      =  66.128907083
}
var
  Delta, Expected, TempVal: Double;
  PosFile: TextFile;
  PosFileName, TempTxt, Line1, Line2, ExpectedHeader: string;
  LineItems2: TStringArray;
begin
  Delta := 0.0001;
  Expected:= 66.128907083;
  PosFileName := './data/positions.csv';
  ExpectedHeader := 'Date/time;JD;Sun;Moon;Value;';
  AssignFile(PosFile, PosFileName);
  try
    Reset(PosFile);
    ReadLn(PosFile, Line1);
    ReadLn(PosFile, Line2);
  finally
    CloseFile(PosFile);
  end;
  // Header
  AssertEquals(ExpectedHeader, Line1);
  LineItems2 := Line2.Split(';');
  TempTxt := LineItems2[4];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(Expected, TempVal, Delta);
end;


{ ItPairedPositionsLatVenusMars }

function ItPairedPositionsLatVenusMars.CreateRequest: TSeriesPairedRequest;
var
  LookupValues: TLookupValues;
  ObserverPos: TObserverPosSpec;
  Coordinate: TCoordinateSpec;
  Ayanamsha: TAyanamshaSpec;
  CycleType: TCycleTypeSpec;
  Period: TPeriod;
  Interval: integer;
  CelPointPair: TCelPointPairedSpec;
  CelPointPairs : TCelPointPairedSpecArray;
begin
  LookupValues := TLookupValues.Create;
  Period.StartDate := CreateDate(2458849.5, 2020, 1, 1, 1);
  Period.EndDate := CreateDate(2459214.5, 2021, 1, 1, 1);
  Interval := 1;
  CycleType := LookupValues.AllCycleTypes[1];                // paired position
  Ayanamsha := LookupValues.AllAyanamshas[0];                // tropical (no Ayanamsha)
  Coordinate := LookupValues.AllCoordinates[1];              // latitude
  ObserverPos := LookupValues.AllObserverPos[0];             // geocentric
  CelPointPair.FirstCP:= LookUpValues.AllCelPoints[3];       // Venus
  CelPointPair.SecondCP:= LookupValues.AllCelPoints[4];      // Mars
  SetLength(CelPointPairs, 1);
  CelPointPairs[0]:= CelPointPair;
  Result := TSeriesPairedRequest.Create(Period, Interval, CycleType, Ayanamsha, Coordinate, ObserverPos, CelPointPairs);
end;

procedure ItPairedPositionsLatVenusMars.TestLatVenusMars;
{ lat Venus:  -1°50'20.5792 = -1.839049778
  lat Mars:    0°21'41.6955 =  0.361582083
  difference                =  2.200631861
}
var
  Delta, Expected, TempVal: Double;
  PosFile: TextFile;
  PosFileName, TempTxt, Line1, Line2, ExpectedHeader: string;
  LineItems2: TStringArray;
begin
  Delta := 0.0001;
  Expected:= 2.200631861;
  PosFileName := './data/positions.csv';
  ExpectedHeader := 'Date/time;JD;Venus;Mars;Value;';
  AssignFile(PosFile, PosFileName);
  try
    Reset(PosFile);
    ReadLn(PosFile, Line1);
    ReadLn(PosFile, Line2);
  finally
    CloseFile(PosFile);
  end;
  // Header
  AssertEquals(ExpectedHeader, Line1);
  LineItems2 := Line2.Split(';');
  TempTxt := LineItems2[4];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(Expected, TempVal, Delta);

end;


initialization
  RegisterTest(ItPairedPositionsLongSunMoon);
  RegisterTest(ItPairedPositionsLatVenusMars);
end.

