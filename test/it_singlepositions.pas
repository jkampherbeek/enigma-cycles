{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit it_SinglePositions;

{< Integration tests for single positions. Thre is overlap with unit it_allcellpointp.}

{$mode objfpc}{$H+}

interface

uses
  Classes, fpcunit, SysUtils, testregistry, UnitApi, unitdomainxchg, UnitInit, UnitReqResp, UnitTranslation;

type

  { Parent for testclasses for single point positions }
  ItSinglePosParent = class(TTestCase)
  protected
    Request: TSeriesSingleRequest;
    Response: TSeriesResponse;
    Api: TSeriesAPI;
    function CreateDate(PJd: double; PYear, PMonth, PDay, PCalendar: integer): TValidatedDate;
    function CreateRequest: TSeriesSingleRequest; virtual; abstract;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestResponse;
  end;


  { Test for heliocentric longitude }
  ItSinglePosHelioTropLon = class(ItSinglePosParent)
  protected
    function CreateRequest: TSeriesSingleRequest; override;
  published
    procedure TestHelioTropLongMeVeEaHuQu; // Heliocentric, tropical longitude for Mercury, Venus, Earth, Huya, Quaoar
  end;


  { Test for geocentric longitude }
  ItSinglePosGeoTropLon = class(ItSinglePosParent)
  protected
    function CreateRequest: TSeriesSingleRequest; override;
  published
    procedure TestGeoTropLongSuMoJuOrSe;   // Geocentric, tropical, longitude for Sun, Moon, Jupiter, Orcus, Sedna
  end;


  { Test for declination }
  ItSinglePosDecl = class(ItSinglePosParent)
  protected
    function CreateRequest: TSeriesSingleRequest; override;
  published
    procedure TestDeclMoon;
  end;

  { Test for geocentric distance }
  ItSinglePosGeoDistance = class(ItSinglePosParent)
  protected
    function CreateRequest: TSeriesSingleRequest; override;
  published
    procedure TestDistanceMoJuPlMkEr;
  end;

implementation





{ ItSinglePosParent }


procedure ItSinglePosParent.SetUp;
var
  Rosetta: TRosetta;
begin
  Rosetta := TRosetta.Create;
  Rosetta.ChangeLanguage('en');
  Request := CreateRequest;
  Api := TSeriesAPI.Create;
  Response := Api.GetSeries(Request);
  inherited SetUp;
end;

procedure ItSinglePosParent.TearDown;
begin
  FreeAndNil(Request);
  FreeAndNil(Api);
  inherited TearDown;
end;

function ItSinglePosParent.CreateDate(PJd: double; PYear, PMonth, PDay, PCalendar: integer): TValidatedDate;
var
  ValDate: TValidatedDate;
begin
  ValDate.IsValid := True;
  ValDate.JulianDay := PJd;
  ValDate.Calendar := PCalendar;
  ValDate.Year := PYear;
  ValDate.Month := PMonth;
  ValDate.Day := PDay;
  Result := ValDate;
end;

procedure ItSinglePosParent.TestResponse;
begin
  AssertFalse(Response.Errors);
  AssertEquals('', Response.ErrorText);
end;


{ ItSinglePosGeoDistance }

function ItSinglePosGeoDistance.CreateRequest: TSeriesSingleRequest;
var
  LookupValues: TLookupValues;
  ObserverPos: TObserverPosSpec;
  Coordinate: TCoordinateSpec;
  Ayanamsha: TAyanamshaSpec;
  CycleType: TCycleTypeSpec;
  Period: TPeriod;
  Interval: integer;
  CelPoints: TCelPointSpecArray;
begin
  LookupValues := TLookupValues.Create;
  Period.StartDate := CreateDate(2458849.5, 2020, 1, 1, 1);
  Period.EndDate := CreateDate(2459214.5, 2021, 1, 1, 1);
  Interval := 1;
  CycleType := LookupValues.AllCycleTypes[0];     // position
  Ayanamsha := LookupValues.AllAyanamshas[0];     // tropical (no Ayanamsha)
  Coordinate := LookupValues.AllCoordinates[4];   // distance
  ObserverPos := LookupValues.AllObserverPos[0];  // geocentric
  CelPoints := LookupValues.AllCelPoints;
  CelPoints[1].Selected := True;                  // Maan
  CelPoints[5].Selected := True;                  // Jupiter
  CelPoints[9].Selected := True;                  // Pluto
  CelPoints[21].Selected := True;                 // Makemake
  CelPoints[23].Selected := True;                 // Eris
  Result := TSeriesSingleRequest.Create(Period, Interval, CycleType, Ayanamsha, Coordinate, ObserverPos, CelPoints);
end;


procedure ItSinglePosGeoDistance.TestDistanceMoJuPlMkEr;
var
  ExpectedHeader: string;
  Delta, TempVal, ExpMoon2, ExpMoon3, ExpJup2, ExpJup3, ExpPluto2, ExpPluto3, ExpMak2, ExpMak3,
  ExpEris2, ExpEris3: double;
  PosFile: TextFile;
  PosFileName, TempTxt, Line1, Line2, Line3: string;
  LineItems2, LineItems3: TStringArray;
begin
  PosFileName := './data/positions.csv';
  Delta := 0.0001;
  ExpMoon2 := 0.0027;
  ExpMoon3 := 0.0027;
  ExpJup2 := 6.20890;
  ExpJup3 := 6.20877;
  ExpPluto2 := 34.91014;
  ExpPluto3 := 34.91058;
  ExpMak2 := 52.51212;
  ExpMak3 := 52.51059;
  ExpEris2 := 95.78840;           // PD 95.7867
  ExpEris3 := 95.79004;
  ExpectedHeader := 'Date/time;JD;Moon;Jupiter;Pluto;Makemake;Eris;';
  AssignFile(PosFile, PosFileName);
  try
    Reset(PosFile);
    ReadLn(PosFile, Line1);
    ReadLn(PosFile, Line2);
    ReadLn(PosFile, Line3);
  finally
    CloseFile(PosFile);
  end;
  // Header
  AssertEquals(ExpectedHeader, Line1);
  LineItems2 := Line2.Split(';');
  LineItems3 := Line3.Split(';');
  // Distance of Moon
  TempTxt := LineItems2[2];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpMoon2, TempVal, Delta);
  TempTxt := LineItems3[2];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpMoon3, TempVal, Delta);
  // Distance of Jupiter
  TempTxt := LineItems2[3];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpJup2, TempVal, Delta);
  TempTxt := LineItems3[3];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpJup3, TempVal, Delta);
  // Distance of Pluto
  TempTxt := LineItems2[4];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpPluto2, TempVal, Delta);
  TempTxt := LineItems3[4];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpPluto3, TempVal, Delta);
  // Distance of MakeMake
  TempTxt := LineItems2[5];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpMak2, TempVal, Delta);
  TempTxt := LineItems3[5];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpMak3, TempVal, Delta);
  // Distance of Eris
  TempTxt := LineItems2[6];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpEris2, TempVal, Delta);
  TempTxt := LineItems3[6];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpEris3, TempVal, Delta);
end;

{ ItSinglePosDecl }

function ItSinglePosDecl.CreateRequest: TSeriesSingleRequest;
var
  LookupValues: TLookupValues;
  ObserverPos: TObserverPosSpec;
  Coordinate: TCoordinateSpec;
  Ayanamsha: TAyanamshaSpec;
  CycleType: TCycleTypeSpec;
  Period: TPeriod;
  Interval: integer;
  CelPoints: TCelPointSpecArray;
begin
  LookupValues := TLookupValues.Create;
  Period.StartDate := CreateDate(2458849.5, 2020, 1, 1, 1);
  Period.EndDate := CreateDate(2459214.5, 2021, 1, 1, 1);
  Interval := 1;
  CycleType := LookupValues.AllCycleTypes[0];     // position
  Ayanamsha := LookupValues.AllAyanamshas[0];     // tropical (no Ayanamsha)
  Coordinate := LookupValues.AllCoordinates[3];   // declination
  ObserverPos := LookupValues.AllObserverPos[0];  // geocentric
  CelPoints := LookupValues.AllCelPoints;
  CelPoints[1].Selected := True;                  // Moon
  Result := TSeriesSingleRequest.Create(Period, Interval, CycleType, Ayanamsha, Coordinate, ObserverPos, CelPoints);
end;


procedure ItSinglePosDecl.TestDeclMoon;
var
  ExpectedHeader: string;
  Delta, TempVal, ExpMoon2, ExpMoon3: double;
  PosFile: TextFile;
  PosFileName, TempTxt, Line1, Line2, Line3: string;
  LineItems2, LineItems3: TStringArray;
begin
  PosFileName := './data/positions.csv';
  Delta := 0.0001;
  ExpMoon2 := -9.9748;
  ExpMoon3 := -9.54594;
  ExpectedHeader := 'Date/time;JD;Moon;';
  AssignFile(PosFile, PosFileName);
  try
    Reset(PosFile);
    ReadLn(PosFile, Line1);
    ReadLn(PosFile, Line2);
    ReadLn(PosFile, Line3);
  finally
    CloseFile(PosFile);
  end;
  // Header
  AssertEquals(ExpectedHeader, Line1);
  LineItems2 := Line2.Split(';');
  LineItems3 := Line3.Split(';');
  // Declination of Moon
  TempTxt := LineItems2[2];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpMoon2, TempVal, Delta);
  TempTxt := LineItems3[2];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpMoon3, TempVal, Delta);

end;


{ ItSinglePosHelioTropLon }

function ItSinglePosHelioTropLon.CreateRequest: TSeriesSingleRequest;
var
  LookupValues: TLookupValues;
  ObserverPos: TObserverPosSpec;
  Coordinate: TCoordinateSpec;
  Ayanamsha: TAyanamshaSpec;
  CycleType: TCycleTypeSpec;
  Period: TPeriod;
  Interval: integer;
  CelPoints: TCelPointSpecArray;
begin
  LookupValues := TLookupValues.Create;
  Period.StartDate := CreateDate(2458849.5, 2020, 1, 1, 1);
  Period.EndDate := CreateDate(2459214.5, 2021, 1, 1, 1);
  Interval := 10;
  CycleType := LookupValues.AllCycleTypes[0];     // position
  Ayanamsha := LookupValues.AllAyanamshas[0];     // tropical (no Ayanamsha)
  Coordinate := LookupValues.AllCoordinates[0];   // ecliptical longitude
  ObserverPos := LookupValues.AllObserverPos[1];  // heliocentric
  CelPoints := LookupValues.AllCelPoints;
  CelPoints[2].Selected := True;                  // Mercury
  CelPoints[3].Selected := True;                  // Venus
  CelPoints[12].Selected := True;                 // Earth
  CelPoints[20].Selected := True;                 // Huya
  CelPoints[26].Selected := True;                 // Quaoar
  Result := TSeriesSingleRequest.Create(Period, Interval, CycleType, Ayanamsha, Coordinate, ObserverPos, CelPoints);
end;


procedure ItSinglePosHelioTropLon.TestHelioTropLongMeVeEaHuQu;
var
  ExpectedHeader: string;
  Delta, TempVal, ExpMercury2, ExpMercury3, ExpVenus2, ExpVenus3, ExpHuya2, ExpHuya3: double;
  PosFile: TextFile;
  PosFileName, TempTxt, Line1, Line2, Line3: string;
  LineItems2, LineItems3: TStringArray;
begin
  PosFileName := './data/positions.csv';
  Delta := 0.0001;
  ExpMercury2 := 262.44444;
  ExpMercury3 := 265.20147;
  ExpVenus2 := 4.42543;
  ExpVenus3 := 6.01710;
  ExpHuya2 := 250.13948;
  ExpHuya3 := 250.14701;
  ExpectedHeader := 'Date/time;JD;Mercury;Venus;Earth;Huya;Quaoar;';
  // values 2nd line '2020/01/01 00,00;2458849,5;262,44444;004,42543;100,00951;250,13948;273,05620;'
  // values 3rd line '2020/01/02 00,00;2458850,5;265,20147;006,01710;101,02894;250,14701;273,05981;'
  // read lines from file
  AssignFile(PosFile, PosFileName);
  try
    Reset(PosFile);
    ReadLn(PosFile, Line1);
    ReadLn(PosFile, Line2);
    ReadLn(PosFile, Line3);
  finally
    CloseFile(PosFile);
  end;
  // Header
  AssertEquals(ExpectedHeader, Line1);
  LineItems2 := Line2.Split(';');
  LineItems3 := Line3.Split(';');
  // Longitude of Mercury
  TempTxt := LineItems2[2];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpMercury2, TempVal, Delta);
  TempTxt := LineItems3[2];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpMercury3, TempVal, Delta);
  // Longitude of Venus
  TempTxt := LineItems2[3];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpVenus2, TempVal, Delta);
  TempTxt := LineItems3[3];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpVenus3, TempVal, Delta);
  // Longitude of Huya
  TempTxt := LineItems2[5];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpHuya2, TempVal, Delta);
  TempTxt := LineItems3[5];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpHuya3, TempVal, Delta);
end;


{ ItSinglePosGeoTropLon }

procedure ItSinglePosGeoTropLon.TestGeoTropLongSuMoJuOrSe;
var
  ExpectedHeader: string;
  Delta, TempVal, ExpJd2, ExpJd3, ExpSun2, ExpSun3, ExpMoon2, ExpMoon3, ExpSedna2, ExpSedna3: double;
  PosFile: TextFile;
  PosFileName, TempTxt, Line1, Line2, Line3: string;
  LineItems2, LineItems3: TStringArray;
begin
  PosFileName := './data/positions.csv';
  Delta := 0.0001;
  ExpJd2 := 2458849.5;
  ExpJd3 := 2458849.6;
  ExpSun2 := 280.00951;
  ExpSun3 := 280.11145;
  ExpMoon2 := 346.13841;
  ExpMoon3 := 347.32907;
  ExpSedna2 := 57.07489;
  ExpSedna3 := 57.07416;
  ExpectedHeader := 'Date/time;JD;Sun;Moon;Jupiter;Orcus;Sedna;';
  // values 2nd line '2020/01/01 00,00;2458849,5;280,00951;346,13841;276,67035;162,13054;057,07489;'
  // values 3rd line '2020/01/01 02,40;2458849,6;280,11145;347,32907;276,69340;162,12978;057,07416;'
  // read lines from file
  AssignFile(PosFile, PosFileName);
  try
    Reset(PosFile);
    ReadLn(PosFile, Line1);
    ReadLn(PosFile, Line2);
    ReadLn(PosFIle, Line3);
  finally
    CloseFile(PosFile);
  end;
  // Header
  AssertEquals(ExpectedHeader, Line1);
  LineItems2 := Line2.Split(';');
  LineItems3 := Line3.Split(';');
  // Jd number
  TempTxt := LineItems2[1];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpJd2, TempVal, Delta);
  TempTxt := LineItems3[1];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpJd3, TempVal, Delta);
  // Longitude of Sun
  TempTxt := LineItems2[2];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpSun2, TempVal, Delta);
  TempTxt := LineItems3[2];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpSun3, TempVal, Delta);
  //Longitude of Moon
  TempTxt := LineItems2[3];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpMoon2, TempVal, Delta);
  TempTxt := LineItems3[3];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpMoon3, TempVal, Delta);
  //Longitude of Sedna
  TempTxt := LineItems2[6];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpSedna2, TempVal, Delta);
  TempTxt := LineItems3[6];
  TempVal := StrToFloat(TempTxt);
  AssertEquals(ExpSedna3, TempVal, Delta);
end;



function ItSinglePosGeoTropLon.CreateRequest: TSeriesSingleRequest;
var
  LookupValues: TLookupValues;
  ObserverPos: TObserverPosSpec;
  Coordinate: TCoordinateSpec;
  Ayanamsha: TAyanamshaSpec;
  CycleType: TCycleTypeSpec;
  Period: TPeriod;
  Interval: integer;
  CelPoints: TCelPointSpecArray;
begin
  LookupValues := TLookupValues.Create;
  Period.StartDate := CreateDate(2458849.5, 2020, 1, 1, 1);
  Period.EndDate := CreateDate(2459214.5, 2021, 1, 1, 1);
  Interval := 1;
  CycleType := LookupValues.AllCycleTypes[0];     // position
  Ayanamsha := LookupValues.AllAyanamshas[0];     // tropical (no Ayanamsha)
  Coordinate := LookupValues.AllCoordinates[0];   // ecliptical longitude
  ObserverPos := LookupValues.AllObserverPos[0];  // geocentric
  CelPoints := LookupValues.AllCelPoints;
  CelPoints[0].Selected := True;                  // Sun
  CelPoints[1].Selected := True;                  // Moon
  CelPoints[5].Selected := True;                  // Jupiter
  CelPoints[25].Selected := True;                 // Orcus
  CelPoints[27].Selected := True;                 // Sedna
  Result := TSeriesSingleRequest.Create(Period, Interval, CycleType, Ayanamsha, Coordinate, ObserverPos, CelPoints);
end;



initialization
  RegisterTest(ItSinglePosHelioTropLon);
  RegisterTest(ItSinglePosDecl);
  RegisterTest(ItSinglePosGeoDistance);
  RegisterTest(ItSinglePosGeoTropLon);
end.
