unit test_domainxchg;

{< Unit tests for domain. }

{$mode objfpc}{$H+}

interface

uses
  Classes, fpcunit, SysUtils, testregistry, UnitDomainXchg;

type


  { Tests for domain }
  TTestDomainXchg = class(TTestCase)
  protected
    CycleTypeEnumConverter: TCycleTypeEnumConverter;
    CoordinateEnumConverter: TCoordinateEnumConverter;
    ObserverPosEnumConverter: TObserverPosEnumConverter;
    AyanamshaEnumConverter: TAyanamshaEnumConverter;
    CelPointEnumConverter: TCelPointEnumConverter;
    procedure SetUp; override;
    procedure TearDown; override;

  published
    procedure TestCycleTypeStringFromId;
    procedure TestCycleTypeIdFromString;
    procedure TestCoordinateStringFromId;
    procedure TestCoordinateIdFromString;
    procedure TestObserverPosStringFromId;
    procedure TestObserverPosIdFromString;
    procedure TestAyanamshaStringFromId;
    procedure TestAyanamshaIdFromString;
    procedure TestCelPointStringFromId;
    procedure TestCelpointIdFromString;
    procedure TestCpSpecFromId;
  end;



implementation

procedure TTestDomainXchg.SetUp;
begin
  CycleTypeEnumConverter := TCycleTypeEnumConverter.Create;
  CoordinateENumConverter := TCoordinateEnumConverter.Create;
  ObserverPosEnumConverter := TObserverPosEnumConverter.Create;
  AyanamshaEnumConverter := TAyanamshaEnumConverter.Create;
  CelPointEnumConverter := TCelPointEnumConverter.Create;
  inherited SetUp;
end;

procedure TTestDomainXchg.TearDown;
begin
  FreeAndNil(CycleTypeEnumConverter);
  FreeAndNil(CoordinateEnumConverter);
  FreeAndNil(ObserverPosEnumConverter);
  FreeAndNil(AyanamshaEnumConverter);
  FreeAndNil(CelPointEnumConverter);
  inherited TearDown;
end;

procedure TTestDomainXchg.TestCycleTypeStringFromId;
begin
  assertEquals('position', CycleTypeEnumConverter.StringFromId(position));
  assertEquals('angle', CycleTypeEnumConverter.StringFromId(angle));
end;

procedure TTestDomainXchg.TestCycleTypeIdFromString;
begin
  assertTrue(position = CycleTypeEnumConverter.IdFromString('position'));
  assertTrue(angle = CycleTypeEnumConverter.IdFromString('angle'));
end;

procedure TTestDomainXchg.TestCoordinateStringFromId;
begin
  assertEquals('longitude', CoordinateEnumConverter.StringFromId(longitude));
  assertEquals('latitude', CoordinateEnumConverter.StringFromId(latitude));
  assertEquals('rightasc', CoordinateEnumConverter.StringFromId(rightasc));
  assertEquals('decl', CoordinateEnumConverter.StringFromId(decl));
  assertEquals('radv', CoordinateEnumConverter.StringFromId(radv));
end;

procedure TTestDomainXchg.TestCoordinateIdFromString;
begin
  assertTrue(longitude = CoordinateEnumConverter.IdFromString('longitude'));
  assertTrue(latitude = CoordinateEnumConverter.IdFromString('latitude'));
  assertTrue(rightasc = CoordinateEnumConverter.IdFromString('rightasc'));
  assertTrue(decl = CoordinateEnumConverter.IdFromString('decl'));
  assertTrue(radv = CoordinateEnumConverter.IdFromString('radv'));
end;

procedure TTestDomainXchg.TestObserverPosStringFromId;
begin
  assertEquals('geocentric', ObserverPosEnumConverter.StringFromId(geocentric));
  assertEquals('heliocentric', ObserverPosEnumConverter.StringFromId(heliocentric));

end;

procedure TTestDomainXchg.TestObserverPosIdFromString;
begin
  assertTrue(geocentric = ObserverPosEnumConverter.IdFromString('geocentric'));
  assertTrue(heliocentric = ObserverPosEnumConverter.IdFromString('heliocentric'));
end;


procedure TTestDomainXchg.TestAyanamshaStringFromId;
begin
  assertEquals('none', AyanamshaEnumConverter.StringFromId(none));
  assertEquals('fagan', AyanamshaEnumConverter.StringFromId(fagan));
  assertEquals('lahiri', AyanamshaEnumConverter.StringFromId(lahiri));
  assertEquals('raman', AyanamshaEnumConverter.StringFromId(raman));
  assertEquals('krishnamurti', AyanamshaEnumConverter.StringFromId(krishnamurti));
  assertEquals('huber', AyanamshaEnumConverter.StringFromId(huber));
  assertEquals('brand', AyanamshaEnumConverter.StringFromId(brand));
end;

procedure TTestDomainXchg.TestAyanamshaIdFromString;
begin
  assertTrue(none = AyanamshaEnumConverter.IdFromString('none'));
  assertTrue(fagan = AyanamshaEnumConverter.IdFromString('fagan'));
  assertTrue(lahiri = AyanamshaEnumConverter.IdFromString('lahiri'));
  assertTrue(raman = AyanamshaEnumConverter.IdFromString('raman'));
  assertTrue(krishnamurti = AyanamshaEnumConverter.IdFromString('krishnamurti'));
  assertTrue(huber = AyanamshaEnumConverter.IdFromString('huber'));
  assertTrue(brand = AyanamshaEnumConverter.IdFromString('brand'));
end;

procedure TTestDomainXchg.TestCelPointStringFromId;
begin
  assertEquals('sun', CelPointEnumConverter.StringFromId(sun));
  assertEquals('moon', CelPointEnumConverter.StringFromId(moon));
  assertEquals('mercury', CelPointEnumConverter.StringFromId(mercury));
  assertEquals('venus', CelPointEnumConverter.StringFromId(venus));
  assertEquals('mars', CelPointEnumConverter.StringFromId(mars));
  assertEquals('jupiter', CelPointEnumConverter.StringFromId(jupiter));
  assertEquals('saturn', CelPointEnumConverter.StringFromId(saturn));
  assertEquals('uranus', CelPointEnumConverter.StringFromId(uranus));
  assertEquals('neptune', CelPointEnumConverter.StringFromId(neptune));
  assertEquals('pluto', CelPointEnumConverter.StringFromId(pluto));
  assertEquals('meannode', CelPointEnumConverter.StringFromId(meannode));
  assertEquals('oscunode', CelPointEnumConverter.StringFromId(oscunode));
  assertEquals('earth', CelPointEnumConverter.StringFromId(earth));
  assertEquals('chiron', CelPointEnumConverter.StringFromId(chiron));
  assertEquals('pholus', CelPointEnumConverter.StringFromId(pholus));
  assertEquals('ceres', CelPointEnumConverter.StringFromId(ceres));
  assertEquals('pallas', CelPointEnumConverter.StringFromId(pallas));
  assertEquals('juno', CelPointEnumConverter.StringFromId(juno));
  assertEquals('vesta', CelPointEnumConverter.StringFromId(vesta));
  assertEquals('nessus', CelPointEnumConverter.StringFromId(nessus));
  assertEquals('huya', CelPointEnumConverter.StringFromId(huya));
  assertEquals('makemake', CelPointEnumConverter.StringFromId(makemake));
  assertEquals('haumea', CelPointEnumConverter.StringFromId(haumea));
  assertEquals('eris', CelPointEnumConverter.StringFromId(eris));
  assertEquals('ixion', CelPointEnumConverter.StringFromId(ixion));
  assertEquals('orcus', CelPointEnumConverter.StringFromId(orcus));
  assertEquals('quaoar', CelPointEnumConverter.StringFromId(quaoar));
  assertEquals('sedna', CelPointEnumConverter.StringFromId(sedna));
  assertEquals('varuna', CelPointEnumConverter.StringFromId(varuna));
  assertEquals('meanapog', CelPointEnumConverter.StringFromId(meanapog));
  assertEquals('oscuapog', CelPointEnumConverter.StringFromId(oscuapog));
end;

procedure TTestDomainXchg.TestCelpointIdFromString;
begin
  assertTrue(sun = CelPointEnumConverter.IdFromString('sun'));
  assertTrue(moon = CelPointEnumConverter.IdFromString('moon'));
  assertTrue(mercury = CelPointEnumConverter.IdFromString('mercury'));
  assertTrue(venus = CelPointEnumConverter.IdFromString('venus'));
  assertTrue(mars = CelPointEnumConverter.IdFromString('mars'));
  assertTrue(jupiter = CelPointEnumConverter.IdFromString('jupiter'));
  assertTrue(saturn = CelPointEnumConverter.IdFromString('saturn'));
  assertTrue(uranus = CelPointEnumConverter.IdFromString('uranus'));
  assertTrue(neptune = CelPointEnumConverter.IdFromString('neptune'));
  assertTrue(pluto = CelPointEnumConverter.IdFromString('pluto'));
  assertTrue(meannode = CelPointEnumConverter.IdFromString('meannode'));
  assertTrue(oscunode = CelPointEnumConverter.IdFromString('oscunode'));
  assertTrue(earth = CelPointEnumConverter.IdFromString('earth'));
  assertTrue(chiron = CelPointEnumConverter.IdFromString('chiron'));
  assertTrue(pholus = CelPointEnumConverter.IdFromString('pholus'));
  assertTrue(ceres = CelPointEnumConverter.IdFromString('ceres'));
  assertTrue(pallas = CelPointEnumConverter.IdFromString('pallas'));
  assertTrue(juno = CelPointEnumConverter.IdFromString('juno'));
  assertTrue(vesta = CelPointEnumConverter.IdFromString('vesta'));
  assertTrue(nessus = CelPointEnumConverter.IdFromString('nessus'));
  assertTrue(huya = CelPointEnumConverter.IdFromString('huya'));
  assertTrue(makemake = CelPointEnumConverter.IdFromString('makemake'));
  assertTrue(haumea = CelPointEnumConverter.IdFromString('haumea'));
  assertTrue(eris = CelPointEnumConverter.IdFromString('eris'));
  assertTrue(ixion = CelPointEnumConverter.IdFromString('ixion'));
  assertTrue(orcus = CelPointEnumConverter.IdFromString('orcus'));
  assertTrue(quaoar = CelPointEnumConverter.IdFromString('quaoar'));
  assertTrue(sedna = CelPointEnumConverter.IdFromString('sedna'));
  assertTrue(varuna = CelPointEnumConverter.IdFromString('varuna'));
  assertTrue(meanapog = CelPointEnumConverter.IdFromString('meanapog'));
  assertTrue(oscuapog = CelPointEnumConverter.IdFromString('oscuapog'));
end;

procedure TTestDomainXchg.TestCpSpecFromId;
begin
  assertTrue(makemake = CelPointEnumConverter.CpSpecFromId(makemake).Identification);
end;




initialization

  RegisterTest('DomainXchg', TTestDomainXchg);
end.







