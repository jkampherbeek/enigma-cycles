{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit test_process;

{< Unit tests for processing the calculation of cycles. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, unitprocess, unitdomainxchg, unitastron;

type
  { Tests for the flags as used by the Swiss Ephemeris. }
  TestSeFlags = class(TTestCase)
  strict private
    AyanamshaSpecTropical: TAyanamshaSpec;
  protected
    procedure SetUp; override;
  published
    procedure TestFlagsGeoEclTrop;
    procedure TestFlagsHelioEclTrop;
    procedure TestFlagsEquatTrop;
    procedure TestFlagsEclSidereal;
  end;

  { Test for the conversion of date and time. }
  TestDateTimeConversion = class(TTestCase)
  protected
    Ephemeris: TEphemeris;
    DateTimeConversion: TDateTimeConversion;
  published
    procedure TestHappyFlow;
  end;


implementation


{ TestSeFlags -------------------------------------------------------------------------------------------------------- }

procedure TestSeFlags.SetUp;
begin
  AyanamshaSpecTropical.SeId := -1;
  AyanamshaSpecTropical.Name := 'None';
  AyanamshaSpecTropical.Identification := None;
end;

procedure TestSeFlags.TestFlagsGeoEclTrop;
var
  CoordinateSpec: TCoordinateSpec;
  ObserverPosSpec: TObserverPosSpec;
  SeFlags: TSeFlags;
begin
  CoordinateSpec.Identification := longitude;
  ObserverPosSpec.Identification:= geocentric;
  SeFlags := TSeFlags.Create(CoordinateSpec, AyanamshaSpecTropical, ObserverPosSpec);
  assertEquals(258, SeFlags.FlagsValue);                // 2 or 256
end;

procedure TestSeFlags.TestFlagsHelioEclTrop;
var
  CoordinateSpec: TCoordinateSpec;
    ObserverPosSpec: TObserverPosSpec;
  SeFlags: TSeFlags;
begin
  CoordinateSpec.Identification := longitude;
    ObserverPosSpec.Identification:= heliocentric;
  SeFlags := TSeFlags.Create(CoordinateSpec, AyanamshaSpecTropical, ObserverPosSpec);
  assertEquals(266, SeFlags.FlagsValue);                // 2 or 256 or 8
end;

procedure TestSeFlags.TestFlagsEquatTrop;
var
  CoordinateSpec: TCoordinateSpec;
  ObserverPosSpec: TObserverPosSpec;
  SeFlags: TSeFlags;
begin
  CoordinateSpec.Identification := rightasc;
  ObserverPosSpec.Identification:= geocentric;
  SeFlags := TSeFlags.Create(CoordinateSpec, AyanamshaSpecTropical, ObserverPosSpec);
  assertEquals(2306, SeFlags.FlagsValue);                // 2 or 256 or 2048
end;

procedure TestSeFlags.TestFlagsEclSidereal;
var
  CoordinateSpec: TCoordinateSpec;
  ObserverPosSpec: TObserverPosSpec;
  Ayanamsha: TAyanamshaSpec;
  SeFlags: TSeFlags;
begin
  CoordinateSpec.Identification := longitude;
  ObserverPosSpec.Identification:= geocentric;
  Ayanamsha.Name := 'Huber';
  Ayanamsha.SeId := 4;
  SeFlags := TSeFlags.Create(CoordinateSpec, Ayanamsha, ObserverPosSpec);
  assertEquals(65794, SeFlags.FlagsValue);                // 2 or 256 or (64 * 1024))
end;


{ TestDateTimeConversion --------------------------------------------------------------------------------------------- }
procedure TestDateTimeConversion.TestHappyFlow;
var
  ExpectedJD: double = 2434406.5;
  CalculatedJD, Delta: double;
  DateText: string = '1953/01/29';
  Calendar: integer = 1;                   // Gregorian
begin
  Delta := 0.00000001;
  Ephemeris := TEphemeris.Create;
  DateTimeConversion := TDateTimeConversion.Create(Ephemeris);
  CalculatedJD := DateTimeConversion.DateTextToJulianDay(DateText, Calendar);
  AssertEquals(ExpectedJD, CalculatedJD, Delta);
end;



initialization
  RegisterTest('Process', TestSeFlags);
  RegisterTest('Process', TestDateTimeConversion);
end.

