{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit test_translation;

{< Unit tests for internationalisation. }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, unittranslation;

type

  { Tests for Rosetta, the translation enigne. }
  TTestRosetta = class(TTestCase)
  protected
    procedure SetUp; override;
  strict private
    Rosetta: TRosetta;
  published
    procedure TestChangeLanguage;
    procedure TestGetTextEn;
    procedure TestGetTextNl;
    procedure TestGetTextWrongSection;
    procedure TestGetTextWrongKey;
  end;

implementation

procedure TTestRosetta.SetUp;
begin
  Rosetta:= TRosetta.Create;
end;


procedure TTestRosetta.TestChangeLanguage;
begin
  Rosetta.ChangeLanguage('en');
  AssertTrue(Rosetta.CurrentLanguage = 'en');
  Rosetta.ChangeLanguage('nl');
  AssertTrue(Rosetta.CurrentLanguage = 'nl');
end;

procedure TTestRosetta.TestGetTextEn;
var
  translatedTxt: string;
begin
  Rosetta.ChangeLanguage('en');
  translatedTxt:= Rosetta.getText('formstartscreen', 'existing');
  AssertEquals('Existing Cycle', translatedTxt);
end;

procedure TTestRosetta.TestGetTextNl;
var
  translatedTxt: string;
begin
   Rosetta.ChangeLanguage('nl');
  translatedTxt:= Rosetta.getText('formstartscreen', 'existing');
  AssertEquals('Bestaande Cyclus', translatedTxt);
end;

procedure TTestRosetta.TestGetTextWrongSection;
var
  translatedTxt: string;
begin
  translatedTxt:= Rosetta.getText('wrong_section', 'existing');
  AssertEquals('-NOT FOUND-', translatedTxt);
end;

procedure TTestRosetta.TestGetTextWrongKey;
var
  translatedTxt: string;
begin
  translatedTxt:= Rosetta.getText('formstartscreen', 'wrong_key');
  AssertEquals('-NOT FOUND-', translatedTxt);
end;

initialization

  RegisterTest('Translation', TTestRosetta);
end.

