{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit test_uiperiods;

{< Unit tests for UI periods. }
{$mode objfpc}{$H+}

interface

uses
  Classes, fpcunit, SysUtils, testregistry, UnitDomainXchg, UnitUiUtils;

type

  { Tests for UI periods }
  TTestUiPeriods = class(TTestCase)
  private
    function CreateValidatedDate(JulianDay: Double; Year, Month, Day,
      Calendar: integer): TValidatedDate;
  protected
    Periods: TUiPeriods;
    CelPointsWithMoon, CelPointsSlowPoints: TCelPointSpecArray;
    PairedCelPoints: TCelPointPairedSpecArray;
    function CreateCelPoint(SeId, Interval, MaxDayParts: integer; identification: TEnumCelpoint;
      Name: string): TCelPointspec;
    procedure DefineCelPointSets;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestIntervalHappyFlow;
    procedure TestIntervalSlowPlanets;
    procedure TestIntervalPairs;
    procedure TestPeriodLengthHappyflow;
    procedure TestPeriodLengthSlowPlanets;
    procedure TestPeriodLengthPairs;
    procedure TestDefineMaxFittingPeriod;
  end;

implementation

procedure TTestUiPeriods.SetUp;
begin
  Periods := TUiPeriods.Create;
  DefineCelPointSets;
  inherited SetUp;
end;

procedure TTestUiPeriods.TearDown;
begin
  FreeAndNil(Periods);
  inherited TearDown;
end;

function TTestUiPeriods.CreateCelPoint(SeId, Interval, MaxDayParts: integer; identification: TEnumCelpoint;
  Name: string): TCelPointspec;
var
  CelPoint: TCelPointSpec;
begin
  CelPoint.SeId := SeId;
  CelPoint.Interval := Interval;
  CelPoint.MaxDayParts := MaxDayParts;
  CelPoint.Identification := Identification;
  CelPoint.Name := Name;
  CelPOint.Selected:= true;
  Result := CelPoint;
end;

procedure TTestUiPeriods.DefineCelPointSets;
var
  PairedCP: TCelPointPairedSpec;
begin
  CelPointsWithMoon[0] := CreateCelpoint(1, 1, 20000, moon, 'Moon');
  CelPointsWithMoon[1] := CreateCelpoint(16, 100, 2000000, pholus, 'Pholus');
  CelPointsWithMoon[2] := CreateCelpoint(9, 100, 7500000, pluto, 'Pluto');

  CelPointsSlowPoints[0] := CreateCelpoint(9, 100, 7500000, pluto, 'Pluto');
  CelPointsSlowPoints[1] := CreateCelpoint(146199, 200, 20000000, eris, 'Eris');
  CelPointsSlowPoints[2] := CreateCelpoint(100377, 200, 20000000, sedna, 'Sedna');

  SetLength(PairedCelPoints, 2);
  PairedCP.FirstCP:= CreateCelpoint(16, 100, 2000000, pholus, 'Pholus');
  PairedCP.SecondCP:= CreateCelpoint(146199, 200, 20000000, eris, 'Eris');
  PairedCelPoints[0]:= PairedCP;
  PairedCP.FirstCP:= CreateCelpoint(0, 10, 200000, sun, 'Sun');
  PairedCP.SecondCP:= CreateCelpoint(15, 100, 2000000, chiron, 'Chiron');
  PairedCelPoints[1]:= PairedCP;
end;

procedure TTestUiPeriods.TestIntervalHappyFlow;
var
  SmallestInterval: integer;
begin
  SmallestInterval := Periods.defineShortestInterval(CelPointsWithMoon);
  AssertEquals(1, SmallestInterval);
end;

procedure TTestUiPeriods.TestIntervalSlowPlanets;
var
  SmallestInterval: integer;
begin
  SmallestInterval := Periods.defineShortestInterval(CelPointsSlowPoints);
  AssertEquals(100, SmallestInterval);
end;

procedure TTestUiPeriods.TestIntervalPairs;
var
  SmallestInterval: integer;
begin
  SmallestInterval:= Periods.defineShortestInterval(PairedCelPoints);
  AssertEquals(10, SmallestInterval);
end;

procedure TTestUiPeriods.TestPeriodLengthHappyflow;
var
  ShortestPeriod: integer;
begin
  ShortestPeriod:= Periods.defineShortestPeriodLength(CelPointsWithMoon);
  AssertEquals(20000, ShortestPeriod);
end;

procedure TTestUiPeriods.TestPeriodLengthSlowPlanets;
var
  ShortestPeriod: integer;
begin
  ShortestPeriod:= Periods.defineShortestPeriodLength(CelPointsSlowPoints);
  AssertEquals(7500000, ShortestPeriod);
end;

procedure TTestUiPeriods.TestPeriodLengthPairs;
var
  ShortestPeriod: integer;
begin
  ShortestPeriod:= Periods.defineShortestPeriodLength(PairedCelPoints);
  AssertEquals(200000, ShortestPeriod);
end;

procedure TTestUiPeriods.TestDefineMaxFittingPeriod;
var
  Period, CorrectedPeriod: TPeriod;
  JdNr: Double;
  Year, Month, Day, Calendar, MaxPeriodLength: integer;
begin
  JdNr:= 2434396.5;       // 1953-1-19
  Year:= 1953;
  Month:= 1;
  Day:= 19;
  Calendar:= 1;
  MaxPeriodLength:= 10;
  Period.StartDate:= CreateValidatedDate(JdNr, Year, Month, Day, Calendar);
  Period.EndDate:= CreateValidatedDate(JdNr + 10, Year, Month, Day + 10, Calendar);
  CorrectedPeriod:= Periods.DefineMaxFittingPeriod(Period, MaxPeriodLength);
  AssertEquals(CorrectedPeriod.EndDate.JulianDay, JdNr + 10);
  AssertEquals(CorrectedPeriod.EndDate.Year, Year);
  AssertEquals(CorrectedPeriod.EndDate.Month, Month);
  AssertEquals(CorrectedPeriod.EndDate.Day, Day + 10);
  AssertEquals(CorrectedPeriod.EndDate.Calendar, Calendar);
  AssertTrue(CorrectedPeriod.EndDate.IsValid);
end;

function TTestUiPeriods.CreateValidatedDate(JulianDay: Double; Year, Month, Day, Calendar: integer): TValidatedDate;
var
  ValDate: TValidatedDate;
begin
  ValDate.JulianDay:= JulianDay;
  ValDate.IsValid:= true;
  ValDate.Year:= Year;
  ValDate.Month:= Month;
  ValDate.Day:= Day;
  ValDate.Calendar:= Calendar;
  Result:= ValDate;
end;


initialization
  RegisterTest('UI Utils', TTestUiPeriods);
end.
