{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit test_uitexts;

{< Unit tests for handling of text in UI. }

{$mode objfpc}{$H+}
{$WARN 5091 off : Local variable "$1" of a managed type does not seem to be initialized}
interface

uses
  Classes, fpcunit, SysUtils, testregistry, UnitDomainXchg, UnitTranslation, UnitUiUtils;

type

  { Tests for handling of texts in UI. }
  TTestUiTexts = class(TTestCase)
  protected
    UiTexts: TUiTexts;
    StartDate, EndDate, TestDate: TValidatedDate;
    Rosetta: TRosetta;
    function CreateDate(Year, Month, Day, Calendar: integer): TValidatedDate;
    function CreateCelpointSpec(SeId: integer; Identification: TEnumCelpoint; Name: string;
      FirstJd, LastJd: double; GeoCentric, HelioCentric, Distance, Selected: boolean): TCelPointSpec;
    function CreateCelPointPairedSpec(Cp1, Cp2: TCelPointSpec): TCelPointPairedSpec;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestHappyFlowTextForDate;
    procedure TestAncientTextForDate;
    procedure TestHappyFlowTextForPeriod;
    procedure TestAncientTextforPeriod;
    procedure TestTextForCelPoints;
    procedure TestTextForCelPointPairs;
  end;


implementation

{ TTestUiTexts }

procedure TTestUiTexts.TestHappyFlowTextForDate;
begin
  TestDate := CreateDate(2021, 7, 10, 1);
  AssertEquals('2021/7/10 Greg.', UiTexts.TextForDate(TestDate));
end;

procedure TTestUiTexts.TestAncientTextForDate;
begin
  TestDate := CreateDate(-10123, 12, 1, 0);
  AssertEquals('-10123/12/1 Jul.', UiTexts.TextForDate(TestDate));
end;

procedure TTestUiTexts.TestHappyFlowTextForPeriod;
begin
  StartDate := CreateDate(1900, 1, 1, 1);
  EndDate := CreateDate(1901, 1, 1, 1);
  AssertEquals('1900/1/1 Greg. - 1901/1/1 Greg.', UiTexts.TextForPeriod(StartDate, EndDate));
end;

procedure TTestUiTexts.TestAncientTextforPeriod;
begin
  StartDate := CreateDate(-1000, 2, 3, 0);
  EndDate := CreateDate(-600, 4, 5, 0);
  AssertEquals('-1000/2/3 Jul. - -600/4/5 Jul.', UiTexts.TextForPeriod(StartDate, EndDate));
end;

procedure TTestUiTexts.TestTextForCelPoints;
var
  CelPoints: TCelPointSpecArray;
  CelPointText, ExpectedText: string;
begin
  CelPoints[0] := CreateCelpointSpec(0, sun, 'Sun', -3026613.5, 5227458.5, True, False, True, True);
  CelPoints[1] := CreateCelpointSpec(1, moon, 'Moon', -3026613.5, 5227458.5, True, False, True, False);
  CelPoints[2] := CreateCelpointSpec(2, mercury, 'Mercury', -3026613.5, 5227458.5, True, True, True, True);
  CelPoints[3] := CreateCelpointSpec(3, venus, 'Venus', -3026613.5, 5227458.5, True, True, True, False);
  CelPoints[4] := CreateCelpointSpec(4, mars, 'Mars', -3026613.5, 5227458.5, True, True, True, True);
  CelPoints[5] := CreateCelpointSpec(5, jupiter, 'Jupiter', -3026613.5, 5227458.5, True, True, True, True);
  CelPoints[6] := CreateCelpointSpec(6, saturn, 'Saturn', -3026613.5, 5227458.5, True, True, True, False);
  ExpectedText := 'Sun - Mercury' + LineEnding + 'Mars - Jupiter';
  CelPointText := UiTexts.TextForCelPoints(CelPoints);
  AssertEquals(ExpectedText, CelPointText);

end;

procedure TTestUiTexts.TestTextForCelPointPairs;
var
  Pairs: TCelPointPairedSpecArray;
  PairText, ExpectedText: string;
begin
  SetLength(Pairs, 5);
  Pairs[0] := CreateCelPointPairedSpec(CreateCelpointSpec(0, sun, 'Sun', -3026613.5, 5227458.5,
    True, False, True, True), CreateCelpointSpec(1, moon, 'Moon', -3026613.5, 5227458.5, True, False, True, True));
  Pairs[1] := CreateCelPointPairedSpec(CreateCelpointSpec(0, sun, 'Sun', -3026613.5, 5227458.5,
    True, False, True, True), CreateCelpointSpec(6, saturn, 'Saturn', -3026613.5, 5227458.5,
    True, True, True, True));
  Pairs[2] := CreateCelPointPairedSpec(CreateCelpointSpec(4, mars, 'Mars', -3026613.5,
    5227458.5, True, True, True, True), CreateCelpointSpec(3, venus, 'Venus', -3026613.5,
    5227458.5, True, True, True, True));
  Pairs[3] := CreateCelPointPairedSpec(CreateCelpointSpec(146472, makemake, 'MakeMake',
    625292.5, 2816291.5, True, True, True, True), CreateCelpointSpec(12, meanapog, 'Mean Apogee',
    -3026613.5, 5227458.5, True, False, False, True));
  Pairs[4] := CreateCelPointPairedSpec(CreateCelpointSpec(15, chiron, 'Chiron', 1967601.5,
    3419437.5, True, True, True, True), CreateCelpointSpec(146199, eris, 'Eris', 625384.5,
    2816383.5, True, True, True, True));
  ExpectedText := 'Sun and Moon - Sun and Saturn' + LineEnding + 'Mars and Venus - MakeMake and Mean Apogee' +
    LineEnding + 'Chiron and Eris';
  PairText := UiTexts.TextForCelPointPairs(Pairs);
  AssertEquals(ExpectedText, PairText);
end;

function TTestUiTexts.CreateDate(Year, Month, Day, Calendar: integer): TValidatedDate;
var
  NewDate: TValidatedDate;
begin
  NewDate.IsValid := True;
  NewDate.JulianDay := 123456.789;   // dummy value
  NewDate.Year := Year;
  NewDate.Month := Month;
  NewDate.Day := Day;
  NewDate.Calendar := Calendar;
  NewDate.IsValid := True;
  Result := NewDate;
end;

function TTestUiTexts.CreateCelpointSpec(SeId: integer; Identification: TEnumCelpoint;
  Name: string; FirstJd, LastJd: double; GeoCentric, HelioCentric, Distance, Selected: boolean): TCelPointSpec;
var
  CpSpec: TCelPointSpec;
begin
  CpSpec.SeId := SeId;
  CpSpec.Identification := Identification;
  CpSpec.Name := Name;
  CpSpec.FIrstJd := FirstJd;
  CpSpec.LastJd := LastJd;
  CpSpec.Geocentric := Geocentric;
  CpSpec.HelioCentric := HelioCentric;
  CpSpec.Distance := Distance;
  CpSpec.Selected := Selected;
  Result := CpSpec;
end;

function TTestUiTexts.CreateCelPointPairedSpec(Cp1, Cp2: TCelPointSpec): TCelPointPairedSpec;
var
  Pair: TCelPointPairedSpec;
begin
  Pair.FirstCP := Cp1;
  Pair.SecondCP := Cp2;
  Result := Pair;
end;

procedure TTestUiTexts.SetUp;
begin
  UiTexts := TUiTexts.Create;
  Rosetta := TRosetta.Create;
  Rosetta.ChangeLanguage('en');
  inherited SetUp;
end;

procedure TTestUiTexts.TearDown;
begin
  FreeAndNil(UiTexts);
  inherited TearDown;
end;


initialization
  RegisterTest('UI Utils', TTestUiTexts);
end.
