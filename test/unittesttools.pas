{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitTestTools;

{< Utility classes for unit tests. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

  { Utility class for converting texts. }
  TConversions4Test = class
  public
    function SePositionSignDMS2Decimal(Sign, Degrees, Minutes: integer; Seconds: double): double;
    function SePositionDMS2Decimal(PosMin, Degrees, Minutes: integer; Seconds: double): double;
  end;

implementation

{ TConversions4Test }

function TConversions4Test.SePositionSignDMS2Decimal(Sign, Degrees, Minutes: integer; Seconds: double): double;
begin
  Result := (Sign - 1) * 30 + Degrees + Minutes / 60.0 + Seconds / 3600.0;
end;

function TConversions4Test.SePositionDMS2Decimal(PosMin, Degrees, Minutes: integer; Seconds: double): double;
begin
  Result := (Abs(Degrees) + Minutes / 60.0 + Seconds / 3600.0) * PosMin;
end;

end.

