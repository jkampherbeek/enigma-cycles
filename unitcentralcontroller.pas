{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitcentralcontroller;

{< Unit for a set of singletons that manage the applications as a whole.}

{$mode objfpc}{$H+}
{$WARN 3018 off : Constructor should be public}
interface

uses
  Classes, Forms, SysUtils, unitapi, unitdomainxchg, unitinit, unitreqresp;

type

  TStateMessages = (NewCycle, ExistingCycle, Cancel, CycleTypeDefined, CoordinateDefined, PeriodDefined,
    PairedCPDefined, SingleCPDefined, Confirmed, CloseMain, ShowFreqChart, ShowLineChart,
    CloseFreqChart, CloseLineChart);

  { Central Controller (Singleton): handles the flow of the application. }
  TCenCon = class
  strict private
    FLookupValues: TLookupValues;
    constructor Init;
  public
    class function Create: TCenCon;
    property LookupValues: TLookupValues read FLookupValues;
  end;



  { Finite State Machine (Singleton): keeps track of the state of hte application }
  TFinStateMachine = class
  strict private
    FCycleType: TCycleTypeSpec;
    FCoordinate: TCoordinateSpec;
    FAyanamsha: TAyanamshaSpec;
    FObserverPos: TObserverPosSpec;
    FPeriod: TPeriod;
    FStartDate, FEndDate: string;
    FInterval: integer;
    FSingleCPs: TCelPointSpecArray;
    FPairedCPs: TCelPointPairedSpecArray;
    FDataFilename, FTempFilename, FFreqFilename: string;
    SeriesAPI: TSeriesAPI;
    FStatusInit: boolean;
    constructor Init;
    procedure WriteMeta(Request: TSeriesRequest);
    procedure ProcessCycle;
    procedure ReUseCycle;
    function CreateRequest: TSeriesRequest;
  public
    class function Create: TFinStateMachine;
    property Ayanamsha: TAyanamshaSpec read FAyanamsha write FAyanamsha;
    property ObserverPos: TObserverPosSpec read FObserverPos write FObserverPos;
    property CycleType: TCycleTypeSpec read FCycleType write FCycleType;
    property Coordinate: TCoordinateSpec read FCoordinate write FCoordinate;
    property Period: TPeriod read FPeriod write FPeriod;
    property StartDate: string read FStartDate write FStartDate;
    property EndDate: string read FEndDate write FEndDate;
    property Interval: integer read FInterval write FInterval;
    property SingleCPs: TCelPointSpecArray read FSingleCPs write FSingleCPs;
    property PairedCPs: TCelPointPairedSpecArray read FPairedCPs write FPairedCPs;
    property StatusInit: boolean read FStatusInit write FStatusInit;
    property DataFilename: string read FDataFilename write FDataFilename;
    property TempFilename: string read FTempFilename write FTempFilename;
    property FreqFilename: string read FFreqFilename write FFreqFilename;
    procedure ChangeState(Message: TStateMessages);
  end;



implementation

uses unitastron, UnitConst, UnitConversions, unitdlgconfirm, UnitDlgCoordinate, UnitDlgCycleType,
  unitdlgpairedcp, UnitDlgPeriod, unitdlgsinglecp,
  unitfreqchart, unitlinechart, unitmain, unitstartscreen;

var
  CenCon: TCenCon = nil;
  FinStateMachine: TFinStateMachine = nil;


{ TFinStateMachine }

constructor TFinStateMachine.Init;
begin
  SeriesAPI := TSeriesAPI.Create;
  inherited Create;
end;

class function TFinStateMachine.Create: TFinStateMachine;
begin
  if FinStateMachine = nil then FinStateMachine := TFinStateMachine.Init;
  Result:= FinStateMachine;
end;

procedure TFinStateMachine.WriteMeta(Request: TSeriesRequest);
var
  MetaFile: TextFile;
  i, cal: integer;
  tempText: string;
  tempJd: double;
  CycleTypeEnumConverter: TCycleTypeEnumConverter;
  CoordinateEnumConverter: TCoordinateEnumConverter;
  ObserverPosEnumConverter: TObserverPosEnumConverter;
  AyanamshaEnumConverter: TAyanamshaEnumConverter;
  CelPointEnumConverter: TCelPointEnumConverter;
  JulianDayConversion: TJulianDayConversion;
  SingleRequest: TSeriesSingleRequest;
  PairedRequest: TSeriesPairedRequest;
begin
  CycleTypeEnumConverter := TCycleTypeEnumConverter.Create;
  CoordinateEnumConverter := TCoordinateEnumConverter.Create;
  ObserverPosEnumConverter := TObserverPosEnumConverter.Create;
  AyanamshaEnumConverter := TAyanamshaEnumConverter.Create;
  CelPointEnumConverter := TCelPointEnumConverter.Create;
  JulianDayConversion := TJulianDayConversion.Create(TSeFrontend.Create);
  try
    AssignFile(MetaFile, DATA_PATH + METAFILENAME);
    rewrite(MetaFile);
    writeln(MetaFile, 'Property;Value');
    writeln(MetaFile, 'CycleType;' + CycleTypeEnumConverter.StringFromId(Request.CycleType.Identification));
    writeln(MetaFile, 'Coordinate;' + CoordinateEnumConverter.StringFromId(Request.Coordinate.Identification));
    writeln(MetaFile, 'ObserverPos;' + ObserverPosEnumConverter.StringFromId(Request.ObserverPos.Identification));
    writeln(MetaFile, 'Ayanamsha;' + AyanamshaEnumConverter.StringFromId(Request.Ayanamsha.Identification));
    tempJd := Request.Period.StartDate.JulianDay;
    cal := Request.Period.StartDate.Calendar;
    writeln(MetaFile, 'StartDate;' + JulianDayConversion.ConvertJdToDateText(tempJd, cal));
    tempJd := Request.Period.EndDate.JulianDay;
    writeln(MetaFile, 'EndDate;' + JulianDayConversion.ConvertJdToDateText(tempJd, cal));
    writeln(MetaFile, 'Interval;' + IntToStr(Request.Interval));

    if Request.CycleType.Identification = angle then begin
      PairedRequest := Request as TSeriesPairedRequest;
      tempText := 'CelPointPairs';
      for i := 0 to length(PairedRequest.CelPointPairs) - 1 do
        tempText := tempText + CelPointEnumConverter.StringFromId(
          PairedRequest.CelPointPairs[i].FirstCP.Identification) + '-' +
          CelPointEnumConverter.StringFromId(PairedRequest.CelPointPairs[i].SecondCP.Identification) + ';';
      writeln(MetaFile, tempText);
    end else begin
      SingleRequest := Request as TSeriesSingleRequest;
      tempText := 'CelPoints;';
      for i := 0 to length(SingleRequest.CelPoints) - 1 do if (SingleRequest.CelPoints[i].Selected) then
          tempText := tempText + CelPointEnumConverter.StringFromId(SingleRequest.CelPoints[i].Identification) + ';';
      writeln(MetaFile, tempText);
    end;
  finally
    CloseFile(MetaFile);
  end;

end;

procedure TFinStateMachine.ProcessCycle;
var
  Request: TSeriesRequest;
  {%H-}Response: TSeriesResponse;
begin
  Request := CreateRequest;
  WriteMeta(Request);
  Response := SeriesAPI.GetSeries(Request);
  StatusInit := False;
  FormDlgConfirm.Close;
  FormMain.Show;
end;

procedure TFinStateMachine.ReUseCycle;
begin
  StatusInit := False;
  FormMain.Show;
end;

function TFinStateMachine.CreateRequest: TSeriesRequest;
var
  Request: TSeriesRequest;
begin
  if (CycleType.Identification = position) then
    Request := TSeriesSingleRequest.Create(Period, Interval, CycleType, Ayanamsha, Coordinate, ObserverPos, SingleCPs)
  else
    Request := TSeriesPairedRequest.Create(Period, Interval, CycleType, Ayanamsha, Coordinate, ObserverPos, PairedCPs);
  Result := Request;
end;

procedure TFinStateMachine.ChangeState(Message: TStateMessages);
begin
  case Message of
    ExistingCycle: begin
      FormStartScreen.Hide;
      ReUseCycle;
    end;
    NewCycle: begin
      if StatusInit then FormStartScreen.Hide
      else
        FormMain.Hide;
      FormDlgCycleType.Show;
    end;
    CycleTypeDefined: FormDlgCoordinate.Show;
    CoordinateDefined: FormDlgPeriod.Show;
    PeriodDefined: if (CycleType.Identification = position) then FormDlgSingleCP.Show
      else
        FormDlgPairedCP.Show;
    PairedCpDefined: FormDlgConfirm.Show;
    SingleCPDefined: FormDlgConfirm.Show;
    Confirmed: ProcessCycle;
    ShowLineChart: FormLineChart.Show;
    ShowFreqChart: FormFreqChart.Show;
    CloseLineChart: FormMain.Show;
    CloseFreqChart: FormMain.Show;
    Cancel: if StatusInit then FormStartScreen.Show
      else
        FormMain.Show;
    CloseMain: FormStartScreen.Show;
  end;
end;


{ TCenCon }

constructor TCenCon.Init;
begin
  FLookUpvalues := TLookupValues.Create;
  inherited Create;
end;

class function TCenCon.Create: TCenCon;
begin
  if CenCon = nil then CenCon := TCenCon.Init;
  Result:= CenCon;
end;


end.
