{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitLog;

{ < Simple logger. }

{$mode objfpc}{$H+}
{$WARN 3018 off : Constructor should be public}
interface

uses
  Classes, SysUtils;

type

  { Handles logging (Singleton). }
  TLogger = class
  private
    constructor Init;
    procedure ShiftLogFiles;
    function MakeLogLine(Msg: string): string;
  public
    class function Create: TLogger;
    procedure Log(msg: string);
  end;

implementation

const
  LOGOLD = './log/sessionold.log';
  LOGPREV = './log/sessionprev.log';
  LOGCURR = './log/sessioncurr.log';

var
  Logger: TLogger = nil;
  LogFile: TextFile;

{ TLogger }

constructor TLogger.Init;
begin
  ShiftLogFiles;
  inherited Create;
end;

class function TLogger.Create: TLogger;
begin
  if Logger = nil then Logger := TLogger.Init;
  Result := Logger;
end;

procedure TLogger.ShiftLogFiles;
begin
  if FileExists(LOGOLD) then DeleteFile(LOGOLD);
  if FileExists(LOGPREV) then RenameFile(LOGPREV, LOGOLD);
  if FileExists(LOGCURR) then RenameFile(LOGCURR, LOGPREV);
  AssignFile(LogFile, LOGCURR);
  Rewrite(LogFile);
  CloseFile(LogFile);
  Log('Logfile for new session created');
end;

function TLogger.MakeLogLine(Msg: string): string;
var
  DateTimeText: string;
  DateTime: TDateTime;
begin
  DateTime := Now;
  DateTimeText := FormatDateTime('YYYY/MM/DD hh:nn:ss.zzz', DateTime);
  Result := DateTimeText + '|' + Msg;
end;


procedure TLogger.Log(msg: string);
var
  LogLine: string;
begin
  LogLine := MakeLogLine(msg);
  AssignFile(LogFile, LOGCURR);
  Append(LogFile);
  writeln(LogFile, LogLine);
  CloseFile(LogFile);
end;


end.



