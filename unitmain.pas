{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitmain;

{< Main form.}

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, ExtCtrls, Forms, Graphics, Grids, Menus, HelpIntfs,
  StdCtrls, SysUtils, unitcentralcontroller, unitdomainxchg, unitinit, UnitTranslation, UnitLog;

type

  { Form that shows results in a table, central form for the application. }

  TFormMain = class(TForm)
    BtnExit: TButton;
    BtnFreq: TButton;
    BtnLineChart: TButton;
    BtnNew: TButton;
    BtnHelp: TButton;
    LblAyanamsha: TLabel;
    LblAyanamshaValue: TLabel;
    LblCoordinate: TLabel;
    LblCoordinateValue: TLabel;
    LblCycleType: TLabel;
    LblCycleTypeValue: TLabel;
    LblInterval: TLabel;
    LblInterValValue: TLabel;
    LblMeta: TLabel;
    LblObserverPos: TLabel;
    LblObserverPosValue: TLabel;
    LblPeriod: TLabel;
    LblPeriodValue: TLabel;
    LblTitle: TLabel;
    MiCycle: TMenuItem;
    MiAbout: TMenuItem;
    MiNew: TMenuItem;
    MiExit: TMenuItem;
    MiCharts: TMenuItem;
    MiFreq: TMenuItem;
    MiLine: TMenuItem;
    MiHelp: TMenuItem;
    MiScreenHelp: TMenuItem;
    MenuMain: TMainMenu;
    SgPositions: TStringGrid;
    procedure ExitClick(Sender: TObject);
    procedure FreqClick(Sender: TObject);
    procedure HelpClick(Sender: TObject);
    procedure LineChartClick(Sender: TObject);
    procedure NewClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    LookupValues: TLookupValues;
    CycleTypeEnumConverter: TCycleTypeEnumConverter;
    CoordinateEnumConverter: TCoordinateEnumConverter;
    ObserverPosEnumConverter: TObserverPosEnumConverter;
    AyanamshaEnumConverter: TAyanamshaEnumConverter;
    CelPointEnumConverter: TCelPointEnumConverter;
    Rosetta: TRosetta;
    Logger: TLogger;
    procedure PopulateTexts;
    procedure PopulateData;
    procedure ReadMeta;
    procedure ProcessMetaLine(MetaLine: string);
  public
    CenCon: TCenCon;
    FinStateMachine: TFinStateMachine;
  end;

var
  FormMain: TFormMain;

implementation

{$R *.lfm}

uses
  UnitConst;

const
  SECTION = 'formmain';

{ TFormMain }

procedure TFormMain.FormCreate(Sender: TObject);
begin
  FinStateMachine := TFinStateMachine.Create;
  CenCon := TCenCon.Create;
  LookupValues := TLookupValues.Create;
  Rosetta := TRosetta.Create;
  Logger:= TLogger.Create;
  CycleTypeEnumConverter := TCycleTypeEnumConverter.Create;
  CoordinateEnumConverter := TCoordinateEnumConverter.Create;
  ObserverPosEnumConverter := TObserverPosEnumConverter.Create;
  AyanamshaEnumConverter := TAyanamshaEnumConverter.Create;
  CelPointEnumConverter := TCelPointEnumConverter.Create;
end;

procedure TFormMain.FormShow(Sender: TObject);
begin
  LookupValues := TLookupValues.Create;
  PopulateTexts;
  ReadMeta;
  PopulateData;
  BtnFreq.Enabled := not (FinStateMachine.Coordinate.Identification = radv);
end;

procedure TFormMain.PopulateTexts;
begin
  with Rosetta do begin
    FormMain.Caption := GetText(SECTION, 'formtitle');
    LblTitle.Caption := GetText(SECTION, 'title');
    LblAyanamsha.Caption := GetText(SECTION, 'ayanamsha');
    LblCoordinate.Caption := GetText(SECTION, 'coordinate');
    LblCycleType.Caption := GetText(SECTION, 'cycletype');
    LblMeta.Caption := GetText(SECTION, 'definition');
    LblInterval.Caption := GetText(SECTION, 'interval');
    LblObserverPos.Caption := GetText(SECTION, 'observerpos');
    LblInterval.Caption := GetText(SECTION, 'interval');
    LblPeriod.Caption := GetText(SECTION, 'period');
    BtnLineChart.Caption := GetText(SECTION, 'linechart');
    BtnFreq.Caption := GetText(SECTION, 'frequency');
    BtnNew.Caption := GetText(SECTION, 'new');
    BtnExit.Caption := GetText(SHARED_SECTION, 'exit');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'help');
    MiCycle.Caption := GetText(SECTION, 'menucycle');
    MiNew.Caption := GetText(SECTION, 'new');
    MiExit.Caption := GetText(SHARED_SECTION, 'exit');
    MiCharts.Caption := GetText(SECTION, 'menucharts');
    MiFreq.Caption := GetText(SECTION, 'frequency');
    MiLine.Caption := GetText(SECTION, 'linechart');
    MiHelp.Caption := GetText(SHARED_SECTION, 'help');
    MiScreenHelp.Caption := GetText(SECTION, 'miscreenhelp');
    MiAbout.Caption := GetText(SECTION, 'miabout');
  end;
end;

procedure TFormMain.PopulateData;
var
  FilenameData: string;
begin
  FilenameData := DATA_PATH + POSFILENAME;
  try
    SgPositions.LoadFromCSVFile(FilenameData, ';', True, 0, True);
  except
    on E: EInOutError do begin
      Logger.Log('ERROR: UnitMain: Could not read PositionsFile: ' + E.Message);
      ShowMessage(Rosetta.GetText('error','noreadpositions'));
    end;
  end;
end;

procedure TFormMain.ReadMeta;
var
  FilenameMeta, singleLine, calendar: string;
  MetaFile: TextFile;
begin
  FilenameMeta := DATA_PATH + METAFILENAME;
  AssignFile(MetaFile, FilenameMeta);
  reset(MetaFile);
  try
    while not EOF(MetaFile) do begin
      readln(MetaFile, SingleLine);
      ProcessMetaLine(singleLine);
    end;
    CloseFile(MetaFile);
    if (FinStateMachine.Period.StartDate.Calendar = 1) then Calendar := 'G'
    else
      Calendar := 'J';
    LblAyanamshaValue.Caption := FinStateMachine.Ayanamsha.Name;
    LblCoordinateValue.Caption := FinStateMachine.Coordinate.Name;
    LblCycleTypeValue.Caption := FinStateMachine.CycleType.Name;
    LblInterValValue.Caption := (FinStateMachine.Interval / 10).ToString;
    LblObserverPosValue.Caption := FinStateMachine.ObserverPos.Name;
    LblPeriodValue.Caption := FinStateMachine.StartDate + ' - ' + FinStateMachine.EndDate + ' ' + Calendar;
  except
    on E: EInOutError do begin
      Logger.Log('ERROR: UnitMain: Could not read MetaFile: ' + E.Message);
      ShowMessage(Rosetta.GetText('error','noreadmeta'));
    end;
  end;
end;

procedure TFormMain.ProcessMetaLine(MetaLine: string);
var
  Items, SubItems: TStringArray;
  MetaName, Value: string;
  i: integer;
  EnumCycleType: TEnumCycleType;
  EnumCoordinate: TEnumCoordinate;
  EnumObserverPos: TEnumObserverPos;
  EnumAyanamsha: TEnumAyanamsha;
  PairedCelPoint: TCelPointPairedSpec;
  PairedCelPoints: TCelPointPairedSpecArray;
begin
  Items := MetaLine.Split(';');
  MetaName := Items[0];
  Value := Items[1];
  case MetaName of
    'Property': ;     // ignore first line
    'CelPoints': ;    // ignore, already handled
    'CycleType': begin
      EnumCycleType := CycleTypeEnumConverter.IdFromString(Value);
      for i := 0 to length(LookUpValues.AllCycleTypes) - 1 do
        if LookUpValues.AllCycleTypes[i].Identification = EnumCycleType then
          FinStateMachine.CycleType := LookUpValues.AllCycleTypes[i];
    end;
    'Coordinate': begin
      EnumCoordinate := CoordinateEnumConverter.IdFromString(Value);
      for i := 0 to length(LookupValues.AllCoordinates) - 1 do
        if LookupValues.AllCoordinates[i].Identification = EnumCoordinate then
          FinStateMachine.Coordinate := LookupValues.AllCoordinates[i];
    end;
    'ObserverPos': begin
      EnumObserverPos := ObserverPosEnumConverter.IdFromString(Value);
      for i := 0 to length(LookupValues.AllObserverPos) - 1 do
        if LookupValues.AllObserverPos[i].Identification = EnumObserverPos then
          FinStateMachine.ObserverPos := LookupValues.AllObserverPos[i];
    end;
    'Ayanamsha': begin
      EnumAyanamsha := AyanamshaEnumConverter.IdFromString(Value);
      for i := 0 to length(LookupValues.AllAyanamshas) - 1 do
        if LookupValues.AllAyanamshas[i].Identification = EnumAyanamsha then
          FinStateMachine.Ayanamsha := LookupValues.AllAyanamshas[i];
    end;
    'StartDate': FinStateMachine.StartDate := Value;
    'EndDate': FinStateMachine.EndDate := Value;
    'Interval': FinStateMachine.Interval := Value.ToInteger();
    'CelPointPairs': begin
      for i := 1 to length(Items) - 1 do SubItems := Items[i].Split('-');
      PairedCelPoint.FirstCP := CelPointEnumConverter.CpSpecFromId(CelPointEnumConverter.IdFromString(SubItems[0]));
      PairedCelPoint.SecondCP := CelPointEnumConverter.CpSpecFromId(CelPointEnumConverter.IdFromString(SubItems[1]));
       {%H-}PairedcelPoints[i - 1] := PairedCelPoint;
      FinStateMachine.PairedCPs := PairedCelPoints;
    end
    else Logger.log('ERROR: UnitMain: encountered unknown MetaName: ' + MetaName);
  end;
end;

procedure TFormMain.NewClick(Sender: TObject);
begin
  SgPositions.Clear;
  FinStateMachine.StatusInit := False;
  FinStateMachine.ChangeState(NewCycle);
end;

procedure TFormMain.HelpClick(Sender: TObject);
var
  HelpText: string;
begin
  if Rosetta.CurrentLanguage = 'en' then HelpText := 'html/en_main.html'
  else
    HelpText := 'html/nl_main.html';
  ShowHelpOrErrorForKeyword(EmptyStr, HelpText);
end;

procedure TFormMain.FreqClick(Sender: TObject);
begin
  Close;
  FinStateMachine.ChangeState(ShowFreqChart);
end;

procedure TFormMain.ExitClick(Sender: TObject);
begin
  Close;
  FinStateMachine.ChangeState(CloseMain);
end;

procedure TFormMain.LineChartClick(Sender: TObject);
begin
  Close;
  FinStateMachine.ChangeState(ShowLineChart);
end;


end.
