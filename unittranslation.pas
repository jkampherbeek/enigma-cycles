{ Jan Kampherbeek, (c)  2021.
  Enigma Cycles is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitTranslation;

{< Classes for i18n. Implemented as singleton. }

{$mode objfpc}{$H+}
{$WARN 3018 off : Constructor should be public}
interface

uses
  Classes, inifiles, SysUtils, UnitLog;

type

  { Translator. }
  TRosetta = class
  strict private
    FCurrentLanguage: string;
    IniFile: TIniFile;
    Logger: TLogger;
    constructor Init;
    procedure Update;
    procedure PersistLanguageChoice(lang: string);
    function ReadLanguageChoice: string;
  public
    class function Create: TRosetta;
    procedure ChangeLanguage(lang: string);
    function GetText(section, key: string): string;
    property CurrentLanguage: string read FCurrentLanguage;
  end;

implementation

uses
  StrUtils;

var
  Rosetta: TRosetta = nil;

const
  LangSel = '.\lang.sel.properties';
  EnTexts = '.\languages\langen.ini';
  NlTexts = '.\languages\langnl.ini';

{ TRosetta }

constructor TRosetta.Init;
begin
  Logger:= TLogger.Create;
  Update;
  inherited Create;
end;

class function TRosetta.Create: TRosetta;
begin
  if Rosetta = nil then Rosetta:= TRosetta.Init;
  Result:= Rosetta;
end;

procedure TRosetta.Update;
var
  LanguageFilename: string;
begin
  FCurrentLanguage := ReadLanguageChoice;
  if (FCurrentLanguage = 'en') then LanguageFilename := EnTexts
  else
    LanguageFileName := NlTexts;
  if (Assigned(IniFile)) then IniFile.Free;
  IniFile := TIniFile.Create(LanguageFileName);
end;

procedure TRosetta.PersistLanguageChoice(lang: string);
var
  LangSelFile: TextFile;
begin
  AssignFile(LangSelFile, LangSel);
  try
    Rewrite(LangSelFile);
    WriteLn(LangSelFile, lang);
  finally
    CloseFile(LangSelFile);
  end;
end;

function TRosetta.ReadLanguageChoice: string;
var
  LangSelFile: TextFile;
  Lang: string;
begin
  AssignFile(LangSelFile, LangSel);
  try
    Reset(LangSelFile);
    ReadLn(LangSelFile, Lang);
  finally
    CloseFile(LangSelFile);
  end;
  Result := Lang;
end;

procedure TRosetta.ChangeLanguage(lang: string);
begin
  if (lang = 'en') or (lang = 'nl') then begin
    PersistLanguageChoice(lang);
    FCurrentLanguage := lang;
    Update;
  end;
end;

function TRosetta.GetText(section, key: string): string;
var
  RawText: string;
begin
  RawText := IniFile.ReadString(section, key, '-NOT FOUND-');
  if RawText = '-NOT FOUND-' then Logger.log('ERROR:UnitTranslation|Could not find text for section ' +
      section + ' or key ' + key);
  Result := ReplaceStr(RawText, '[EOL]', LineEnding);
end;

end.

